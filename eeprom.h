/*******************************************************************************
 *    Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 * FILE
 *    i2c_comms.h
 *
 *  DESCRIPTION
 *    Header file for the I2C procedures
 *
 ******************************************************************************/
#ifndef _EEPROM_H
#define _EEPROM_H

/*=============================================================================
 *  SDK Header Files
 *============================================================================*/
#include <pio.h>
#include <types.h>

/*=============================================================================
 *  Public Type Definitions
 *============================================================================*/

#include "configuration.h" //--

// EEPROM interface 150408
extern sys_status eepromReadBytes(uint16 romAddress, uint16 numBytes, uint16 *buffer);
extern sys_status eepromWriteBytes(uint16 romAddress, uint16 *data, uint16 numBytes);
extern uint32 eepromSize(void);

#endif /* _I2C_COMMS_H */