
#include <time.h>
#include <nvm.h>

#include "configuration.h"
#include "ht_hw.h"
#include "lightsensor.h"
#include "i2c_comms.h"

#define LIGHTSENSOR_I2C_ADDR  0x20  // 0x20 0010000b shift 1 bit


void lightSensorPowerDown(void)
{
    i2cUsePeripheralBus();
    
    i2cWriteRegister(LIGHTSENSOR_I2C_ADDR, 0x00, 0x01); // power shutdown
       
    i2cUseMainBus();
}    
void lightSensorPowerOn(void)
{
    lightSensorInit();
}

void lightSensorInit()
{       

    i2cUsePeripheralBus();
        
    i2cWriteRegister(LIGHTSENSOR_I2C_ADDR, 0x00, 0x00);
    i2cWriteRegister(LIGHTSENSOR_I2C_ADDR, 0x00, 0x01);  // power shutdown

    i2cWriteRegister(LIGHTSENSOR_I2C_ADDR, 0x00, 0x41);  // reset enable, keep shutdown

    i2cWriteRegister(LIGHTSENSOR_I2C_ADDR, 0x00, 0x06);  // 200ms integration time, high sensitivity, power on

    //-- TimeWaitWithTimeout16(FALSE, (300*MILLISECOND), result);
    //-- This function will later be followed by a time wait more than 1 second to reset, no need of this.
    
    
    i2cUseMainBus();
   
}

uint16 lightSensorReadDirect()
{
    uint8 values[3];
    uint16 value16bit;
    bool result;
    
    // must read it consecutive way
    result=i2cReadRegisters(LIGHTSENSOR_I2C_ADDR, 0x50, 2, values);

    value16bit=values[1];
    value16bit=(value16bit<<8) | values[0];   
    
    return value16bit;    
}

uint16 lightSensorRead()
{    
    uint16 value16bit;
    
    i2cUsePeripheralBus();

    value16bit=lightSensorReadDirect();
    
    i2cUseMainBus();
    
    return value16bit;
}

extern uint16 lightDataCurrent(void); // defined on sleep monitoring module

AmbientLightStatus lightSensorAmbientLevel()
{
    AmbientLightStatus ambientLightLevel;
    uint16 value=lightDataCurrent(); //-- SensorReadDirect();
    if(value>=0xF000){
      ambientLightLevel=AMBIENT_LIGHT_BRIGHT;
    } else if (value>0x7000){
      ambientLightLevel=AMBIENT_LIGHT_NORMAL;
    } else {
      ambientLightLevel=AMBIENT_LIGHT_WEAK;        
    }    
    return ambientLightLevel;
    
}