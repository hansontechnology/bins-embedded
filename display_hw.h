/**
---------------------------------------------------------------------
**
**  This device driver was created by 
**  8-Bit Single-Chip Microcontrollers
**
**  Copyright(C) visionox display Corporation 2002 - 2009
**  All rights reserved by Kunshan visionox display Corporation.
**
**  This program should be used on your own responsibility.
**
**  Filename :	IC.h
**  Abstract :	This file implements device driver for define SH1106 IC Pin function.
**
**  Device :	STC89LE516RD
**
**  Compiler :	KEIL uVision2 
**
**  Module:  M01030
**
**  Dot Matrix: 128*32
**
**  Display Color: White
**
**  Driver IC: SH1106G
**
**  Edit : 
**
**  Creation date:	
---------------------------------------------------------------------
**/

#ifndef  DISPLAY_IC_
#define  DISPLAY_IC_

//
//
#define high 1
#define low 0 
#define W_Data 0x5C
//
//
//

/*************IC引脚功能定义***************/


#define DISPLAY_RESET    (4)
#define DISPLAY_ENVCC    (1)

//
//
//
#define STATE_MAX 0xFF
#define STATE_MIN 0x00
#define STATE_55 0x55
#define STATE_AA 0xAA
#define START_PAGE 0xB0
#define PAGE_TOTAL 8
#define START_HIGH_BIT 0x10
#define START_LOW_BIT 0x00
#define FRAME_HIGH_ROW 0x01
#define FRAME_LOW_ROW 0x80

#define DISPLAY_MIN 0
#define COLUMN_MAX 128
#define ROW_MAX 32
//
//
#define	DISPLAY_I2C_ADDR 0x78	//while SA==0, if SA==1, addr=0x7A	// 器件地址
#define	DISPLAY_COMMAND_REG 0x00
#define	DISPLAY_DATA_REG 0x40
//
//
#define Manual 1
#define Auto 0
//
//
/**********************************************/
//
//
typedef unsigned int  uint;
typedef unsigned char  uchar;
typedef unsigned long  ulong;

//
//

#endif
