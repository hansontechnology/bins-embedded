
Changes

12/15

Reordering motion sensor sensing routine trying to avoid instable Bluetooth broadcasting lost issue.


10/13

1. [save space] remarked the test codes for stability tests
2. [improve efficiency] sleep tracking timer will be terminated if the sleep tracking function is disabled.
3. [instant responsiveness] if shake-to-UI level is changed, set g-sensor to sleep and wake one time, to reflect the change. 
                            otherwise, the new value needs to be reflected once after g-sensor going to sleep next time.
