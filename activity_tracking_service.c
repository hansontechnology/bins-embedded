/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2012-2013
 *  Part of CSR uEnergy SDK 2.2.2
 *  Application version 2.2.2.0
 *
 *  FILE
 *      health_thermo_service.c
 *
 *  DESCRIPTION
 *      This file defines routines for using Health Thermometer service.
 *
 ******************************************************************************/
 
/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <gatt.h>
#include <gatt_prim.h>
#include <buf_utils.h>
#include <time.h>
#include <reset.h>
#include <app_gatt_db.h>
#include <config_store.h>
#include <mem.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/

#include "ht_hw.h"
#include "app_gatt.h"
#include "activity_tracking_service.h"
#include "nvm_access.h"
#include "app_gatt_db.h"
#include "pedometer.h"
#include "display.h"
#include "touch.h"
#include "sleep_monitoring.h"
#include "motion.h"
#include "hr_sensor.h"
#include "ht_gatt.h"
#include "sleep_monitoring.h"

/*============================================================================*
 *  Private Data Types
 *============================================================================*/


//--** test
// uint16 second_boot;

/*============================================================================*
 *  Private Data
 *============================================================================*/

/* Health Thermometer service data instance */
AT_SERV_DATA_T g_at_serv_data;  //-- 150527 become global

/*============================================================================*
 *  Private Definitions
 *===========================================================================*/

/* Number of words of NVM memory used by Health Thermometer service */
#ifdef CONFIG_IBEACON
#define ACTIVITY_TRACKING_SERVICE_NVM_MEMORY_WORDS      (17+(BEACON_UUID_LENGTH/2)) 
                                                        // 150501 from 20, big enough to include beacon uuid
                                                        // BEACON_UUID_LENGTH is 20 bytes
#else
#define ACTIVITY_TRACKING_SERVICE_NVM_MEMORY_WORDS      (17)    
#endif


/* The offset of data being stored in NVM for activity tracking service.
 * This offset is added to activity tracking service offset to NVM region 
 * to get the absolute offset at which this data is stored in NVM
 */
#define AT_NVM_CLIENT_CONFIG_OFFSET                     (0)
#define AT_NVM_LOCAL_CLOCK_OFFSET                       (2)
#define AT_NVM_ACTIVITY_GOAL_OFFSET                     (6)
#define AT_NVM_CONFIGURATION_OFFSET                     (8)
#define AT_NVM_UI_SENSE_OFFSET                          (10)
#define AT_NVM_LINK_LOSS_ALERT_OFFSET                   (11)
#define AT_NVM_UI_GESTURE_ANGLE_OFFSET                  (12)
#define AT_NVM_UI_GESTURE_WTIME_OFFSET                  (13)
#define AT_NVM_SLEEP_CYCLE_TIME_OFFSET                  (14)
#define AT_NVM_CLOCK_ALARM_OFFSET                       (15)
#define AT_NVM_BEACON_UUID_OFFSET                       (16)



/*============================================================================*
 *  Private Function Implementations
 *===========================================================================*/

#include <random.h>

//-- #undef CONFIG_TARGET_BINS        //--** test

#ifndef CONFIG_TARGET_BINS

static uint8 demoActivityUploaded;
static uint16 demoActivityValueSeq;
static uint16 demoActivityTimeStampSeq;

#define DEMO_TIMESTAMP_MAX (24*60*1)     //-- three days

static uint16 GetDemoActivityValue(void)
{
    uint16 timeStamp;
    uint16 result;

    if(demoActivityUploaded==TRUE) return 0;

    timeStamp= (( DEMO_TIMESTAMP_MAX-demoActivityTimeStampSeq) ) ;

    if(timeStamp<10)  //-- less than 10 minutes from now
    { 
        demoActivityTimeStampSeq=1;
        demoActivityValueSeq=1;
        demoActivityUploaded=TRUE;
        
        return 0;
    }
  
    //-- ELSE

    demoActivityValueSeq++;

    if(demoActivityValueSeq>100) demoActivityValueSeq=1;

    result=((((demoActivityValueSeq*Random16()))& 0x00ff)); //-- max. value cannot > 256 steps / minutes 

    return result;
}

static uint16 GetDemoActivityTime(void)
{
    uint16 timeStamp;

    if(demoActivityUploaded==TRUE) return 0;
    
    timeStamp=DEMO_TIMESTAMP_MAX-demoActivityTimeStampSeq;

    demoActivityTimeStampSeq++;
        
    return timeStamp;
}

static uint32 GetDemoActivityRecord(void)
{
    uint16 sTime=GetDemoActivityTime();
    uint16 sValue=GetDemoActivityValue();   
    uint32 netValue;
    
    netValue=sTime;
    netValue=(netValue<<16) +sValue;
    
    return netValue;
}

static bool GetDemoHasNewActivity(void)
{
    bool hasNew;
    if(demoActivityUploaded==TRUE) hasNew=FALSE;
    else hasNew=TRUE;
 
    demoActivityUploaded=FALSE; //-- reset
    
    return hasNew;
}

static void DemoActivityInit(void)
{
    demoActivityUploaded=FALSE;
    demoActivityTimeStampSeq=1;
    demoActivityValueSeq=1;
        
}

#else  //-- CONFIG_TARGET_BINS

#if 0   //-- 09.13 not use anymore            

#include "pedometer.h"

static uint16 GetActivityTime(void)
{
    StepRecord *sr=stepRecordGetFromTail();
    if(sr==NULL) return 0;

    return systemTimeInMinute()-StepRecordMinute(sr); 
    //-- In this way, that can sustain up to 45 days (track past five days), without wrapped.
}    
    
static uint16 GetActivityValue(void)
{
    uint16 sCount;
    
    StepRecord *sr=stepRecordAvailGetFromTail();
    if(sr==NULL) return 0;
    
    sCount=sr->stepCount;
    
    StepRecordMarkUploaded(sr);
    
    return sCount;
}

#endif //-- 09.13 not use anymore 

static uint32 GetActivityRecord(void)
{
    uint16 sCount;
    uint16 mShift;
    uint32 rPair;
    
    StepRecord *sr=stepRecordAvailGetFromTail();
    if(sr==NULL) return 0;
    
    sCount=STEP_COUNT(sr);   
    mShift=(systemTimeInMinute()-StepRecordMinute(sr));
    if(ACTIVITY_TYPE_BIT15(sr)!=0){
        rPair= 0x8000;    
    }
    else{
        rPair= 0x0000;
    }   
    rPair= rPair | (mShift & 0x7FFF) ;
    rPair=(rPair << 16) + sCount;
    
    //-- $rPair= 0x80000000L | sCount;
    
            
    StepRecordMarkUploaded(sr);
     
    return rPair;
}


#endif   //--

/*============================================================================*
 *  Public Function Implementations
 *===========================================================================*/

void atDeviceEventInit(void);
void setIbeaconUuid(void)
{
    BD_ADDR_T bdaddr;

    //-- 150517 use bluetooth address as ibeacon proxi id
             
    if(CSReadBdaddr(&bdaddr))
    {
        uint16 *proxi_uuid=(&(g_at_serv_data.beacon_uuid[0]));  //-- TODO VERIFY, [4]??->[0] still correct..                            

        proxi_uuid[0] = (bdaddr.lap & 0xFFFF);
        proxi_uuid[1] = (bdaddr.lap >> 16);
        proxi_uuid[2] = ((bdaddr.uap));
        proxi_uuid[3] = (bdaddr.nap & 0xFFFF);
        proxi_uuid[4] = 0;                   
    }
    
#if 0           
            for(i=0; i<5; i++){
                //-- endian mode change
                uint16 userKey=CSReadUserKey(i);
                uint16 temp;
                temp=(userKey&0x00FF)<<8;
                temp=temp+((userKey&0xFF00)>>8);
                
                g_at_serv_data.beacon_uuid[4+i]=temp;
            }
                        /*
            //-- 150502 test
            for(i=0; i<10; i++)
               g_at_serv_data.beacon_uuid[i]=i;
            */

#endif

}

extern void ActivityTrackingReadDataFromNVM(bool nvm_fresh_start, uint16 *p_offset)
{
     g_at_serv_data.nvm_offset = *p_offset;

    /* Read NVM only if devices are bonded */
    if(AppIsDeviceBonded())
    {

        /* Read Heart Rate Measurement Client Configuration */
        Nvm_Read((uint16*)&g_at_serv_data.activity_client_config,
                 sizeof(g_at_serv_data.activity_client_config),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_CLIENT_CONFIG_OFFSET);

    }

//--**  test    
        Nvm_Read(&g_at_serv_data.activity_goal, 
                 sizeof(g_at_serv_data.activity_goal),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_ACTIVITY_GOAL_OFFSET);
/* 10.14    
        if(g_at_serv_data.activity_goal==49){
            // uiAlertEnable(TRUE); 
            second_boot=g_at_serv_data.activity_goal;
        }
        else second_boot=0;
*/        
        
//-------------
        
    if(nvm_fresh_start)
    {
        /* As NVM is being written for the first time, update NVM with the 
         * 
         */

        pedometerReset();
        g_at_serv_data.local_clock_base=gLocalClock;
        g_at_serv_data.activity_goal=gActivityGoal;
        g_at_serv_data.configuration=gConfiguration;        
        g_at_serv_data.ui_sense_strength=gUiSenseStrength;
        
#ifdef CONFIG_CLOCK_ALARM
        //-- g_at_serv_data.clock_alarm= (0x8000 | (6)<<8 | 30); // enabled, 06:30m for alarm as default
        g_at_serv_data.clock_alarm= (0x0000 | (6)<<8 | 30); // disabled, 06:30m for alarm as default

        Nvm_Write((uint16*)&g_at_serv_data.clock_alarm, 
                 sizeof(g_at_serv_data.clock_alarm),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_CLOCK_ALARM_OFFSET);
#endif
        
                       
        Nvm_Write((uint16*)&g_at_serv_data.local_clock_base, 
                 sizeof(g_at_serv_data.local_clock_base),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_LOCAL_CLOCK_OFFSET);

        Nvm_Write(&g_at_serv_data.activity_goal, 
                 sizeof(g_at_serv_data.activity_goal),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_ACTIVITY_GOAL_OFFSET);

        Nvm_Write(&g_at_serv_data.configuration, 
                 sizeof(g_at_serv_data.configuration),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_CONFIGURATION_OFFSET);

        Nvm_Write((uint16*)&g_at_serv_data.ui_sense_strength, 
                 sizeof(g_at_serv_data.ui_sense_strength),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_UI_SENSE_OFFSET);
/*        
        Nvm_Write((uint16*)&g_at_serv_data.link_loss_alert, 
                 sizeof(g_at_serv_data.link_loss_alert),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_LINK_LOSS_ALERT_OFFSET);
*/

        // 150621
        g_at_serv_data.sleep_cycle_time=SLEEP_CHECK_CYCLE_DEFAULT;
        Nvm_Write((uint16*)&g_at_serv_data.sleep_cycle_time, 
                 sizeof(g_at_serv_data.sleep_cycle_time),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_SLEEP_CYCLE_TIME_OFFSET);

#ifdef CONFIG_UI_GESTURE_TRACK  // 150527
        g_at_serv_data.ui_gesture_angle=DEFAULT_UI_GESTURE_ANGLE;   // G delta
        g_at_serv_data.ui_gesture_wait_time=DEFAULT_UI_GESTURE_WTIME;   // in milisecond
        
        Nvm_Write(g_at_serv_data.ui_gesture_angle, 
                 sizeof(g_at_serv_data.ui_gesture_angle),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_UI_GESTURE_ANGLE_OFFSET);
        
        Nvm_Write(g_at_serv_data.ui_gesture_wait_time, 
                 sizeof(g_at_serv_data.ui_gesture_wait_time),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_UI_GESTURE_WTIME_OFFSET);                
#endif          
        
    }
    else
    {
        Nvm_Read((uint16*)&g_at_serv_data.local_clock_base, 
                 sizeof(g_at_serv_data.local_clock_base),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_LOCAL_CLOCK_OFFSET);

        Nvm_Read(&g_at_serv_data.activity_goal, 
                 sizeof(g_at_serv_data.activity_goal),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_ACTIVITY_GOAL_OFFSET);

        Nvm_Read(&g_at_serv_data.configuration, 
                 sizeof(g_at_serv_data.configuration),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_CONFIGURATION_OFFSET);
/*
        Nvm_Read((uint16*)&g_at_serv_data.link_loss_alert, 
                 sizeof(g_at_serv_data.link_loss_alert),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_LINK_LOSS_ALERT_OFFSET);
*/        
        //-- recover from last time
        
        clockSync(g_at_serv_data.local_clock_base);
        
        gActivityGoal=g_at_serv_data.activity_goal;
        gUiSenseStrength=g_at_serv_data.ui_sense_strength;
        gConfiguration=g_at_serv_data.configuration;        

        gShakeThreshold=getShakeThresholdFromConfig(gConfiguration); // 10.11
        if(gShakeThreshold==0) gShakeThreshold=SHAKE_THRESHOLD_LEVEL_DEFAULT; //-- 10.11 in case something wrong

#ifdef CONFIG_UI_GESTURE_TRACK  // 150527
        Nvm_Read(g_at_serv_data.ui_gesture_angle, 
                 sizeof(g_at_serv_data.ui_gesture_angle),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_UI_GESTURE_ANGLE_OFFSET);
        
        Nvm_Read(g_at_serv_data.ui_gesture_wait_time, 
                 sizeof(g_at_serv_data.ui_gesture_wait_time),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_UI_GESTURE_WTIME_OFFSET); 
#endif        
        //-- 150621
        Nvm_Read(&g_at_serv_data.sleep_cycle_time, 
                 sizeof(g_at_serv_data.sleep_cycle_time),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_SLEEP_CYCLE_TIME_OFFSET);  
        
        if((g_at_serv_data.sleep_cycle_time & 0xFF) > SLEEP_CHECK_CYCLE_MAX){ 
            g_at_serv_data.sleep_cycle_time=SLEEP_CHECK_CYCLE_MAX;    
            //- 20150815 in case, wrong number from firmware upgrade
        }
        
#ifdef CONFIG_CLOCK_ALARM
         Nvm_Read(&g_at_serv_data.clock_alarm, 
                 sizeof(g_at_serv_data.clock_alarm),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_CLOCK_ALARM_OFFSET);              
#endif
    }
    
#ifdef CONFIG_IBEACON
    //-- write it everytime, in case different version firmware multiple rom burning
    setIbeaconUuid();
    
    /*
    Nvm_Write(g_at_serv_data.beacon_uuid, 
                 BEACON_UUID_LENGTH/2,
                 g_at_serv_data.nvm_offset +
                 AT_NVM_BEACON_UUID_OFFSET);

    Nvm_Read(g_at_serv_data.beacon_uuid, 
                 BEACON_UUID_LENGTH/2,
                 g_at_serv_data.nvm_offset +
                 AT_NVM_BEACON_UUID_OFFSET);
    */
        
    atDeviceEventInit();    // 150505
    
    {
        int i;
        uint8 bytes[26];
        for(i=0; i<10; i++){
            bytes[i*2]=g_at_serv_data.beacon_uuid[i] & 0x00FF;
            bytes[i*2+1]= (g_at_serv_data.beacon_uuid[i] & 0xFF00)>>8;    
        }
        GattAdvertisingBeaconUpdate(bytes);
    }
#endif
    g_at_serv_data.link_loss_alert=0x00; // reset
    
    /* Increment the offset by the number of words of NVM memory required 
     * by the service 
     */
    *p_offset += ACTIVITY_TRACKING_SERVICE_NVM_MEMORY_WORDS;

}



/*----------------------------------------------------------------------------*
 *  NAME
 *      HealthThermoDataInit
 *
 *  DESCRIPTION
 *      This function is used to initialise Health Thermometer service data 
 *      structure.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

extern void ActivityTrackingDataInit(void)
{
    if(!AppIsDeviceBonded())
    {
        /* Initialise Temperature Characteristic Client Configuration 
         * only if device is not bonded
         */
        g_at_serv_data.activity_client_config = gatt_client_config_none;
    }

    g_at_serv_data.ind_cfm_pending =FALSE;

#ifndef CONFIG_TARGET_BINS    
    DemoActivityInit(); //--
#endif
    
    //-- gLocalConfiguration=0x0100;
    
}
    

/*----------------------------------------------------------------------------*
 *  NAME
 *      HealthThermoHandleAccessRead
 *
 *  DESCRIPTION
 *      This function handles read operation on health thermometer service 
 *      attributes maintained by the application and responds with the 
 *      GATT_ACCESS_RSP message.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

extern void ActivityTrackingHandleAccessRead(GATT_ACCESS_IND_T *p_ind)
{
    uint16 length = 0;

#ifdef CONFIG_TARGET_BINSZ3_HBP
    uint16 sleepRecordBuffer[MAX_SLEEP_DATA_UPLOAD_SIZE];
#endif
    
#if defined(CONFIG_IBEACON) || defined(CONFIG_TARGET_BINSZ3_HBP)
    //-- 150605, MAX_SLEEP_DATA_UPLOAD_SIZE is biggest one required
    //--  equal to 20 bytes , same as ibeacon need
    
    uint8 val[20];
#else
    uint8  val[8]; //-- max length of data for read 
#endif
    uint8 *p_value = NULL;
    sys_status rc = sys_status_success;

    switch(p_ind->handle)
    {

        case HANDLE_AT_ACTIVITY_TYPE:
            p_value=val;
            length=1;
            p_value[0]=1;   //-- default is 1                        
        break;
        
        case HANDLE_AT_ACTIVITY_VALUE:
            p_value=val;
            length=2;
#if 0   //-- 09.13 not use anymore            
#ifndef CONFIG_TARGET_BINS
            BufWriteUint16((uint8 **)&p_value, GetDemoActivityValue());                       
#else
            BufWriteUint16((uint8 **)&p_value, GetActivityValue());                       
#endif
#endif
            
        break;
        
        case HANDLE_AT_ACTIVITY_TIME:
            p_value=val;
            length=2;
#if 0   //-- 09.13 not use anymore            
            
#ifndef CONFIG_TARGET_BINS
            BufWriteUint16((uint8 **)&p_value, GetDemoActivityTime()); 
#else
            BufWriteUint16((uint8 **)&p_value, GetActivityTime()); 
#endif
#endif
            
        break;
       
       case HANDLE_AT_ACTIVITY_TIME_VALUE:
        {
            uint32 sValue;
            p_value=val;
            length=4;
#ifndef CONFIG_TARGET_BINS
            sValue=GetDemoActivityRecord();                       
#else
            sValue=GetActivityRecord();
#endif
           //-- test sValue=0x0000;
            
            BufWriteUint32((uint8 **)&p_value, &sValue); 
        }
        break;
        
        case HANDLE_AT_NEW_ACTIVITY:
            p_value=val;
            length=1;
#ifndef CONFIG_TARGET_BINS
            p_value[0]=GetDemoHasNewActivity();                       
#else
            p_value[0]=1;
#endif
       break;

       case HANDLE_AT_LOCAL_CLOCK:
            p_value=val;
            length=2;
            gLocalClock= (gLocalHour << 8) | gLocalMinute;
            BufWriteUint16((uint8 **)&p_value, gLocalClock);
       break;
/*
       case HANDLE_AT_TIMER:
       {
            // uint16 currentTime[3],diffTime[3];
            uint32 diffMinutes;
            // TimeGet48WithOffset(currentTime,0);
            // TimeSub48(diffTime,currentTime,gFuncTimerBase);
            diffMinutes=systemTimeInMinute()-gFuncTimerBase;
            length=4;
            BufWriteUint32((uint8 **)&p_value, &diffMinutes);                                            
       }
       break;
*/
       
       
       case HANDLE_AT_LINK_LOSS_ALERT:
            p_value=val;
            length=1;
            p_value[0]=g_at_serv_data.link_loss_alert;
        break;
        
       case HANDLE_AT_SETTING_CFG:
            p_value=val;
            length=2;
            BufWriteUint16((uint8 **)&p_value, gConfiguration);                                            
            g_at_serv_data.configuration=gConfiguration;
            //-- uiActionStart();
        break;       
        
#ifdef CONFIG_TARGET_BINSZ3_HBP
        case HANDLE_SLEEP_RECORD_TIME_SHIFT:
        {
            uint16 timeShift;            
            p_value=val;
            length=2;
            if(sleepRecordAvailSize()>0){
                timeShift=sleepTimeShiftInMinute();
                timeShift|= 0x8000;
            }else{
                timeShift=0;
            }
            BufWriteUint16((uint8 **)&p_value, timeShift); 
        }    
        break;
        case HANDLE_SLEEP_RECORDS:
        {
            uint16 numActualRead;
            uint16 numLeft;    
            
            p_value=val;
            length=2;

            numLeft=sleepRecordGet(sleepRecordBuffer, &numActualRead);
    
            {
                uint16 i=0;
                for(i=0; i<numActualRead; i++){
                    val[2+i*2]=sleepRecordBuffer[i] & 0xFF;
                    val[2+i*2+1]= (sleepRecordBuffer[i] & 0xFF00)>>8;                    
                }
            }
            
            if(numLeft>0) p_value[0]=1;
            else p_value[0]=0; 
            p_value[1]=(numLeft & 0xff);    //--150619
            length=2;
            
            length+=(2*numActualRead);
            
        }                   
        break;
#endif //-- CONFIG_TARGET_BINSZ3_HBP
#ifdef CONFIG_IBEACON
        case HANDLE_IBEACON_UUID:
        {
            int i=0;            
            p_value=val;
            for(i=0; i<10; i++){
               val[i*2]=(g_at_serv_data.beacon_uuid[i]&0x00FF);
               val[i*2+1]=(g_at_serv_data.beacon_uuid[i]&0xFF00 ) >>8;                       
            }                      
            length=20;            
        }                   
        break;
#endif
        default:
            rc = gatt_status_read_not_permitted;
        break;
        

    }

    GattAccessRsp(p_ind->cid, p_ind->handle, rc,
                  length, val);

}

// 10.11
int getShakeThresholdFromConfig(uint8 config)
{
    int threshValue=0;
    if(config & 0x8000) threshValue |= 0x4;
    if(config & 0x0800) threshValue |= 0x2;
    if(config & 0x0010) threshValue |= 0x1;

    return threshValue;
}
void setShakeThresholdToConfig(int8 threshValue)
{
    uint16 config=gConfiguration;
    
    if(threshValue & 0x04) config |= 0x8000; else config &= (~0x8000); 
    if(threshValue & 0x02) config |= 0x0800; else config &= (~0x0800); 
    if(threshValue & 0x01) config |= 0x0010; else config &= (~0x0010); 

    gConfiguration=config;
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      HealthThermoHandleAccessWrite
 *
 *  DESCRIPTION
 *      This function handles write operation on health thermometer service 
 *      attributes maintained by the application.and responds with the 
 *      GATT_ACCESS_RSP message.
 *
 *  RETURNS
 *      Nothing
 *
 *---------------------------------------------------------------------------*/
#define ALERT_MESSAGE_MASK  0x40
#define ALERT_LOST_MASK     0x80

extern void ActivityTrackingHandleAccessWrite(GATT_ACCESS_IND_T *p_ind)
{
    uint8 *p_value = p_ind->value;
 
    sys_status rc = sys_status_success;

    switch(p_ind->handle)
    {
        case HANDLE_AT_LOCAL_CLOCK:
             g_at_serv_data.local_clock_base=BufReadUint16(&p_value);
             clockSync(g_at_serv_data.local_clock_base);
             
             //-- cannot do it ! if(StepRecordCount()==0) systemTimeReset();   //-- a chance to reset to 0;
             
#if 1
             Nvm_Write((uint16*)&g_at_serv_data.local_clock_base, 
                 sizeof(g_at_serv_data.local_clock_base),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_LOCAL_CLOCK_OFFSET);            
#endif
        break;
/*        
        case HANDLE_AT_TIMER:
            //-- TimeGet48WithOffset(gFuncTimerBase,0); //-- to reset function timer to current sys time.
            gFuncTimerBase=systemTimeInMinute();
        break;        
*/
        
#ifndef CONFIG_TARGET_BINSX2        
        case HANDLE_AT_UI_SENSITIVITY:
        {
            gUiSenseStrength=BufReadUint32(&p_value);
            g_at_serv_data.ui_sense_strength=gUiSenseStrength;
        }
        break;
#endif
        
        
        case HANDLE_AT_ACTIVITY_GOAL:
        {
            uint16 newGoal;
            newGoal=BufReadUint16(&p_value); 
            if(newGoal>0)
            {
                gActivityGoal=newGoal;
                g_at_serv_data.activity_goal=gActivityGoal;            
#if 1
                /* Write to NVM  */
                Nvm_Write((uint16*)&g_at_serv_data.activity_goal,
                    sizeof(g_at_serv_data.activity_goal),
                    g_at_serv_data.nvm_offset + 
                    AT_NVM_ACTIVITY_GOAL_OFFSET);
#endif
            }
        }
        break;        

        case HANDLE_AT_LINK_LOSS_ALERT:
            g_at_serv_data.link_loss_alert=p_value[0];
            // no need to store in NVM
            {
                uint8 alertContent=p_value[0];
                if(alertContent & ALERT_MESSAGE_MASK){  //-- 0x40
#ifndef CONFIG_TARGET_BINSX2
                    if((alertContent & 0xF)>0){
                        uiMessageNotice(TRUE);
                    } else {
                        uiMessageNotice(FALSE);
                    }   
#else
                    if((alertContent & 0xF)>0){
                       uiDisplayMessage(TRUE);
                    } else {
                       uiDisplayMessage(FALSE);                        
                    }                     
#endif                    
                }

                else    //-- 0x00 or 0x80
                    //-- 09.18 backward compatible if(alertContent & ALERT_LOST_MASK)
                { 
#ifndef CONFIG_TARGET_BINSX2
                    if((g_at_serv_data.link_loss_alert & 0x0F)>0){
                        uiAlertEnable(TRUE);
                    }
                    else{
                        uiAlertEnable(FALSE);
                    }
#else   
                    if((g_at_serv_data.link_loss_alert & 0x0F)>0){
                        uiDisplayAlert(TRUE);
                    }
                    else{
                        uiDisplayAlert(FALSE);
                    }                    
#endif
                }
            }            
        break;
        
        case HANDLE_AT_SETTING_CFG:
        
        // for test only 
        //-- uiVibrationStart(1000); // 12.22
        
        //-- set configuration
        {
            uint16 oldConfiguration=gConfiguration;            
            uint16 newConfig=BufReadUint16(&p_value);
            
#ifdef CONFIG_TARGET_BINSX2
            uint16 STEP_WATCH_DISABLE_COMMAND=BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK 
                                   | BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK;

            if((newConfig & STEP_WATCH_DISABLE_COMMAND)== STEP_WATCH_DISABLE_COMMAND){
                dayStepCounterReset(FALSE);
                break;
            }
            else if((newConfig&BINS_CONFIG_COMMAND_HRD_START_MASK)!=0){
                uiDisplayShow(FALSE);
                touchHeartRateStartAll();
                break;
            }
            else if((newConfig&BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK)!=0){
                dayStepCounterReset(TRUE);
                break;
            }
/*  10.10          
            else if((newConfig&BINS_CONFIG_UI_WAKEUP_THRESHOLD_DECREASE_MASK)!=0){
                shakeThresholdChange(-1);
                break;
            }
            else if((newConfig&BINS_CONFIG_UI_WAKEUP_THRESHOLD_INCREASE_MASK)!=0){
                shakeThresholdChange(1);
                break;
            }
*/
#if 0            
            else if((newConfig&BINS_CONFIG_COMMAND_SYS_RESET_MASK)!=0){
                systemResetAndRecovery();
            }
#endif
            
#endif      // CONFIG_TARGET_BINSX2
            
            // 10.13
            if((newConfig&BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK)!=0){
                gUILocked=TRUE;
                break;
            }
            if(gUILocked==TRUE){
                gUILocked=FALSE; //-- 10.09
                motionWakeup();
            }            
            //-- to check for no change
            {
                int newShakeThresh=getShakeThresholdFromConfig(newConfig);  
                if(newShakeThresh>0 && gShakeThreshold!=newShakeThresh){
                    gShakeThreshold=newShakeThresh;
                    motionSleep(); //-- 10.15
                    // shakeThresholdCheck();
                }
            }
                
            if(gConfiguration!=(newConfig & BINS_CONFIG_SETTING_ALL_MASK))  //-- 10.08
            {
                gConfiguration=newConfig & BINS_CONFIG_SETTING_ALL_MASK;            
                g_at_serv_data.configuration=gConfiguration;

                Nvm_Write(&g_at_serv_data.configuration, 
                 sizeof(g_at_serv_data.configuration),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_CONFIGURATION_OFFSET); 
 
                //-- 09.26 sleep monitoring
                if((oldConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)==0 &&
                       (gConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)!=0       )
                {

#ifdef CONFIG_TARGET_BINSZ3_HBP
                    sleepRecordReset();
                    lightDataQueueInit();
#endif
                    sleepCheckNewCycle();
                }           
                else if((oldConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)!=0 &&
                       (gConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)==0       )
                {
                    sleepCheckStop();   // 10.14
                } 
            }
           
            //-- if(gConfiguration & BINS_CONFIG_LED_MASK_UI_ONETIME) 
            if((newConfig & BINS_CONFIG_COMMAND_UI_ONETIME_BIT_MASK)!=0){
#ifndef CONFIG_TARGET_BINSX2
                 //-- 09.21 save power,
                 uiActionStart();
#else
                 //-- 09.21 save power, 
                 uiDisplayShow(FALSE);   //-- 09.15, 08.19
#endif
            }
            
            
#ifdef CONFIG_IBEACON
            {
                //-- here means the connection has been setup already
        
                DeviceEventStatus *eStatus=atGetDeviceEvent();
                if(eStatus->connectRequest==TRUE){
                    // start it.
                    //-- 150517 no need to measure , touchPowerdown(FALSE);  // leave power down mode
                    //-- touchHeartRateStartMeasure();                           
                    atDeviceEvent(AT_DEVICE_EVENT_CONNECT_REQUEST, FALSE);
                    g_at_serv_data.connectByDevice=TRUE;
                } else {
                    g_at_serv_data.connectByDevice=FALSE;                    
                }
            }
#endif            
            
        }
        break;
        
#ifdef CONFIG_TARGET_BINSZ3_HBP        
        case HANDLE_SLEEP_RECORD_TIME_SHIFT:
        {
            g_at_serv_data.sleep_cycle_time= g_at_serv_data.sleep_cycle_time & 0xFF00;
            g_at_serv_data.sleep_cycle_time|= p_value[0];   // the first byte is for sleep check cycle time 
            
            Nvm_Write((uint16*)g_at_serv_data.sleep_cycle_time, 
                 sizeof(g_at_serv_data.sleep_cycle_time),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_SLEEP_CYCLE_TIME_OFFSET);            
        }    
        break;
#endif
        
#if 0 //-- 20150815 no need this anymore #ifdef CONFIG_IBEACON
        
        case HANDLE_IBEACON_UUID:
        {
            int i;
            for(i=0; i<20; i++){
                g_at_serv_data.beacon_uuid[i]=p_value[i];
            }
        }
        
        Nvm_Write((uint16*)g_at_serv_data.beacon_uuid, 
                 10,
                 g_at_serv_data.nvm_offset +
                 AT_NVM_BEACON_UUID_OFFSET); 

        break;
                  
#endif
        
#ifdef CONFIG_UI_GESTURE_TRACK
        
        case HANDLE_AT_UI_GESTURE_ANGLE:
        {
            g_at_serv_data.ui_gesture_angle=BufReadUint16(&p_value);         
            Nvm_Write((uint16*)&g_at_serv_data.ui_gesture_angle, 
                 sizeof(g_at_serv_data.ui_gesture_angle),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_UI_GESTURE_ANGLE_OFFSET); 
        }
        break;
                          
        case HANDLE_AT_UI_GESTURE_WTIME:
        {
            g_at_serv_data.ui_gesture_wait_time=BufReadUint16(&p_value);         
            Nvm_Write((uint16*)&g_at_serv_data.ui_gesture_wait_time, 
                 sizeof(g_at_serv_data.ui_gesture_wait_time),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_UI_GESTURE_WTIME_OFFSET); 
        }
        break;
                  
#endif

#ifdef CONFIG_CLOCK_ALARM
        case HANDLE_AT_CLOCK_ALARM:
        {
           g_at_serv_data.clock_alarm=BufReadUint16(&p_value);         
           Nvm_Write((uint16*)&g_at_serv_data.clock_alarm, 
                 sizeof(g_at_serv_data.clock_alarm),
                 g_at_serv_data.nvm_offset +
                 AT_NVM_CLOCK_ALARM_OFFSET);             
        }
        break;
#endif
        
#ifdef CONFIG_TARGET_BINSX2
        
        case HANDLE_HR_HOST_UPDATE:
            // no need to store in NVM
        {
            uint8 newHR=p_value[0];
            touchHeartRateUpdate(newHR);
            uiDisplayHeartRateNotice(FALSE);   // only update if it has number
#ifdef CONFIG_IBEACON
#ifdef CONFIG_IBEACON_HEARTRATE
            atDeviceEvent(BEACON_MAJOR_HEARTRATE, TRUE);
#endif
#endif
#ifdef CONFIG_HEARTRATE_SERVICE            
            //-- 11.27
            sendHRMeasurement();    
#endif
        }    
        break;
#endif

        default:
            rc = gatt_status_write_not_permitted;
        break;
    }

    /* Send ACCESS RESPONSE */
    GattAccessRsp(p_ind->cid, p_ind->handle, rc, 0, NULL);

}


/*----------------------------------------------------------------------------*
 *  NAME
 *      HealthThermoSendTempReading
 *
 *  DESCRIPTION
 *      This function is used to send temperature reading as an indication 
 *      to the connected host.
 *
 *  RETURNS
 *      Boolean: TRUE (If temperature reading is indicated to the connected 
 *               host) OR
 *               FALSE (If temperature reading could not be indicated to the 
 *               connected host)
 *
 *---------------------------------------------------------------------------*/




/*----------------------------------------------------------------------------*
 *  NAME
 *      HealthThermoRegIndicationCfm
 *
 *  DESCRIPTION
 *      This function is used to set the status of pending confirmation for the 
 *      transmitted temperature measurement indications
 *
 *  RETURNS
 *      Nothing
 *
 *---------------------------------------------------------------------------*/

extern void ActivityTrackingRegIndicationCfm(bool ind_state)
{
    g_at_serv_data.ind_cfm_pending = ind_state;
}


extern void ActivityTrackingRegNotificationCfm(GATT_CHAR_VAL_IND_CFM_T *p_event_data)
{
    if(p_event_data->result==gatt_status_busy){
        gIsTouchHeartRateQueued=TRUE;
    }
    else{
        gIsTouchHeartRateQueued=FALSE;
    }
}



/*----------------------------------------------------------------------------*
 *  NAME
 *      HealthThermoCheckHandleRange
 *
 *  DESCRIPTION
 *      This function is used to check if the handle belongs to the health 
 *      thermometer service
 *
 *  RETURNS
 *      Boolean - Indicating whether handle falls in range or not.
 *
 *---------------------------------------------------------------------------*/

extern bool ActivityTrackingCheckHandleRange(uint16 handle)
{
    return ((handle >= HANDLE_ACTIVITY_TRACKING_SERVICE) &&
            (handle <= HANDLE_ACTIVITY_TRACKING_SERVICE_END))
            ? TRUE : FALSE;
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      HealthThermoBondingNotify
 *
 *  DESCRIPTION
 *      This function is used by application to notify bonding status to 
 *      health themometer service
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

extern void ActivityTrackingBondingNotify(void)
{

    /* Write data to NVM if bond is established */
    if(AppIsDeviceBonded())
    {
        /* Write to NVM the client configuration value of temperature */
        Nvm_Write((uint16*)&g_at_serv_data.activity_client_config,
                  sizeof(g_at_serv_data.activity_client_config),
                  g_at_serv_data.nvm_offset + 
                  AT_NVM_CLIENT_CONFIG_OFFSET);
    }

}

extern void activityValueNotify(uint16 ucid, StepRecord *sr, uint8 numSteps)
{
    
    //-- 07.28 uint16 sCount=sr->stepCount;   
    uint16 mShift;
    uint32 rPair;
    
    // 10.14
    unsigned char  gVar[8];
    unsigned char  *p_value;

    //-- uint8  var[4];
#if 0
int i;
unsigned char *charPair;
unsigned char c;
#endif

    p_value=gVar;
    
    //-- mShift=(systemTimeInMinute()-StepRecordMinute(sr));
    //-- mShift=systemTimeInMinute();
  
    //-- 150514 mShift=(systemTimeInMinute()-StepRecordMinute(sr));
    //-- 150514  it should always be 0, as this must be the latest time to notify data.  
    mShift=0; 
    
    // rPair= ACTIVITY_TYPE_BIT15(sr);
    //-- sr->stepCount= 0x8000;
    rPair= (sr->stepCount & 0x8000);
    rPair= rPair | (mShift & 0x7FFF);
    rPair= rPair<<16;
    rPair= rPair | (numSteps & 0x07FFF);
    
    //-- test rPair=0x12345678;   
    //-- rPair=0x12345678;
    // rPair=0x80004567;
    
    BufWriteUint32((uint8 **)&p_value, &rPair);


#if 0
    charPair=(unsigned char*)(&rPair);

    for(i=0; i<4; i++)
    {
        c=charPair[3-i];
        p_value[i]=c;
    }
#endif
    
    GattCharValueNotification(ucid, 
                              HANDLE_AT_ACTIVITY_TIME_VALUE, 
                              4, 
                              gVar);  /* heart rate */
    
}

extern void activityLinkLossCheck(void)
{                    
    if((g_at_serv_data.link_loss_alert & 0x80) &&
       (g_at_serv_data.link_loss_alert & 0x0F)>0)  {   
#ifndef CONFIG_TARGET_BINSX2
           uiAlertEnable(TRUE);
#else
           uiDisplayAlert(TRUE);
#endif
    }
}


#define TOUCH_HEARTRATE_DATA_SIZE   19
/*
uint8 gLastHeartRateData[TOUCH_HEARTRATE_DATA_SIZE];
*/

extern void heartRateRawDataNotify(uint16 ucid, uint8 *hrdData)      
{ 
    
#ifdef CONFIG_TARGET_BINSX2 
    
/*    
    if(gIsTouchHeartRateQueued==TRUE){
        GattCharValueNotification(ucid, 
                              HANDLE_HR_DATA_STREAM, 
                              TOUCH_HEARTRATE_DATA_SIZE,       // 13+6 
                              gLastHeartRateData);            
    }
    
    gIsTouchHeartRateQueued=FALSE;
*/
   
    GattCharValueNotification(ucid, 
                              HANDLE_HR_DATA_STREAM, 
                              TOUCH_HEARTRATE_DATA_SIZE,       // 13+6 
                              hrdData);  /* heart rate */    
/*    
    {
        int i;
        for(i=0; i<TOUCH_HEARTRATE_DATA_SIZE; i++){
            gLastHeartRateData[i]=hrdData[i];
        }
    }
*/
    
#endif
    
}

#ifdef CONFIG_IBEACON

#include "battery_service.h"

static timer_id gAtBeaconRequest_tid;
static bool *activeBeaconEvent;
static void atSetBeaconTimer(bool *event);

#ifdef CONFIG_IBEACON_BATTERY_LOW 

//-- for every 5 minutes
static int8 gAtCheckBatteryCounter;
void atCheckBatteryLow(void)
{
    gAtCheckBatteryCounter++;
    if(gAtCheckBatteryCounter>10) gAtCheckBatteryCounter=0;  //-- check every 10 minutes
    if(gAtCheckBatteryCounter==1){
        if(CheckBatteryLevel(TRUE)==FALSE){
            atDeviceEvent(AT_DEVICE_EVENT_BATTERY, TRUE);
        } else {    //-- if leave from low power
            if(g_at_serv_data.eventStatus.batteryLow==TRUE){    //-- if it is still set to low battery
                atDeviceEvent(AT_DEVICE_EVENT_BATTERY, FALSE);  //-- update to quit low battery state
            }
        }
    }
}

#endif // CONFIG_IBEACON_BATTERY_LOW

void atDisconnectByDevice(void)
{
    // more chance to reset it
    atDeviceEvent(AT_DEVICE_EVENT_CONNECT_REQUEST, FALSE);
    atDeviceEvent(AT_DEVICE_EVENT_HEARTRATE_CYCLE, FALSE);

    //-- if(g_at_serv_data.connectByDevice==TRUE)
    {
        GattDisconnectReq(g_ht_data.st_ucid);   
        g_at_serv_data.connectByDevice=FALSE;
    }
}

DeviceEventStatus *atGetDeviceEvent(void)
{   
    return (&g_at_serv_data.eventStatus); 
}

//-- Change beacon output based on current beacon settings, and priority

static void atBeaconEventUpdate(void)
{
    DeviceEventStatus *eStatus=(&g_at_serv_data.eventStatus);
    bool *currentDeviceEvent=NULL;

#ifdef CONFIG_TARGET_BINSZ3_ST  //---------------------------------------------------
    
    if(eStatus->connectRequest==TRUE){
        GattBeaconMajorMinor(BEACON_MAJOR_CONNECT_REQUEST,0);
        currentDeviceEvent=(&eStatus->connectRequest);
    }else if(eStatus->heartRateRequest==TRUE){
        GattBeaconMajorMinor(BEACON_MAJOR_HEARTRATE_CYCLE,0);
        currentDeviceEvent=(&eStatus->heartRateRequest);    //-- FIXed IT 20150919 from connectRequest
    }
#ifdef CONFIG_BUTTON_SOS
    else if(eStatus->longPress==TRUE){ 
        GattBeaconMajorMinor(BEACON_MAJOR_BUTTON_SOS, 0);
        currentDeviceEvent=(&eStatus->longPress);
    }
#endif 
#ifdef CONFIG_FREEFALL_DETECTION
    else if(eStatus->freeDrop==TRUE){
        GattBeaconMajorMinor(BEACON_MAJOR_FREEDROP,0);
        currentDeviceEvent=(&eStatus->freeDrop);
    }
#endif
#ifdef CONFIG_IBEACON_HEARTRATE
    else if(eStatus->heartRate==TRUE){ 
        GattBeaconMajorMinor(BEACON_MAJOR_HEARTRATE, getLatestHeartRate());
        currentDeviceEvent=(&eStatus->heartRate);
    }
#endif       
#ifdef CONFIG_IBEACON_BATTERY_LOW
    else if(eStatus->batteryLow==TRUE){
        GattBeaconMajorMinor(BEACON_MAJOR_BATTERY_LOW,0); 
        currentDeviceEvent=(&eStatus->batteryLow);
    }
#endif
    else{ 
        //-- uiVibrationStart(300);  // for test
        GattBeaconMajorMinor(BEACON_MAJOR_NORMAL,0);
    }

#else   //-------------- NOT SILTRON CASE  -------------------------------------
    
    if(eStatus->connectRequest==TRUE){
        GattBeaconMajorMinor(1, BEACON_MAJOR_CONNECT_REQUEST);
        currentDeviceEvent=(&eStatus->connectRequest);
    }else if(eStatus->heartRateRequest==TRUE){
        GattBeaconMajorMinor(1, BEACON_MAJOR_HEARTRATE_CYCLE);
        currentDeviceEvent=(&eStatus->heartRateRequest);    //-- FIXed IT 20150919 from connectRequest        
    }else if(eStatus->batteryLow==TRUE){        // 150827
        GattBeaconMajorMinor(1, BEACON_MAJOR_BATTERY_LOW);
        currentDeviceEvent=(&eStatus->batteryLow);
    }
    
#ifdef CONFIG_BUTTON_SOS
    else if(eStatus->longPress==TRUE){ 
        GattBeaconMajorMinor(1, BEACON_MAJOR_BUTTON_SOS);
        currentDeviceEvent=(&eStatus->longPress);
    }
#endif 
#ifdef CONFIG_FREEFALL_DETECTION
    else if(eStatus->freeDrop==TRUE){
        GattBeaconMajorMinor(1, BEACON_MAJOR_FREEDROP);
        currentDeviceEvent=(&eStatus->freeDrop);
    }
#endif
#ifdef CONFIG_IBEACON_HEARTRATE
    else if(eStatus->heartRate==TRUE){ 
        GattBeaconMajorMinor(BEACON_MAJOR_HEARTRATE, getLatestHeartRate());
        currentDeviceEvent=(&eStatus->heartRate);
    }
#endif       
#ifdef CONFIG_IBEACON_BATTERY_LOW
    else if(eStatus->batteryLow==TRUE){
        GattBeaconMajorMinor(1, BEACON_MAJOR_BATTERY_LOW); 
        currentDeviceEvent=(&eStatus->batteryLow);
    }
#endif
    else{ 
        //-- uiVibrationStart(300);  // for test
        GattBeaconMajorMinor(0, BEACON_MAJOR_NORMAL);
    }    
    
#endif  //-----------------------------------------------------------------------
    
    if(currentDeviceEvent!=NULL){
        atSetBeaconTimer(currentDeviceEvent);
    }
}

static void atBeaconRequestTimerHandler(timer_id tid)
{
    //--  uiDisplayShow(FALSE);  // for test 150602

    if(tid!=gAtBeaconRequest_tid) return;
    gAtBeaconRequest_tid=TIMER_INVALID;
    *activeBeaconEvent=FALSE;
    activeBeaconEvent=NULL;
    atBeaconEventUpdate();
}

#define IBEACON_CONNECT_REQUEST_EVENT_TIME  (30*SECOND)
#define IBEACON_BATTERY_LOW_EVENT_TIME (30*SECOND)
#define DEFAULT_IBEACON_EVENT_TIME (10*MINUTE)

static void atSetBeaconTimer(bool *event)
{
    uint32 activeTime=0;
    
    DeviceEventStatus *eStatus=atGetDeviceEvent();
    
    if((&eStatus->connectRequest)==event){
        activeTime=IBEACON_CONNECT_REQUEST_EVENT_TIME;
    } else if((&eStatus->heartRateRequest)==event){
        activeTime=IBEACON_CONNECT_REQUEST_EVENT_TIME;        
    }
#ifdef CONFIG_BUTTON_SOS
    else if((&eStatus->longPress)==event){
 
        activeTime=DEFAULT_IBEACON_EVENT_TIME;
    }    
#endif
#ifdef CONFIG_FREEFALL_DETECTION
    else if((&eStatus->freeDrop)==event){
        activeTime=DEFAULT_IBEACON_EVENT_TIME;
    }
#endif
#ifdef CONFIG_IBEACON_HEARTRATE
    else if((&eStatus->heartRate)==event){
        activeTime=DEFAULT_IBEACON_EVENT_TIME;        
    }
#endif    
#ifdef CONFIG_IBEACON_BATTERY_LOW
    else if((&eStatus->batteryLow)==event){
        //-- 150830
        //-- per cycle time, 10 minutes as example, 
        //-- atCheckBatteryLow() will check and trigger this ibeacon
        //-- and this also will start advertising and last for 40 seconds
#ifdef CONFIG_PUSH_ADVERTISING
        PushAdvertisingEnable();    
#endif
        activeTime=IBEACON_BATTERY_LOW_EVENT_TIME;
    }
#endif    
    if(activeTime>0 && activeBeaconEvent!=event){
        
        if(gAtBeaconRequest_tid!=TIMER_INVALID){
            TimerDelete(gAtBeaconRequest_tid);
            gAtBeaconRequest_tid=TIMER_INVALID;
            if(activeBeaconEvent!=event)
                *activeBeaconEvent=FALSE;   //-- early recovery from event set
        }
        activeBeaconEvent=event;
        
        //-- 150505 will halt system, GattStartAdverts(TRUE);    //-- 150502  to start advertising right away
        //-- uiDisplayShow(TRUE);  // for test 150602

        gAtBeaconRequest_tid=TimerCreate(activeTime, TRUE, atBeaconRequestTimerHandler); 
        
/*        if(gAtBeaconRequest_tid==TIMER_INVALID){
            //-- touchPowerdown(FALSE);
        }
*/
        
    }
}

void atDeviceEvent(AtDeviceEvent event, bool newStatus)
{    
    DeviceEventStatus *eStatus=(&g_at_serv_data.eventStatus);
    bool *currentDeviceEvent=NULL;
    
    switch (event) {
#ifdef CONFIG_IBEACON_BATTERY_LOW        
    case AT_DEVICE_EVENT_BATTERY:
        currentDeviceEvent= (&eStatus->batteryLow);
        break;
#endif
#ifdef CONFIG_BUTTON_SOS
    case AT_DEVICE_EVENT_LONGPRESS:
        currentDeviceEvent= (&eStatus->longPress);
        break;    
#endif
#ifdef CONFIG_FREEFALL_DETECTION
    case AT_DEVICE_EVENT_FREEDROP:
        currentDeviceEvent= (&eStatus->freeDrop);  
        
        //-- uiDisplayShow(FALSE);  // for test 150602
        
        break;       
#endif 
    case AT_DEVICE_EVENT_CONNECT_REQUEST:
        currentDeviceEvent= (&eStatus->connectRequest);
        break;      
    case AT_DEVICE_EVENT_HEARTRATE_CYCLE:
        currentDeviceEvent= (&eStatus->heartRateRequest);
        break;
    default:
        // do nothing
        break;
    }
    
    if(currentDeviceEvent==NULL){
        return;
    }    
     
    (*currentDeviceEvent)=newStatus;
    
    // if a new beacon request with the same as running one
    if(gAtBeaconRequest_tid!=TIMER_INVALID && currentDeviceEvent==activeBeaconEvent){
        TimerDelete(gAtBeaconRequest_tid);
        gAtBeaconRequest_tid=TIMER_INVALID;
        activeBeaconEvent=NULL; //-- tell it to restart this event beacon 
    }  
    atBeaconEventUpdate();
}

void atDeviceEventInit(void)
{
    DeviceEventStatus *eStatus=(&g_at_serv_data.eventStatus);
    MemSet(eStatus, 0, sizeof(eStatus));  
    gAtBeaconRequest_tid=TIMER_INVALID;
}

#endif //-- END CONFIG_IBEACON

void ShortToByteArray(uint8 *bp, int16 sv)
{
    //-- little endian case, fit for Android at least. and iOS
    // normally network endian mode is big endian, but most OSs are little endian.
    
    bp[0]= sv & 0x00FF;
    bp[1]= (sv & 0xFF00) >> 8;
}  