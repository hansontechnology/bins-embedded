/*******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2012-2014
 *  Part of CSR uEnergy SDK 2.4.3
 *  Application version 2.4.3.0
 *
 * FILE
 *      user_config.h
 *
 * DESCRIPTION
 *      This file contains definitions which will enable customization of the
 *      application.
 *
 ******************************************************************************/

#ifndef __USER_CONFIG_H__
#define __USER_CONFIG_H__

#include "configuration.h"

/*=============================================================================*
 *  Public Definitions
 *============================================================================*/

/* Buzzer code has been put under compiler flag ENABLE_BUZZER. If required
 * this flag can be disabled like at the time of current consumption 
 * measurement 
 */
//-- 150330 #define ENABLE_BUZZER

#ifdef ENABLE_BUZZER
/* TIMER values for Buzzer */
#define SHORT_BEEP_TIMER_VALUE  (100* MILLISECOND)
#define LONG_BEEP_TIMER_VALUE   (500* MILLISECOND)
#define BEEP_GAP_TIMER_VALUE    (25* MILLISECOND)
#endif /* ENABLE_BUZZER */

#endif /* __USER_CONFIG_H__ */
