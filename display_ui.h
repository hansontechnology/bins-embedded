
#ifndef  _DISPLAY_UI_
#define  _DISPLAY_UI_

#include "configuration.h"

/* ----------------- macros ------------------------------------ */


#define UI_DISPLAY_MODE_MIN         0
#define UI_DISPLAY_MODE_TIME        1
#define UI_DISPLAY_MODE_ACTIVITY    2
#define UI_DISPLAY_MODE_HRD         3
#define UI_DISPLAY_MODE_OFF         0
// 10.10 #define UI_DISPLAY_MODE_SHAKE       4
#define UI_DISPLAY_MODE_MAX         4   //- 10.10 5   //-- 09.29


#define UI_DISPLAY_LABEL_TIME       1
#define UI_DISPLAY_LABEL_ACTIVITY   2
#define UI_DISPLAY_LABEL_HRD        3

#define DISPLAY_FONT_INDEX_DISCONNECTED 0
#define DISPLAY_FONT_INDEX_HEARTRATE 1
//-- #define DISPLAY_FONT_INDEX_BAR       3  // 150504 not use anymore, 2015.01.31
#define DISPLAY_FONT_INDEX_ARROWTO   3  
//-- 150712 #define DISPLAY_FONT_INDEX_LEVEL     4 
#define DISPLAY_FONT_INDEX_WORKING   4
#define DISPLAY_FONT_INDEX_SPACE    16
#define DISPLAY_FONT_INDEX_COLON    15
#define DISPLAY_FONT_INDEX_CLOCK    22  // 49-5
#define DISPLAY_FONT_INDEX_STEP     21
#define DISPLAY_FONT_INDEX_CHARGING 19
#define DISPLAY_FONT_INDEX_MESSAGE  20
//-- #define DISPLAY_FONT_INDEX_SHAKE    22  // 150504 not use anymore
//-- 150712 #define DISPLAY_FONT_INDEX_QUESTION 22

#define DISPLAY_FONT_INDEX_BATTERY_HALF 22
#define DISPLAY_FONT_INDEX_BATTERY_FULL 23
#define DISPLAY_FONT_INDEX_SLEEP_RUNNING 24

#define DISPLAY_FONT_INDEX_ALERT    2
#define DISPLAY_FONT_INDEX_BATTERY_OUT 17
#define DISPLAY_FONT_INDEX_CONNECTED 18
#define DISPLAY_FONT_INDEX_SPACE_NARROW 0

#define DISPLAY_UI_CHAR_INDEX_NUMBER_ZERO   5
#define DISPLAY_UI_CHAR_INDEX_ALPHABET_A   

//void displayAlphabet(uchar letter);
void displayChar(uint8 font_index);
void displayCharRaw(uint8 font_index);
void uiDisplayNumber(uint32 displayNumber, int8 length, bool isShrink);
void uiDisplayStart(void);
void uiDisplayAlert(bool onoff);
void uiDisplayCharging(bool onoff);
void uiDisplayInit(void);
void uiDisplayHeartRateNotice(bool isUpdateAnyway);
void uiDisplayHeartRateActive(void);
void uiDisplayHeartRateStop(void);
void uiDisplayHeartRateReading(void);
void uiDisplayTime(void);
void uiDisplayActivityUpdate(uint8 steps);
void uiDisplayGoalAchieved(void);
void uiDisplayTimeUpdate(void);

uint8 uiDisplayCurrentMode(bool *isActive);
void uiDisplayShow(bool isNewPage); 
void uiDisplayShowDelayed(uint16 delayTimeMs);
void uiDisplayMessage(bool onoff);
void uiDisplayHeartRateProcessing(bool isStart); // 2015.01.31
void uiDisplayChangeState(void);    // 20150505
bool uiDisplayIsOn(void);    // 150601
void uiDisplayShowAndDone(void); // 150602
void uiDisplayStartDelayed(uint16 delayTimeMs);
void uiDisplayConnectStatusUpdate(void);
void uiLongPressAction(void); // 150710
void setUiDisplayMode(int displayMode);

#ifdef CONFIG_VIBRATION
void uiVibrationStart(int ms);
#endif


extern uint16 dayActivityNumber;
extern bool gHasAlertEvent; //-- 09.18
extern bool gHasMessageEvent; //-- 09.22

extern bool isUiDisplayCharging;


#endif
