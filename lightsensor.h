
#ifndef __LIGHTSENSOR_H__
#define __LIGHTSENSOR_H__

#include "configuration.h"

void lightSensorPowerOn(void);
void lightSensorPowerDown(void);

void lightSensorInit(void);   
uint16 lightSensorRead(void);
uint16 lightSensorReadDirect(void);

typedef enum {
    AMBIENT_LIGHT_BRIGHT,
    AMBIENT_LIGHT_NORMAL,
    AMBIENT_LIGHT_WEAK
} AmbientLightStatus;

AmbientLightStatus lightSensorAmbientLevel(void);

#endif