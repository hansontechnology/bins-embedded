/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2012-2014
 *  Part of CSR uEnergy SDK 2.4.3
 *  Application version 2.4.3.0
 *
 *  FILE
 *      ht_hw.h
 *
 *  DESCRIPTION
 *      This file defines the Health Thermometer hardware specific routines.
 *
 *****************************************************************************/

#ifndef __HT_HW_H__
#define __HT_HW_H__

#include <pio.h>

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <types.h>
#include <bluetooth.h>
#include <timer.h>

/*============================================================================*
 *  Public Definitions
 *============================================================================*/

#include "configuration.h"


/*============================================================================*
 *  Local Header Files
 *============================================================================*/
#include "user_config.h"

#ifndef CONFIG_TARGET_BINS

/* Buzzer code has been put under compiler flag ENABLE_BUZZER. If required
 * this flag can be disabled like at the time of current consumption 
 * measurement 
 */
#define ENABLE_BUZZER

#endif 

/*============================================================================*
 *  Public data type
 *============================================================================*/

#ifdef ENABLE_BUZZER

/* data type for different type of buzzer beeps */
typedef enum
{
    /* No beeps */
    buzzer_beep_off = 0,

    /* Short beep */
    buzzer_beep_short,

    /* Long beep */
    buzzer_beep_long,

    /* Two short beeps */
    buzzer_beep_twice,

    /* Three short beeps */
    buzzer_beep_thrice

}buzzer_beep_type;

#endif

typedef struct
{

#ifdef ENABLE_BUZZER
    /* Buzzer timer id */
    timer_id                    buzzer_tid;

    /* Variable for storing beep type.*/
    buzzer_beep_type            beep_type;

    /* Variable for keeping track of beep counts. This variable will be 
     * initialized to 0 on beep start and will incremented at every beep 
     * sound
     */
    uint16                      beep_count;
#endif /* ENABLE_BUZZER */

    /* Timer for button press */
    timer_id                    button_press_tid;

}APP_HW_DATA_T;

/*============================================================================*
 *  Public Data Declarations
 *============================================================================*/

/* Blood pressure application hardware data instance */
extern APP_HW_DATA_T                   g_app_hw_data;

/*============================================================================*
 *  Public Function Prototypes
 *============================================================================*/

/* This function is called to initialise Health Thermometer hardware */
extern void HtInitHardware(void);

/* This function initialises Health Thermometer hardware data structure */
extern void HtHwDataInit(void);

/* This function handles PIO Changed event */
extern void HandlePIOChangedEvent(uint32 pio_changed);

#ifndef CONFIG_TARGET_BINS
/* This function is called to trigger beeps of different types 
 * 'buzzer_beep_type'
 */
extern void SoundBuzzer(buzzer_beep_type beep_type);

#endif


bool CheckPowerCharging(void);  // 07.11


/* Bit-mask of a PIO. */
#define PIO_BIT_MASK(pio)             (0x01UL << (pio))

#ifdef CONFIG_TARGET_BINS

/*
//-- BINS LED and GPIO

LED 3   -> GPIO [4] 
LED 4   -> GPIO [5] / SPI_CLK
LED 5   -> GPIO [6] / SPI_CS
LED 6   -> GPIO [7] / SPI_MOSI
LED 7   -> GPIO [8] / SPI_MISO

*/

#define LED3_PIO    (4)
#define LED4_PIO    (5)
#define LED5_PIO    (6)
#define LED6_PIO    (7)
#define LED7_PIO    (8)

#define LED3_BIT_MASK PIO_BIT_MASK(LED3_PIO)
#define LED4_BIT_MASK PIO_BIT_MASK(LED4_PIO)
#define LED5_BIT_MASK PIO_BIT_MASK(LED5_PIO)
#define LED6_BIT_MASK PIO_BIT_MASK(LED6_PIO)
#define LED7_BIT_MASK PIO_BIT_MASK(LED7_PIO)

#define LEDS_PIO_MASK  (LED3_BIT_MASK | LED4_BIT_MASK | LED5_BIT_MASK | LED6_BIT_MASK | LED7_BIT_MASK)

#define CHARGING_STATUS_PIO  (3)  //--
#define CHARGING_STATUS_PIO_MASK  (0x1L << CHARGING_STATUS_PIO)  //--

#endif

extern void EnableLED(uint16 pio, int8 pwm_no, bool enable);

extern void newFlashLED(uint16 pio, int8 pwm_no);


#define BATTERY_LEVEL_DETECT_PIO  (1)
#define BATTERY_LEVEL_DETECT_PIO_MASK (0x1L << BATTERY_LEVEL_DETECT_PIO) 
extern uint16 BatteryVoltageGet(void);

#if 1 //-- 09.03 For compilation convenience #ifdef CONFIG_TARGET_BINSX2

#define TOUCH_INT_PIO  (5)
#define TOUCH_INT_PIO_MASK (0x1L << TOUCH_INT_PIO)  //-- 

#define TOUCH_RESET_PIO (7)
#define TOUCH_RESET_PIO_MASK (0x1L << TOUCH_RESET_PIO)  //-- 

#define TOUCH_PD_PIO (8)
#define TOUCH_PD_PIO_MASK (0x1L << TOUCH_PD_PIO)  //-- 

#define VIBRATOR_PIO (6)
#define VIBRATOR_PIO_MASK (0x1L << VIBRATOR_PIO)  //-- 

#endif

#define BUTTON_UI_PIO (0)
#define BUTTON_UI_PIO_MASK (0x1L << BUTTON_UI_PIO)  //-- 12.18


#endif /* __HT_HW_H__ */
