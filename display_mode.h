/**
---------------------------------------------------------------------
**
**  This device driver was created by 
**  8-Bit Single-Chip Microcontrollers
**
**  Copyright(C) visionox display Corporation 2002 - 2009
**  All rights reserved by Kunshan visionox display Corporation.
**
**  This program should be used on your own responsibility.
**
**  Filename :	Display_Mode.h
**  Abstract :	This file implements device driver for display mode function.
**
**  Device :	STC89LE516RD
**
**  Compiler :	KEIL uVision2 
**
**  Module:  M01030
**
**  Dot Matrix: 128*32
**
**  Display Color: White
**
**  Driver IC: SH1106G
**
**  Edit : 
**
**  Creation date:	
---------------------------------------------------------------------
**/

#include "display_hw.h"

#ifndef  _Display_Mode_
#define  _Display_Mode_
//
//
extern void Display_Clear_Screen(void);
extern void Display_All_Screen(void);
extern void Display_Row_Show135(void);
extern void Display_Row_Show246(void);
extern void Display_Row_Show147(void);
extern void Display_Row_Show258(void);
extern void Display_Row_Show369(void);
extern void Display_Column_Show135(void);
extern void Display_Column_Show246(void);
extern void Display_Column_Show147(void);
extern void Display_Column_Show258(void);
extern void Display_Column_Show369(void);
extern void Display_Frame_Show(void);
extern void Display_Chess_Board1(void);
extern void Display_Chess_Board2(void);
extern void Display_Column_Scan(void);
extern void Display_Program_Edit(void);
extern void Display_Photo_Show(void);
// extern void LED_VCC_Control(uchar Control_State);
extern uchar ROW[3][4];
#endif