/*****************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2012-2014
 *  Part of CSR uEnergy SDK 2.3.0
 *  Application version 2.3.0.0
 *
 *  FILE
 *      hr_sensor.h
 *
 *  DESCRIPTION
 *      This file defines a simple implementation of Heart Rate sensor device
 *
 *****************************************************************************/

#ifndef __HR_SENSOR_H__
#define __HR_SENSOR_H__

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <types.h>
#include <bt_event_types.h>
#include <timer.h>

/*============================================================================*
 *  Local Header File
 *============================================================================*/

//-- #include "hr_sensor_gatt.h"
//-- #include "user_config.h"

/*============================================================================*
 *  Public Definitions
 *============================================================================*/

/* Maximum number of words in central device IRK */
#define MAX_WORDS_IRK                                     (8)

/* As per Heart Rate profile spec, the maximum number of RR-Interval Values 
 * that can be notified if Energy Expended is present is 8 and the maximum 
 * number of RR-Interval Values that can be notified if Energy Expended is 
 * not present is 9.
 */
#define MAX_RR_VALUES                                     (8)

 /* The following macro should be defined to enable simulation of Heart rate 
  * sensor data
  */

/* Max HR data that can be send in one ATT notification */
#define MAX_ATT_HR_NOTI_LEN                               (20)


extern void sendHRMeasurement(void);

/* This function starts sending the HR Measurement notifications. */
extern void StartSendingHRMeasurements(void);



#endif /* __HR_SENSOR_H__ */

