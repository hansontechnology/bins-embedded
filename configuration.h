/*******************************************************************************
 *    Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 * FILE
 *    configuration.h
 *
 *  DESCRIPTION
 *    This file defines various parameters that specify the capabilities
 *    of this remote control.
 *
 ******************************************************************************/

#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

//-- #error "Configuration required! Please read the information given here."

/* 
 * This application cannot be used without configuration!
 *
 * This application contains many different configuration options to allow it
 * to be used for many different applications. This application must be
 * configured before it can be used.
 * There are two options:
 * 1. It is strongly recommended to use the CSR Remote Control Configuration
 *    Tool. The latest version is available from <TBD>.
 *    This tool presents a graphical user interface that simplifies
 *    application configuration. Note that the tool overwrites this file and
 *    so any changes made here will be lost.
 * 2. By following the notes given in this configuration file, it is possible
 *    to configure this application "manually". This is only recommended for 
 *    experienced users.
 *    If you choose to follow this route, please comment out the #error
 *    statement above.
 *
 * Best regards, 
 *    the remote control application development team.
 */

/*******************************************************************************
 * CONFIGURATION BEGINS HERE
 */

/* Un-comment below macro to enable GAP Peripheral Privacy support.
 * If Privacy is supported, the remote control uses resolvable random address 
 * for advertising. Peripheral privacy flag and re-connection address
 * characteristics are supported when privacy is supported.
 * The idle (disconnection) time is defined by CONNECTED_IDLE_TIMEOUT_VALUE.
 */
/*#define __GAP_PRIVACY_SUPPORT__*/

/* If this device should never disconnect from the host, comment this. 
 * Otherwise, the device will disconnect (and sleep) when idle, reconnecting
 * only when activity is detected.
 */
/*#define DISCONNECT_ON_IDLE                      1*/

/* Fill in here appropriate values for vendor and product identifiers and
 * product version.
 */
//-- #define CONF_DEVICE_NAME		'C','S','R',' ','R','e','m','o','t','e',' ','C','t','r','l'
//-- #define VENDOR_ID               0x000A
//-- #define PRODUCT_ID              0x014D
//-- #define PRODUCT_VER             0x0100

/******************************************************************************
 * ACCELEROMETER
 *****************************************************************************/
/* If this remote control has an accelerometer, uncomment this definition. */
#define ACCELEROMETER_PRESENT

#if defined(ACCELEROMETER_PRESENT)

/* Choose the accelerometer type. One (and only one) must be enabled. */
//-- #define ACCELEROMETER_IS_KXTF9
#define ACCELEROMETER_IS_MMA8653FC
#define ACCELEROMETER_IS_8BIT   //--

/* #define ACCELEROMETER_IS_LIS331DLH */

/* This is the PIO used for interrupts from the accelerometer. */
//--** not use anymore #define ACCELEROMETER_INTERRUPT_PIO     (9)  //-- INT1 = WAKE, INT2 = 9

#endif /* ACCELEROMETER_PRESENT */




/******************************************************************************
 * I2C bus connection
 *****************************************************************************/
/* If the peripheral devices are on non-standard I2C pins, state the connections
 * here. Note that all peripheral devices (accelerometer, gyroscope, codec,
 * etc., must be on the same I2C bus.
 * If no separate bus is used for peripheral devices, comment out these 
 * definitions.
 */
#define PERIPHERAL_SDA_PIO                  (11)   //--
#define PERIPHERAL_SCL_PIO                  (10)   //--
#define PERIPHERAL_I2C_EXISTS

#define ACCELEROMETER_USES_PERIPHERAL_BUS
/*
//-- BINS LED and GPIO

LED 0   -> GPIO [4] 
LED 1   -> GPIO [5] / SPI_CLK
LED 2   -> GPIO [6] / SPI_CS
LED 3   -> GPIO [7] / SPI_MOSI
LED 4   -> GPIO [8] / SPI_MISO

*/

#define CONFIG_TARGET_BINS  //--
#define CONFIG_TARGET_BINSX2 // 05.21
//-- no need #define CONFIG_TARGET_BINSX2_P2 //-- 09.29 Pearl case

#define CONFIG_TARGET_BINSZ3 // 12.18

#ifdef CONFIG_TARGET_BINSZ3
#define CONFIG_TARGET_BINSZ3_HBP // 150409 for HBP model's configuration
//-- #define CONFIG_TARGET_BINSZ3_ST  // 150510 for Siltron case
#endif

#ifdef CONFIG_TARGET_BINSZ3
#define CONFIG_VIBRATION    //-- 12.22, 09.18
#endif

#define CONFIG_STEP_CHECK_SMART

#ifdef CONFIG_TARGET_BINSX2 // 09.26
#define CONFIG_GSENSOR_AUTOSLEEP
#endif

#ifndef CONFIG_TARGET_BINSX2 // 10.12
#define CONFIG_GSENSOR_AUTOSLEEP
#endif

#define CONFIG_SLEEP_TRACKING   //-- 09.13, 150514 in general all models need the sleep tracking

//-- #define CONFIG_HEARTRATE_SERVICE   //-- 11.27

#ifdef CONFIG_TARGET_BINSZ3
/* OTA code has been put under complier flag ENABLE_OTA */
#define ENABLE_OTA  //-- 150329

#define CONFIG_IBEACON //-- 150423 

//-- for action button, still single click will make sense
//-- 150603 #define CONFIG_BUTTON_ONE_CLICK_SWITCH //-- 150602
  
#define CONFIG_CLOCK_ALARM  //-- 150709

#endif

#ifdef CONFIG_TARGET_BINSZ3_HBP

#define CONFIG_PUSH_ADVERTISING //-- 150513
#define CONFIG_IBEACON_BATTERY_LOW //-- 150827

#endif


#ifdef CONFIG_TARGET_BINSZ3_ST

#define CONFIG_FREEFALL_DETECTION
#define CONFIG_BUTTON_SOS
#define CONFIG_IBEACON_BATTERY_LOW
//-- #define CONFIG_IBEACON_HEARTRATE

#endif

#ifdef CONFIG_FREEFALL_DETECTION
#undef CONFIG_GSENSOR_AUTOSLEEP
#endif

//-- #define CONFIG_DEBUG_OUTPUT     //-- 20150513 for debug purpose, MUST be remarked if for production release


#if (!defined(CONFIG_TARGET_BINSX2)) //-- 09.29 || defined(CONFIG_TARGET_BINSX2_P2)) 
#define CONFIG_HAND_GESTURE  // 2015.01.31 this means a tapping to the body, only applicable to BiNS X1
#endif

#if (!defined(CONFIG_TARGET_BINSZ3) && defined(CONFIG_TARGET_BINSX2))

//-- disable it #define CONFIG_HAND_GESTURE
#define CONFIG_SHAKE_WAKE_TOUCH

#endif


#ifndef CONFIG_TARGET_BINSX2    //-- 10.12, for BiNS X1

#define CONFIG_SHAKE_WAKE_TOUCH

#endif


#ifdef CONFIG_SHAKE_WAKE_TOUCH
#define CONFIG_UI_GESTURE_TRACK //-- 150513 for X2 only, but Z3 is still applicable?
#endif


#endif /* _CONFIGURATION_H */