  

/*  
    Platform condition:
        System timer in second the reoslution is.

    Data sructure :
        keep record of g-sensor values (weigthed averaged) up to 300.

    Data flow:    
    0. Value normalization  
        0.0 For each X, Y, Z, find the historical data in last two, and result a weighted average value.
        0.1 The averaging action must comes with consecutive data collection within a short time window, such as 1 second. 
            More thanthis period, will consider as an independent data.            
    1. Check for hand gesture
        1.1 calculate G sensor signal strength, if it is strong than a threshold, then trigger UI activities defined.
    2. Check for new step cycle
        2.1 if collected active data > 300, recalculate threshold
        2.1 calculate the slope of G sensor data.
        2.3 detect peak (a curve from high to low) event.
        2.4 if peak event is detected, and the values compliance to condition of high threshold and low threshold, 
            then add step counter for that time stamp in minute.
    
    Utilities:
    UI activities: first Time, second number of steps in percentage.
        Time: Hour, [flash three times in second], minutes. [flash three times in second]
        Steps: On sequentially, with 300ms for each light on to next.
               
        
*/

#include <time.h>
#include <gatt.h>
#include "app_gatt_db.h"

#include "configuration.h"
#include "ht_hw.h"
#include "pedometer.h"
#include "activity_tracking_service.h"
#include "health_thermometer.h"
#include "motion.h"
#include "display_ui.h"
#include "sleep_monitoring.h"
 
// #define CONFIG_STEP_UNIFIED   //-- 10.12 09.26

#define GSENSOR_BUFFER_MAX                      (20)  //-- (20)  //++ (500) //++ 1000) // 10
#define GSENSOR_STRENGTH_THRESHOLD              (0x1000) //-- ((0x12000)) //--8000  50000too sensitive through statistic data 

#define GSENSOR_NOT_VALUE                       (0x7FFF)
#define GSENSOR_MAX_VALUE                       ( 0x01FF)    //-- 0000,0001,1111,1111
#define GSENSOR_MIN_VALUE                       (-0x01FF)    //-- 1111,1110,0000,0000

#ifdef CONFIG_TARGET_BINSX2

//--------------------
#ifdef CONFIG_TARGET_BINSZ3

#ifdef CONFIG_TARGET_BINSZ3_HBP
#define STEP_RECORD_MAX                         (3*10*160)
#define STEP_RECORD_CYCLE_TIME                   5  //-- 5 minutes for one cycle to save space
#else                                           // 150510 for BINSZ3_ST case too
#ifdef CONFIG_TARGET_BINSZ3_ST

#define STEP_RECORD_MAX                         (3*15*122)
#define STEP_RECORD_CYCLE_TIME                   5

#else // Z3

#define STEP_RECORD_MAX                         (3*15*124)
#define STEP_RECORD_CYCLE_TIME                   5


#endif

#endif

#else       // if not Z3
 
#ifdef CONFIG_HEARTRATE_SERVICE
#define STEP_RECORD_MAX                         (3*15*145)  // 11.27 

#else       // X2 case

#define STEP_RECORD_MAX                         (3*15*140)    //11.25 37->36s, 09.12 6*20 = 5 days if per minute, 05.21 to 5, 6 -- the storage length to store step record
                                                            //-- 7200, in worst case, five days  
#define STEP_RECORD_CYCLE_TIME                   5  //-- 150506 to use this, reduce memory, 
                                                    //-- 5 minutes for one cycle to save space
#endif

#endif
//--------------------

#else       //-- X1 case

#define STEP_RECORD_MAX                         (3*54*38)   //-- 10.12 09.12
#define STEP_RECORD_CYCLE_TIME                   5

#endif
//-- #define STEP_CHECK_THRESHOLD_DEFAULT            (GSENSOR_MAX_VALUE*2/5)  //-- 3 *2/7
//-- #define STEP_CHECK_THRESHOLD_DEFAULT            (0xCC) // 2/5

//-- #define STEP_CHECK_THRESHOLD_DEFAULT            (0xAA) // /3
#define STEP_CHECK_THRESHOLD_DEFAULT            (0x98) // /3+


#define MAX_BIT_STEP_CHECK_THRESHOLD_ADDER      (5) //-- 32- 64
#define MAX_NUMBER_STEP_CHECK_THRESHOLD_ADDER   (10) //++ 10, 20 //-- (1<<MAX_BIT_STEP_CHECK_THRESHOLD_ADDER) // 2^5= 32

//-- #define STEP_CHECK_THRESHOLD_MIN                ((GSENSOR_MAX_VALUE*2/7)/4)   
//-- #define STEP_CHECK_THRESHOLD_MIN                (0x88) //++ (0x93) // 88, 4C   
#define STEP_CHECK_THRESHOLD_MIN                    ((0x88)<<1) //-- 08.19 <<2, 07.30 (0x94)  

#define STEP_CHECK_THRESHOLD_TIMEOUT            (20*SECOND)
#define STEP_CHECK_INTERVAL                     (100*MILLISECOND)   //-- 160, 320

#define STEP_DURATION_MIN_DELTA    200000  // 09.11, 200 millisecond

typedef struct StGsensorStepCheck
{
    int16 max;
    int16 min;
    int16 thresholdHigh;
    int16 thresholdLow;
    int32 averageThreshAdderHigh;
    int32 averageThreshAdderLow;    
    int16 adderHighCounter, adderLowCounter;
    timer_callback_arg thresholdUpdateTimerHandler;
    
    //-- 09.22 bool  isThresholdCountingTimeout;
    
    bool  stepFlag;
    
    //-- 09.22 int16 gsLastRiseCount, gsLastDownCount;
    //-- 09.22 int16 gsensorRiseCount, gsensorDownCount;

#ifdef CONFIG_STEP_UNIFIED    
    uint32 stepTime;
#endif
    
} GsensorStepCheck;



uint8  gLocalClock, gLocalHour, gLocalMinute; //-- for local clock
// uint16 gFuncTimerBase[3];
uint32 gFuncTimerBase;
uint16 gActivityGoal;
uint16 gConfiguration;
uint32 gUiSenseStrength;
uint8  gGsensorMode;

bool   gUILocked;   // 10.09

GsensorData gGsensorData[GSENSOR_BUFFER_MAX];

uint16 gGsensorDataHeader;
uint16 gGsensorDataTail;
uint16 gGsensorDataCount;

GsensorStepCheck gStepCheckX, gStepCheckY, gStepCheckZ;

StepRecord gStepRecord[STEP_RECORD_MAX];     //-- the actual storage to store records

uint16 gStepRecordHeader;
uint16 gStepRecordTail;
uint16 gStepRecordCount;
uint16 gStepRecordWithMarkedCount;
uint16 gStepTotalToday; // 150721

#ifndef CONFIG_TARGET_BINSX2
static bool uiIsRunning;
#endif

static bool hadStepCheckInit;

/* Clock and Time functions */
uint32 sysTimeMinutes;

//-- uint32 gDayStepCounterBaseTime; //-- 08.19
//-- uint16 gDayStepCountStart;
StepRecord *gDayStepCountBaseRecord; // 2015.01.15

static void clockUpdate(void);
static uint16 gClockHour;
static uint16 gClockMinute;

int8 gShakeThreshold;
#define SHAKE_THRESHOLD_LEVEL_MAX   7   //-- 10.08 from 7

void shakeThresholdCheck()
{
    if(gShakeThreshold>SHAKE_THRESHOLD_LEVEL_MAX) gShakeThreshold=SHAKE_THRESHOLD_LEVEL_MAX;
    if(gShakeThreshold<1) gShakeThreshold=1;
}
#if 0 // 10.11
void shakeThresholdChange(int levelAdd)
{
    gShakeThreshold+=levelAdd;
    if(gShakeThreshold>SHAKE_THRESHOLD_LEVEL_MAX) gShakeThreshold=1;
    shakeThresholdCheck();
}
#endif

#ifdef CONFIG_CLOCK_ALARM

typedef struct {
    uint8 vibDuration;
    uint8 vibDistance;
} ClockAlarmPattern;

static uint8 clockAlarmPatternIndex;

static ClockAlarmPattern clockAlarmPattern[] = {
    {20,5},    
    {1,10},
    {1,1},
    {1,10},
    {1,1},
    {1,1},
    {1,5},
    {2,1},
    {2,1},
    {0,0}
};
static timer_id clockAlarmTimer_tid;

static void clockAlarmTimerHandler(timer_id tid)
{
    uint32 vibTime;
    
    if(tid!=clockAlarmTimer_tid) return;
    if(clockAlarmPattern[clockAlarmPatternIndex].vibDuration==0){
        clockAlarmTimer_tid=TIMER_INVALID;
        return;
    }
    vibTime=clockAlarmPattern[clockAlarmPatternIndex].vibDuration;

    if(vibTime>10) vibTime=300; //-- 150901 special case for less than 1 second vibration, at first alert
    else vibTime=(vibTime*1000);
    
    uiVibrationStart(vibTime);   //-- millisecond unit
    clockAlarmTimer_tid=TimerCreate(
            (vibTime*MILLISECOND)+((clockAlarmPattern[clockAlarmPatternIndex].vibDistance ) * SECOND), 
            TRUE, clockAlarmTimerHandler);
    clockAlarmPatternIndex++;
    
}

void clockAlarmStart()
{
    clockAlarmPatternIndex=0;
    clockAlarmTimer_tid=TIMER_INVALID;
    clockAlarmTimerHandler(clockAlarmTimer_tid);            
}

void clockAlarmStop()
{
    if(clockAlarmTimer_tid!=TIMER_INVALID){
        TimerDelete(clockAlarmTimer_tid);
        clockAlarmTimer_tid=TIMER_INVALID;
    }
}

static void clockAlarmCheck(void)
{
    if(CLOCK_ALARM_ENABLED){
        uint16 clockHour;
        uint16 clockMinute;
        uint16 alarmHour=CLOCK_ALARM_HOUR;
        uint16 alarmMinute=CLOCK_ALARM_MINUTE;
        clockGet(&clockHour, &clockMinute);        
        
        if(clockHour==alarmHour && clockMinute==alarmMinute){
            clockAlarmStart();
            setUiDisplayMode(UI_DISPLAY_MODE_TIME);    // 150901, to show clock time at first button press
        }
    }
}    
#endif


/* 
uint16 syncedSystemTime[3];
uint16 currentSystemTime[3];
                
uint16 diffSystemTime[3];
*/
/*
uint32 timeInMinute(uint16 *timeInMs)
{ 
    uint32 highvalue;
    uint32 lowvalue;
    uint32 result;
    
    lowvalue=timeInMs[1];
    lowvalue= timeInMs[0]+(lowvalue<<16);
    highvalue=timeInMs[2];
    highvalue=highvalue<<16;
    
    lowvalue=(lowvalue/60000000L);  //-- 1 minutes = 60 * 1000 ms * 1000 us
    highvalue=(highvalue/60000000L);
        
    result=(highvalue<<16)+lowvalue;
     
    return result;
}

*/

uint32 sysTimeDiffAccumulated;
uint32 gLastTime32;
uint32 gLocalClockBaseSystemTimeInMinute; //-- 08.20

//-- 09.18
static void stepRecordTimeShifter(uint16 sTime);
static void systemTimeShifter(void);

static void systemTimeShifter(void)
{    
    uint16 shiftTime=sysTimeMinutes-(24*60+10);
    
    stepRecordTimeShifter(shiftTime);
    sysTimeMinutes -= shiftTime;
    gLocalClockBaseSystemTimeInMinute -=shiftTime;
    //-- gDayStepCounterBaseTime -= shiftTime;    
}

void minuteCounterTimerHandler(timer_id tid);
void minuteCounterTimerHandler(timer_id tid)
{
    
    TimerCreate((60*SECOND), TRUE, minuteCounterTimerHandler);
    
#if 0 

    TimeGet48WithOffset(currentSystemTime,0);
               
    TimeSub48(diffSystemTime, currentSystemTime, syncedSystemTime);
        
    sysTimeMinutes=timeInMinute(diffSystemTime);
    
#else
	{ 
		uint32 diffTime32=0;
		uint32 currentTime32=TimeGet32();
        bool hadMinuteChanged=FALSE;

		if(currentTime32>gLastTime32){
			diffTime32=currentTime32-gLastTime32;
		}
		else{
			diffTime32=currentTime32+(0xFFFFFFFFL-gLastTime32)+1;
		}
		gLastTime32=currentTime32;

		sysTimeDiffAccumulated+=diffTime32;
		
		while(sysTimeDiffAccumulated>(60*SECOND)){
    		sysTimeMinutes++;
			sysTimeDiffAccumulated-=(60*SECOND);
            hadMinuteChanged=TRUE;
		}        
        
        if(hadMinuteChanged==TRUE){
            clockUpdate();
            if(gClockHour==0 && gClockMinute==0) gStepTotalToday=0;  //-- 150721 reset the total number
            uiDisplayTimeUpdate();   
#ifdef CONFIG_CLOCK_ALARM
            clockAlarmCheck();
#endif
        }
        
        // 09.18
        if(sysTimeMinutes> 0x3F00){
            systemTimeShifter();               
        }
	}
#endif
        
    motionSensorWatchdog();
    advertisingWatchdog();  //-- 150501
    
#ifdef CONFIG_IBEACON_BATTERY_LOW
    atCheckBatteryLow();
#endif
         
}

    
uint16 systemTimeInMinute(void)
{   
 
    uint16 diffMinutes;
    
    diffMinutes=(uint16)(0x0000ffff & sysTimeMinutes);
    
    return diffMinutes;
}

extern void systemTimeReset(void)
{
    sysTimeMinutes=0;
    sysTimeDiffAccumulated=0;
}

#if 1

void clockGet(uint16 *clockHour, uint16 *clockMinute)
{
   *clockHour=gClockHour;
   *clockMinute=gClockMinute;
}

static void clockUpdate(void)
{
    uint16 diffMinutes;
    int32  diffMinutes32;
    
    // 08.20
    diffMinutes32= sysTimeMinutes-gLocalClockBaseSystemTimeInMinute; // 08.20
    diffMinutes=(uint16)diffMinutes32;
    
    // diffMinutes=systemTimeInMinute(); 
    
    gClockHour=(gLocalHour+(gLocalMinute+diffMinutes)/60)%24;   //-- 24 hour 
    gClockMinute=((gLocalMinute+diffMinutes)%60);                
}

#endif

#if 0 //-- no use anymore

static uint32 arrayToU32(uint16 *value)
{
    uint16 *bt;
    uint32 sum;
    bt=(uint16*)value;
    sum=bt[1];
    sum=(sum<<16);
    return (sum+bt[0]);
}

uint32 timeInSecond(uint16 *timeInMs)
{ 
    uint32 highvalue;
    uint32 lowvalue;
    uint32 result;
    
    lowvalue=arrayToU32(timeInMs);
    highvalue=arrayToU32(timeInMs+2);
    
    lowvalue=(lowvalue/1000);  //-- 1 minutes = 60000 ms.
    highvalue=(highvalue<<16)/1000;
        
    result=(highvalue<<16)+lowvalue;
     
    return result;
}

//-- volatile    uint32 highvalue;
//-- volatile    uint32 lowvalue;

uint32 timeInMinute(uint16 *timeInMs)
{ 
    uint32 highvalue;
    uint32 lowvalue;
    uint32 result;
        
    lowvalue=arrayToU32(timeInMs);
    highvalue=arrayToU32(timeInMs+2);
    
    lowvalue=(lowvalue/60000000);  //-- 1 minutes = 60*1000 ms.* 1000 ms
    highvalue=(highvalue<<16)/60000000;
        
    result=(highvalue<<16)+lowvalue;
     
    return result;
}

#endif //-- no use anymore

static void clockInit(void)
{                
    gLocalHour= (gLocalClock & 0xFF00) >> 8;
    gLocalMinute= (gLocalClock & 0xFF);
    gLocalClockBaseSystemTimeInMinute=sysTimeMinutes;   // 08.20
    
/*    
    TimeGet48WithOffset(syncedSystemTime,0);    //-- bonding system clock with local clock
*/
}


/*  Step Record Functions */

static void stepRecordFlush(void)
{
    gStepRecordHeader=gStepRecordTail=0;
    gStepRecordWithMarkedCount=gStepRecordCount=0;
}

static void stepRecordInit(void)
{
    stepRecordFlush();
}

static StepRecord *stepRecordGetFrom(uint16 indexFromLatest)
{
    StepRecord *sr;
    uint16 aIndex;
    
    if(indexFromLatest>=gStepRecordWithMarkedCount)
        return NULL;
    
    if(indexFromLatest>=gStepRecordHeader) //-- means wrapped
    {
       aIndex=(STEP_RECORD_MAX-1)-(indexFromLatest-gStepRecordHeader);
    }
    else
    {
       aIndex=gStepRecordHeader-1-indexFromLatest;
    }
        
    sr=(&gStepRecord[aIndex]);
        
    return sr;
        
}

extern StepRecord *stepRecordTop(void)
{
    if(gStepRecordCount>0)
    {
        if(gStepRecordHeader==0) return &gStepRecord[STEP_RECORD_MAX-1];
        return &gStepRecord[gStepRecordHeader-1];
    }
    return NULL;
}
static uint16 StepRecordNotified(StepRecord *sr)
{
    return (sr->relMinute & 0x4000);    
}
extern void setStepRecordNotify(StepRecord *sr)
{
    sr->relMinute |= 0x4000;    
}
extern uint16 StepRecordMinute(StepRecord *sr)
{
    return (sr->relMinute & 0x3FFF);  //11 days  
}
/* no need anymore
   
static bool StepRecordUploaded(StepRecord *sr)
{
    if(sr->relMinute & 0x8000) return TRUE;
    
    return FALSE;
}

static void StepRecordCleanFrom(uint16 iRecordOverOneDay)
{
    StepRecord *sr;
    uint16 i;
  
    for(i=iRecordOverOneDay; i<gStepRecordWithMarkedCount; i++)
    {
        sr=stepRecordGetFrom(i);
        if(StepRecordUploaded(sr)==TRUE) sr->relMinute=0;  //-- clean it
        //-- shouldn't happen, else break;     //-- means end of the end it
    }      
        
}   
*/   
extern uint16 StepRecordCount(void)
{
    return gStepRecordCount;
}

extern StepRecord *stepRecordGetFromTail(void)
{
    if(gStepRecordCount>0)
        return &gStepRecord[gStepRecordTail];
        
    return NULL;
}

extern StepRecord *stepRecordAvailGetFromTail(void)
{
    uint16 i;
    uint16 rIndex, rIndexBase;
    uint16 activeRecordCount;
    StepRecord *sr;
    
    activeRecordCount=gStepRecordCount; //-- for current active ones
    
    for(i=0, rIndexBase=gStepRecordTail; i<activeRecordCount; i++){
        rIndex=rIndexBase+i;
        
        if(rIndex>=STEP_RECORD_MAX) rIndex=rIndex-STEP_RECORD_MAX;
        sr= &gStepRecord[rIndex];
        
        if(StepRecordNotified(sr)==0){
            return sr;
        }
        else{
            //--?? 05.30 ?? 
            //-- for the case of notified, no chance to delete them
            //-- sync only start from the beginning of a connection
            //-- notification won't have chance to delete the queued item.

            //-- 05.30 stepRecordDeleteFromTail();
            StepRecordMarkUploaded(sr);
        }
    }
    
    return NULL;
}

extern void stepRecordDeleteFromTail(void)
{
    if(gStepRecordCount>0)
    {
        gStepRecordCount--;
        gStepRecordTail++;
        if(gStepRecordTail==STEP_RECORD_MAX) gStepRecordTail=0;
    }
    
}

//-- called while after the record being uploaded.
//-- keep the latest one day record, and for the rest, don't care.

extern void StepRecordMarkUploaded(StepRecord *sr)
{
    uint16 nowTime=systemTimeInMinute();
    
    stepRecordDeleteFromTail();
    
    if((nowTime-StepRecordMinute(sr))<(24*60))
    {
        sr->relMinute |= 0x8000;    //-- 16th bit to 1 for uploaded data
        
    } 
    else
    {        
        if(gStepRecordWithMarkedCount>0) gStepRecordWithMarkedCount--;    
    }
    
}

extern void stepRecordAdd(uint16 relMinute, uint8 numSteps)
{
    if(gStepRecordCount==STEP_RECORD_MAX)
    {
        gStepRecordCount--;
        gStepRecordTail++;
        if(gStepRecordTail==STEP_RECORD_MAX) gStepRecordTail=0;
        
        gStepRecordWithMarkedCount--;        
    }
    
    gStepRecord[gStepRecordHeader].relMinute=relMinute;
    gStepRecord[gStepRecordHeader].stepCount=numSteps;  //-- 09.13 automatically set the type flag to 0
    gStepRecordHeader++;
    if(gStepRecordHeader==STEP_RECORD_MAX) gStepRecordHeader=0;    
    
    gStepRecordCount++; 
    gStepRecordWithMarkedCount++;
    
    // 2015.01.17, clear it eventually after cycle ups
    if((&gStepRecord[gStepRecordHeader])==gDayStepCountBaseRecord) 
        gDayStepCountBaseRecord=NULL;
}

//--  150413
static bool stepRecordTimeInside(uint16 rangeStart, uint16 newTime)
{
    if(newTime>=rangeStart && newTime<=(rangeStart+STEP_RECORD_CYCLE_TIME))
        return TRUE;
    return FALSE;
}

//-- int gStepTotal=0; // for debug purpose

static void stepAdd(uint8 numSteps)
{
    StepRecord *latestRecord=stepRecordTop();
    uint16 sysTimeMinute;    
    
    //-- gStepTotal=numSteps;   // for test purpose
    
    sysTimeMinute=systemTimeInMinute();
    
    if(ACTIVITY_TYPE(latestRecord)!=0){ // 0 means a step record, otherwise is sleep record
        stepRecordAdd(sysTimeMinute, numSteps);
        latestRecord=stepRecordTop();
    }
    
    if(latestRecord!=NULL && stepRecordTimeInside(StepRecordMinute(latestRecord),sysTimeMinute))
    {
        //-- if needs to be notified, just use the existing active record, 
        //-- becasue the previous values of the record must have been synced already.
        //-- the App will only update the count ONE even the stepCount is NOT one,
        //-- for the case of Bluetooth notification.
        
        //-- For the case of activities happen while syncing, the count may be lost,
        //-- because App side does not set notification yet, 
        //-- the App software won't receive the updates sending from BINS.

        if(g_ht_data.state==app_state_connected &&
            gConfiguration & BINS_CONFIG_MASK_RECORD_NOTIFY)
        {
            ADD_STEP_COUNT(latestRecord,numSteps);
        }
        else    // case that not need to be notified
        {
            if(StepRecordNotified(latestRecord)==0)
            {
                ADD_STEP_COUNT(latestRecord,numSteps);
            }    
            else    // create a new record, to tell from the steps already notified.
            {
                stepRecordAdd(sysTimeMinute, numSteps);
            }                        
        }
    }
    else
    {
        stepRecordAdd(sysTimeMinute, numSteps);
    }    
       
    if(g_ht_data.state==app_state_connected &&
        gConfiguration & BINS_CONFIG_MASK_RECORD_NOTIFY)
    {
        StepRecord *lR=stepRecordTop();
            
        StepRecord vR= (*lR);
            
        setStepRecordNotify(lR);
                        
        activityValueNotify(g_ht_data.st_ucid, &vR, numSteps);       
    }

    {
        uint16 newStepAchieved=gStepTotalToday+numSteps;
        uint16 oldStepAchieved=gStepTotalToday;
        gStepTotalToday=newStepAchieved;

#ifdef CONFIG_TARGET_BINSX2                
        if(g_at_serv_data.activity_goal!=0 &&
           oldStepAchieved<g_at_serv_data.activity_goal && 
           newStepAchieved>=g_at_serv_data.activity_goal){
                uiDisplayGoalAchieved();
        }else{
                uiDisplayActivityUpdate(numSteps);
        }
#endif            
    }
            
}

/* G Sensor Data collection */

static void gsensorDataInit(void)
{
    gGsensorDataHeader=gGsensorDataTail=0;
    gGsensorDataCount=0;
}

static void gsensorDataAdd(int16 x, int16 y, int16 z)
{
    gGsensorData[gGsensorDataHeader].x=x;
    gGsensorData[gGsensorDataHeader].y=y;
    gGsensorData[gGsensorDataHeader].z=z;
    
    gGsensorDataHeader++;   
    if((gGsensorDataHeader==GSENSOR_BUFFER_MAX)) gGsensorDataHeader=0;
    
    if(gGsensorDataTail==gGsensorDataHeader)
    {
        gGsensorDataTail++;
        if(gGsensorDataTail==GSENSOR_BUFFER_MAX) gGsensorDataTail=0;
    }
    else gGsensorDataCount++; 
}

GsensorData *gsensorDataGet(uint16 relIndex)
{
    int16 index;
    GsensorData *gd;
    
    if(relIndex>=gGsensorDataCount)
    { 
        return NULL;
    }  
    
    if(gGsensorDataHeader<=relIndex) index=(GSENSOR_BUFFER_MAX-1-(relIndex-gGsensorDataHeader));
    else index=gGsensorDataHeader-relIndex-1;
    
    gd= (&(gGsensorData[index]));
        
    return gd;    
}    

#if 0 //-- no need of normalization, low frequency
static bool gsensorNormalizedDataGet(int16 relIndex, GsensorData *normalizedData)
{   
    GsensorData *before=gsensorDataGet(relIndex+1);
    GsensorData *now=gsensorDataGet(relIndex);    
    GsensorData *after=gsensorDataGet(relIndex-1);
    
    if(before && now && after)
    {    
        normalizedData->x=((before->x+2*now->x+after->x)/4)  & (0xFFFC); //- ignore the LSB 2 bits
        normalizedData->y=((before->y+2*now->y+after->y)/4)  & (0xFFFC);
        normalizedData->z=((before->z+2*now->z+after->z)/4)  & (0xFFFC);
        
        return TRUE;
    }
    
    return FALSE;    
}
#endif



#if (!defined(CONFIG_TARGET_BINSX2)) || (defined(CONFIG_TARGET_BINSX2) && defined(CONFIG_SLEEP_TRACKING))

//-- 09.18 #ifdef CONFIG_SLEEP_TRACKING    //-- 09.13 
   
static uint32 getSignalStrength(GsensorData *currentData, GsensorData *lastData)
{
    uint32 strengthValue=0;
    int32 tempValue;

#if 1 //-- 09.09 recovery
    tempValue=(currentData->x-lastData->x);
    tempValue=tempValue>>2;
    strengthValue+=(tempValue*tempValue);

    tempValue=(currentData->y-lastData->y);
    tempValue=tempValue>>2;
    strengthValue+=(tempValue*tempValue);

    tempValue=(currentData->z-lastData->z);
    tempValue=tempValue>>2;
    strengthValue+=(tempValue*tempValue);
#else    
    tempValue=(currentData->x-lastData->x);
    strengthValue+=abs(tempValue);

    tempValue=(currentData->y-lastData->y);
    strengthValue+=abs(tempValue);

    tempValue=(currentData->z-lastData->z);
    strengthValue+=abs(tempValue);
   
#endif
    
    //-- 09.13 called later, for use together with sleep function, strengthDataPut(strengthValue);
        
    return strengthValue;
}
#endif //--  CONFIG_SLEEP_TRACKING

#ifdef CONFIG_HAND_GESTURE //------------------------------

/* Strength calculation ---------------------------------------------------------------------- */

#define STRENGTH_RECORD_MAX     (20) // 09.13 65, 09.09 (20), (65) //-- max 10 seconds buffering, 0x180
uint32 aStrengthData[STRENGTH_RECORD_MAX];
uint16 aStrengthDataHead;
uint16 aStrengthDataTail;
uint16 aStrengthDataCounter;

//-- uint32 aStrengthTime[STRENGTH_RECORD_MAX];

static int strengthDataCounter(void)
{
    return aStrengthDataCounter;
}
static void strengthDataPut(uint32 strengthValue)
{
    aStrengthData[aStrengthDataHead]=strengthValue;
    //-- aStrengthTime[aStrengthDataHead]=TimeGet32(); //-- test
    
    aStrengthDataHead++;
    if(aStrengthDataCounter<STRENGTH_RECORD_MAX) aStrengthDataCounter++;
    
    if(aStrengthDataHead>=STRENGTH_RECORD_MAX) aStrengthDataHead=0;
    
    if(aStrengthDataTail==aStrengthDataHead)
    { 
        aStrengthDataTail++;
        if(aStrengthDataTail>=STRENGTH_RECORD_MAX) aStrengthDataTail=0;
    }
}

static bool strengthDataGet(uint16 relIndex, uint32 *data)
{
    uint16 arrayIndex;
    
    if(relIndex>=aStrengthDataCounter) return FALSE;
    
    if(aStrengthDataHead<=relIndex) arrayIndex=(STRENGTH_RECORD_MAX-1)-(relIndex-aStrengthDataHead);
    else arrayIndex=aStrengthDataHead-relIndex-1;
    
    *data=aStrengthData[arrayIndex];
    
    return TRUE;
}

static void strengthDataReset(void)
{
    aStrengthDataCounter=0;
    aStrengthDataHead=aStrengthDataTail=0;
}

#endif // CONFIG_HAND_GESTURE-------------


#ifndef CONFIG_TARGET_BINSX2

/* UI and LED display functions */

//-- #define BINS_LED_PLAY_TIME  (500*MILLISECOND)

//-- #define BINS_LED_PLAY_TIME  (4*SECOND) //-- (1*SECOND)
//-- #define BINS_UI_CLOCK_STAY_TIME (30*SECOND) //-- ((5+2)*SECOND)
#if 0
#define BINS_LED_PLAY_TIME  (750*MILLISECOND)
#define BINS_UI_CLOCK_STAY_TIME ((5+2)*SECOND)
#define BINS_UI_DEMO_STAY_TIME (30*SECOND)
#endif

//-- for Circle #define BINS_LED_PLAY_TIME  (350*MILLISECOND)
#define BINS_LED_PLAY_TIME  (150*MILLISECOND)
#define BINS_UI_CLOCK_STAY_TIME ((5+2)*SECOND)
#define BINS_UI_DEMO_STAY_TIME (30*SECOND)

timer_id  ledFlash_tid;
timer_id  clockStay_tid;
timer_id  ledPlay_tid;
timer_id  ledPlayEnd_tid;

int8 ledFlashActiveNo;
int8 ledFlashActivePwm;
int ledPlayCounter;
int ledPlayTop;
int8 ledPlayMinute;

//-- IMPORTANT HOW CLOCK WORKS
//-- const int8 timeToLedTable[6]= {0, 12, 24, 36, 48, 60};
const int8 timeToLedTable[6]= {0, 10, 25, 35, 50, 60};
const int8 hourToLedTable[8]= {0, 1, 3, 5, 7, 9, 11, 12};
//-- 2014.2.7 const int8 timeToLedTable[6]= {0, 10, 25, 35, 50, 60};

const int8 ledToPIO[5]={LED7_PIO, LED3_PIO, LED4_PIO, LED5_PIO, LED6_PIO};

#if 0
static int minuteToLedNo(int8 m)  //-- 1 - 5 return value
{
    int i;

    for(i=0; i<6; i++)
    {
        if(timeToLedTable[i]<=m && m<timeToLedTable[i+1]) break;
    }
    return i+1;   
}

static int hourToLedNo(int8 h)  //-- 1 - 5 return value
{
    int i;

    for(i=0; i<8; i++)
    {
        if(hourToLedTable[i]<=h && h<hourToLedTable[i+1]) break;
    }
    if(i>=6) i=0;
    
    return i;   
}
#endif

static void ledFlashTimerHandler(timer_id tid)
{
    if(ledFlash_tid==tid){
        ledFlash_tid=TIMER_INVALID;
        EnableLED(ledFlashActiveNo, ledFlashActivePwm, FALSE);
        EnableLED(ledFlashActiveNo, -1, TRUE);
    }
}

static void ledPlayWithFlash(int ledNo)
{
    if(ledNo>5 || ledNo<1) return;
    ledFlashActiveNo=ledToPIO[ledNo-1];
    
    if(ledFlashActivePwm>1 || ledFlashActivePwm<0) ledFlashActivePwm=0; //initialization

    ledFlashActivePwm=1-ledFlashActivePwm;
    EnableLED(ledFlashActiveNo, ledFlashActivePwm, TRUE);
    TimerDelete(ledFlash_tid);
    ledFlash_tid=TimerCreate(BINS_LED_PLAY_TIME, TRUE, ledFlashTimerHandler);
}

void uiClockStayTimerHandler(timer_id tid);

#if 0 //-- for circle
static void ledPlayMinuteTimerHandler(timer_id tid)
{
    //-- TimerDelete(tid);
    //-- ledPlay_tid=TIMER_INVALID;
    EnableLED(ledToPIO[ledPlayMinute-1], 2, TRUE);
    
    if(ledPlayTop!=0) {
        newFlashLED(ledToPIO[ledPlayTop-1],1);  //-- highlight hour too
    }
    
    clockStay_tid=TimerCreate((2*SECOND), TRUE, uiClockStayTimerHandler);
}   
#endif

static void ledStopAll(void)
{
    int i;
    //-- make sure the LEDs are off
    /* Disable the PWM */
    PioEnablePWM(0, FALSE);
    PioEnablePWM(1, FALSE);
    PioEnablePWM(2, FALSE);
    for(i=0; i<5; i++)
       EnableLED(ledToPIO[i],-1,FALSE);    
}

#if 0
static void ledFullOn(void)
{
    int i;
    //-- make sure the LEDs are on
    /* Disable the PWM */
    PioEnablePWM(0, FALSE);
    PioEnablePWM(1, FALSE);
    PioEnablePWM(2, FALSE);
    for(i=0; i<5; i++)
       EnableLED(ledToPIO[i],-1,TRUE);    
}
#endif

#if 0
static void ledTestOn(void)
{
    //-- make sure the LEDs are on
    /* Disable the PWM */
    PioEnablePWM(0, FALSE);
    PioEnablePWM(1, FALSE);
    PioEnablePWM(2, FALSE);
    EnableLED(ledToPIO[0],-1,TRUE);    
    EnableLED(ledToPIO[1],-1,FALSE);    
    EnableLED(ledToPIO[2],-1,TRUE);    
    EnableLED(ledToPIO[3],-1,FALSE);    
    EnableLED(ledToPIO[4],-1,TRUE);    
}
#endif

static void uiStop(void)
{
    ledStopAll();
    uiIsRunning=FALSE;
    
    if(ledPlayEnd_tid!=TIMER_INVALID)
    {
        TimerDelete(ledPlay_tid);
        ledPlayEnd_tid=TIMER_INVALID;
    }
}

static void ledPlayTimerHandler(timer_id tid)
{
 
#if 0 //-- for circle
    if(ledPlayTop!=0) { //-- 11 to 1 clock , no hour LED
        ledPlayWithFlash(ledPlayCounter);
    }
    
    ledPlayCounter++;
    if(ledPlayCounter<=ledPlayTop)
    {
        ledPlay_tid=TimerCreate(BINS_LED_PLAY_TIME, TRUE, ledPlayTimerHandler);
        if(ledPlay_tid==TIMER_INVALID){
            uiStop();
        }
    }
    else
    {        
        //-- for circle ledPlay_tid=TimerCreate(BINS_LED_PLAY_TIME, TRUE, ledPlayMinuteTimerHandler);
        //-- for circle TimerCreate(BINS_LED_PLAY_TIME, TRUE, ledPlayMinuteTimerHandler);
        
        clockStay_tid=TimerCreate((2*SECOND), TRUE, uiClockStayTimerHandler);
        if(clockStay_tid==TIMER_INVALID){
            uiStop();            
        }
    }
#else
    if(ledPlay_tid==tid)
    {
        ledPlay_tid=TIMER_INVALID;

        EnableLED(ledToPIO[ledPlayCounter-1],-1,TRUE);
    
        ledPlayCounter++;
        if(ledPlayCounter<=ledPlayTop)
        {
            ledPlay_tid=TimerCreate(BINS_LED_PLAY_TIME, TRUE, ledPlayTimerHandler);
            if(ledPlay_tid==TIMER_INVALID){
                uiStop();
            }
        }
        else
        {        
            clockStay_tid=TimerCreate((500*MILLISECOND), TRUE, uiClockStayTimerHandler);
            if(clockStay_tid==TIMER_INVALID){
                uiStop();            
            }
        }
    }
#endif    
    
}

#if 0
int8 sysH, sysM;
static void ledPlayTime(int8 h, int8 m)
{
    int n;
    sysH=h;
    sysM=m;
    
    //-- ledPlayWithFlash(3);
    //-- n=minuteToLedNo((h%12)*5);  //-- for hour, (60/12=5)
    //-- the return value can be 0 to 5, but for 0 then no LED light.
    
    n=hourToLedNo(h%12); //- 0 to 11
    
    ledPlayTop=n;
    ledPlayCounter=1;
    ledPlayMinute=minuteToLedNo(m); 
    
    //-- if(ledPlayTop==5 && ledPlayCounter==1) ledPlayCounter=2;
        
    ledPlayTimerHandler(TIMER_INVALID);    
}        
#endif

static void ledPlayEndTimerHandler(timer_id tid)
{
    if(ledPlayEnd_tid==tid)
    {
        ledPlayEnd_tid=TIMER_INVALID;
        if(uiIsRunning==TRUE)   //-- means failed to complete
            uiStop();
    }
}

static void ledPlayCircle(void)
{   
    ledPlayTop=5;
    ledPlayCounter=1;

    ledPlayEnd_tid=TimerCreate((3200*MILLISECOND), TRUE, ledPlayEndTimerHandler);
    if(ledPlayEnd_tid!=TIMER_INVALID){
        ledPlay_tid=TIMER_INVALID;
        ledPlayTimerHandler(TIMER_INVALID); 
    }   
}  

static void ledPlayActivityStopTimerHandler(timer_id tid)
{
    if(ledPlay_tid==tid)
    {
        ledPlay_tid=TIMER_INVALID;
        uiStop();  //-- finally the end of the action running
    }
}

static void ledPlayActivityTimerHandler(timer_id tid)
{
    if(ledPlay_tid==tid)
    {
        ledPlay_tid=TIMER_INVALID;
 
        ledPlayWithFlash(ledPlayCounter);
   
        ledPlayCounter++;
        if(ledPlayCounter<=ledPlayTop)
        {
            ledPlay_tid=TimerCreate(BINS_LED_PLAY_TIME, TRUE, ledPlayActivityTimerHandler);
        }
        else
        {
            ledPlay_tid=TimerCreate(BINS_LED_PLAY_TIME*5, TRUE, ledPlayActivityStopTimerHandler);
        }
        
        if(ledPlay_tid==TIMER_INVALID){
            uiStop();        
        }
    }    
}

void ledPlayActivity(int8 percentage);
void ledPlayActivity(int8 percentage)
{
    int8 n=(percentage/20);
        
    ledPlayTop=n;
    ledPlayCounter=1;
    if(uiIsRunning==TRUE){       
        ledPlay_tid=TimerCreate((1*SECOND), TRUE, ledPlayActivityTimerHandler);  
        if(ledPlay_tid==TIMER_INVALID){
            uiStop();
        }
    }
}

#endif // -------------- NOT CONFIG_TARGET_BINSX2  ----------------------------

//-- 09.18
static void stepRecordTimeShifter(uint16 sTime)
{
    StepRecord *sr;
    uint16 i;
  
    for(i=0; i<gStepRecordWithMarkedCount; i++)
    {
       uint16 oldTime;

       sr=stepRecordGetFrom(i);
       if(sr==NULL) break;
       
       oldTime=StepRecordMinute(sr);
       
       if(oldTime>=sTime)
       {
                sr->relMinute = (sr->relMinute & 0xC000) | ((oldTime-sTime) & 0x3FFF);
       }        
       else
       {
             break;
       }
    }
             
}

uint16 activityGetFromMinute(uint16 sysMinute, uint16 minuteRange);
uint16 activityGetFromMinute(uint16 sysMinute, uint16 minuteRange)
{
    uint16 i;
    StepRecord *sr;
    uint16 totalCount;
    uint16 startMinute;
     
    totalCount=0;
    
    if(sysMinute<minuteRange){ startMinute=0; minuteRange=sysMinute; }
    else startMinute=sysMinute-minuteRange;
        
    for(i=0; i<gStepRecordWithMarkedCount; i++)
    {
       sr=stepRecordGetFrom(i);
       if(sr==NULL) break;

       if(ACTIVITY_TYPE(sr)!=0) continue;   // if is a sleep record, ignore it.
       
       if(StepRecordMinute(sr)>=startMinute)
       {
            totalCount+=STEP_COUNT(sr);
       }        
       else
       {
            //-- 09.16 cannot delete them, as sleep records still need to care,
            //-- StepRecordCleanFrom(i);
            
            break;
       }
       
       if(sr==gDayStepCountBaseRecord) break;// 2015.01.15
    }    

    return totalCount;
}

uint16 activityTheDay(void)
{
    uint16 nowSystemMinute;
    uint16 todayActivities;
    
    uint16 clockHour, clockMinute;
    
    clockGet(&clockHour, &clockMinute);        
    
    nowSystemMinute=systemTimeInMinute();
        
    todayActivities=activityGetFromMinute(nowSystemMinute, 
                                          (clockHour*60)+clockMinute);   // in last one day
    
    return todayActivities;
}

#ifndef CONFIG_TARGET_BINSX2
static bool uiInProgress(void)
{
    return uiIsRunning;
}
#endif

int32 gPercentAchieved; // for test and lookup from debugger

#ifndef CONFIG_TARGET_BINSX2  //--------------------------------------------

void uiClockStayTimerHandler(timer_id tid)
{
    int32   percentAchieved; 
    int8    percent8;
    uint16  activityDay;        

    if(clockStay_tid!=tid) return;
    
    clockStay_tid=TIMER_INVALID;
    ledStopAll();
    
    //-- 09.09 nomore maintained, if(gConfiguration & BINS_CONFIG_LED_MASK_DAILY_ACHIEVE)
    {        
        activityDay=activityTheDay();
        
        percentAchieved=(activityDay*100L);
        
        if(gActivityGoal>0){
            percentAchieved=(percentAchieved/gActivityGoal);
        } else {
            percentAchieved=100;
        }
        
        gPercentAchieved=percentAchieved;
        
        if(percentAchieved>100) percentAchieved=100;
        
        percent8=(int8)(percentAchieved);
        
        ledPlayActivity(percent8);
    }
/* 09.09    
    else
    {
        uiIsRunning=FALSE;
    }
*/
}

#if 0

static void uiDemoStayTimerHandler(timer_id tid)
{
    //-- TimerDelete(tid);
    //-- clockStay_tid=TIMER_INVALID;
    ledStopAll();
}

        

static void uiDemoStart(void)
{
    ledFullOn();
    TimerDelete(clockStay_tid);
    clockStay_tid=TimerCreate(BINS_UI_DEMO_STAY_TIME, TRUE, uiDemoStayTimerHandler);
}
#endif
//----

static bool hadLedAlertOn;
static timer_id ledAlertTimer_tid;
static uint8 ledAlertOnDuration, ledAlertOffDuration, ledAlertLifeCycle;
static bool *alertingEvent;

static void ledAlertTimerHandler(timer_id tid)
{
    uint8 nextTimerDuration;
    bool isRunNextTime=TRUE;
    
    if(ledAlertTimer_tid!=tid) return;

    if(ledAlertLifeCycle<(ledAlertOffDuration+ledAlertOnDuration)){
        //-- end of the alert lifecycle;
        isRunNextTime=FALSE;
        hadLedAlertOn=TRUE; // force to turn off
    }
    
    if(hadLedAlertOn==FALSE)
    {    
        hadLedAlertOn=TRUE;
        if(uiIsRunning==TRUE){   
            uiStop();            
            TimerDelete(clockStay_tid); 
            TimerDelete(ledPlay_tid);
            TimerDelete(ledFlash_tid); 
        }
        {
            EnableLED(ledToPIO[0], 0,TRUE);
            EnableLED(ledToPIO[1], -1,TRUE);
            EnableLED(ledToPIO[2], 1,TRUE);
            EnableLED(ledToPIO[3], -1,TRUE);
            EnableLED(ledToPIO[4], 2,TRUE);
        }
        
        nextTimerDuration=ledAlertOnDuration;
        
        uiIsRunning=TRUE;
    }
    else{
        hadLedAlertOn=FALSE;
        uiStop();
        nextTimerDuration=ledAlertOffDuration;
    }      

    if(isRunNextTime==TRUE){
        ledAlertLifeCycle -=ledAlertOffDuration;
        ledAlertTimer_tid=TimerCreate((nextTimerDuration*SECOND), 
                                                TRUE, ledAlertTimerHandler);        
    }
    else{
        *alertingEvent=FALSE;  //-- auto reset      
    }    
}

static void uiLedAlertBase(bool enable, bool *event, uint8 onTime, uint8 offTime, uint8 lifeTime)
{
    
    alertingEvent=event;
    
    if(ledAlertTimer_tid!=TIMER_INVALID){
        TimerDelete(ledAlertTimer_tid);
        ledAlertTimer_tid=TIMER_INVALID;
    }
    
    if(enable==TRUE)
    {        
        hadLedAlertOn=FALSE;
        ledAlertOnDuration=onTime;
        ledAlertOffDuration=offTime;
        ledAlertLifeCycle=lifeTime;        
        
        ledAlertTimerHandler(ledAlertTimer_tid);
        
        uiIsRunning=TRUE;   
    }
    else
    {
        uiStop();
    }
    
    *event=enable;
}

extern void uiAlertEnable(bool enable)
{
    uiLedAlertBase(enable, &gHasAlertEvent, 90, 0, 90); // ON, OFF, period
}

extern void uiMessageNotice(bool enable)
{
    uiLedAlertBase(enable, &gHasMessageEvent, 1, 5, 60); // ON, OFF, period
}

//----
static void uiChargingStartTimerHandler(timer_id tid)
{
    if(clockStay_tid!=tid) return;   
    clockStay_tid=TIMER_INVALID;
    
    ledStopAll();
    EnableLED(ledToPIO[0],-1,FALSE);
    EnableLED(ledToPIO[1], 0,TRUE);
    EnableLED(ledToPIO[2],-1,FALSE);
    EnableLED(ledToPIO[3], 1,TRUE);
    EnableLED(ledToPIO[4],-1,FALSE);
}

extern void uiChargingEnable(bool enable)
{
    if(enable==TRUE)
    {
        // ledFullOn();
        
        if(uiIsRunning==TRUE){   
            uiStop();            
            TimerDelete(clockStay_tid); 
            TimerDelete(ledPlay_tid);
            TimerDelete(ledFlash_tid);            
        } 
        
        clockStay_tid=TimerCreate((1000*MILLISECOND), TRUE, uiChargingStartTimerHandler);        
        uiIsRunning=TRUE;
        
    }
    else
    {
        uiStop();
    }
}


extern void uiActionStart(void)
{
#if 0    
    uint16 clockHour, clockMinute;

    //-- int h=3, m=48;
    uint16 dispMode;
#endif
    
    if(uiIsRunning==FALSE)
    {    
        uiIsRunning=TRUE;   //--will be reset later after timers finished

#if 0        
        dispMode=(gConfiguration&BINS_CONFIG_LED_MASK_DEMO_MODE);
        
        if(dispMode!=0) //-- demo mode activated
        {
            uiDemoStart();
        }
        else
        {
          if(gConfiguration & BINS_CONFIG_LED_MASK_LOCAL_CLOCK)
           {
                
            if(0){    
                clockGet(&clockHour, &clockMinute);        
                ledPlayTime((int8)clockHour, (int8)clockMinute);
            }
                                    
            //-- TimerDelete(clockStay_tid);
            //-- clockStay_tid=TimerCreate(BINS_UI_CLOCK_STAY_TIME, TRUE, uiClockStayTimerHandler);
          }
          else
          {
            uiClockStayTimerHandler(TIMER_INVALID); //--directly move to next display item
          }
        }  
#else //-- don't know why, but move otherwise gConfiguration error
        ledPlayCircle();
#endif
    }   
}

void uiShowNewCycle(void)   // 10.12
{
     //-- if(gHasAlertEvent==TRUE) uiAlertEnable(FALSE);
     if(gHasMessageEvent==TRUE) uiMessageNotice(FALSE);
     if(uiInProgress()==FALSE)
        uiActionStart();
}

#endif //-- NOT CONFIG_TARGET_BINSX2

int gestureDetectStage;
timer_id gDetectTimer=TIMER_INVALID;

#if 0
static void gestureDetectTimeoutTimerHandler(timer_id tid)
{
    TimerDelete(tid);
    gDetectTimer=TIMER_INVALID;
    if(gestureDetectStage==1)
    {
       gestureDetectStage=2;
       gDetectTimer=TimerCreate((3000*MILLISECOND), TRUE, gestureDetectTimeoutTimerHandler);
       //-- TimerCreate((2*SECOND), TRUE, gestureDetectTimeoutTimerHandler);

    }
    else if(gestureDetectStage==2)
    {
        gestureDetectStage=0;
    }
}
#endif

#ifdef CONFIG_HAND_GESTURE

#include "touch.h"

/* Check Hand Gesture Functions */
//volatile uint32 aStrength;
//volatile uint32 a0, a1, a2, a3; 

// volatile uint32 aBuf[100];
// volatile uint16 aBufIndex=0;
         uint32 st1;
         uint32 st2;
         uint32 st3;
         uint32 st4;
         uint32 st5;
         uint32 st6;
         uint32 st7;
         uint32 st0;

static void checkHandGesture(void)
{
    bool isFoundGesture=FALSE;
    
    GsensorData *nowData=gsensorDataGet(0);
    GsensorData *lastData=gsensorDataGet(1);  //-- last one

    //-- GsensorData *beforeData=gsensorDataGet(2);  //-- last one

    
    if(nowData!=NULL && lastData!=NULL) //--  && beforeData!=NULL)
    {

#ifndef CONFIG_SLEEP_TRACKING   //-- 09.13        
        st0=getSignalStrength(nowData, lastData);
        strengthDataPut(st0);   //-- save to buffer queue
#endif       
        strengthDataGet(0, &st0);   //-- already saved before

        if(gGsensorMode==GSENSOR_WAKE && gestureDetectStage!=1)
        {
            int strengthCnt=strengthDataCounter();   

#ifdef CONFIG_TARGET_BINSX2_P2
//+++++++++++++++++++++++++++++++++++++++++++V

            if(strengthCnt>=5)
            {

                if(strengthDataGet(1, &st1) && strengthDataGet(2, &st2) && 
                  strengthDataGet(3, &st3) && strengthDataGet(4, &st4) &&
                  strengthDataGet(5, &st5) 
                  )
                {
                    if(                             
                         (                           
                            ((st4>=st5) || st5<0x500) && 
                            (st3>=st4) &&
                            (st3>0x1000 && st3<0x40000) &&
                            (st2>0x1000 && st2<0x40000) &&
                            (st1<=st2) &&
                            (st0<=st1 || st0<0x500)
                         )                                                                                   

                       )
                    {
                        isFoundGesture=TRUE;
                    }                    
                } //-- if more than 5 samples available
            }   //-- End of normal case

            
//+++++++++++++++++++++++++++++++++++++++++++^            
#else   //-- BINS ONE            
            
//------------------------------------------ V
            
/* 03.10    
            if(strengthCnt==4)  // 09.04 >=? ==4
            {
                strengthDataGet(1, &st1);
               
                strengthDataGet(2, &st2);
                strengthDataGet(3, &st3);
                if(
                    (st0<=st1 && st1<=st2 && st2<st3) &&
                    (st3>0xB00 && st3<0x40000) &&
                    (st2<0x200) &&
                    (st1<0x200) &&
                    (st0<0x20)
                   )
                {
                    isFoundGesture=TRUE;                
                }

                isFoundGesture=TRUE;
            }
*/
#if 0            
            strengthDataGet(1, &st1);
            strengthDataGet(2, &st2);
            strengthDataGet(3, &st3);
            strengthDataGet(4, &st4);            
            strengthDataGet(5, &st5);
            strengthDataGet(6, &st6);
            
            if( (strengthCnt >= 6) &&                            
               (                           
                 ( 
                  (st0<=st1 || st0<80) && 
                  (st1<=st2 || st1<0x100) && 
                  (st2<=st3)                                
                 ) &&  
                 ( (st4>=st5) && (st5>=st6) ) &&
                 ( st1<0x500 )         
                ) 
               ) 
            {
                isFoundGesture=TRUE;
            }
#endif
           
#if 1  //-- 09.04              
            if(strengthCnt>=7)
            {

                if(strengthDataGet(1, &st1) && strengthDataGet(2, &st2) && 
                  strengthDataGet(3, &st3) && strengthDataGet(4, &st4) &&
                  strengthDataGet(5, &st5) && strengthDataGet(6, &st6) &&
                  strengthDataGet(7, &st7)
                  )
                {
                    if(                             
                         (                           
                            ( (st0<=st1 || st0<0x80) && (st1<=st2 || st1<0x100) && (st2<=st3) ) &&  
                            ( (st4>=st5) && (st5>=st6)              ) &&
                            (st6<0x200) &&
                            (st4>0x2000 && st4<0x40000) &&
                            (st3>0x2000 && st3<0x40000) &&
                            (st2<0x2000)                &&
                            (st1<0x500) 
                         )                                                                          
                         ||
                         (                         
                            ( (st0<0x10 && st0<=st1 && st1<=st2 && st3<st4) &&
                              (st4>st5) &&
                              (st5>st6) &&              
                              (st4>0x180 && st5>0x180) &&
                              (st6<0x10) )
                         )
                         ||
                         (                         
                            ( (st0<0xB0 && st0<=st1 && st1<=st2 && st2>st3 && st3<st4) &&
                              (st4>0x5000 && st5>0x5000) &&
                              (st5>st6) &&              
                              (st6<0xB0) &&
                              (st3<st1)
                            )
                         )
                         ||   
                         (                         
                            ( st0<0x005 && st1<0x010 && 
                              st3>0x100 && st3<0x400 &&
                              st4>0x400 &&
                              st5>0x100 && st5<0x400 &&
                              st6<0x010
                            )
                         )
                         ||
                         (
                              st0<0x0005 && 
                              st1>0x0100 &&
                              st2>0x1000 &&
                              st3<0x1000 &&
                              st4<st3    &&
                              st5>st4    &&
                              st6<0x010                                                          
                         )
                         ||
                         (
                              st0<0x0020 && 
                              st1>0x1000 &&
                              st2>0x1000 &&
                              st3<st2    &&
                              st4>0x1000 &&
                              st5<st4    &&
                              st6<st5    &&
                              st7<0x80 && st7<st6
                         )

                       )
                    {
                        isFoundGesture=TRUE;
                    }                    
                } //-- if more than 5 samples available
            }   //-- End of normal case

            
#endif

//------------------------------------------^
            
            
#endif //-- CONFIG_TARGET_BINS or BINSX2_P2
            
            if(isFoundGesture==TRUE)
            {
                {
                    // for test
                    // a0=aStrength;
                    // a1=st1; a2=st2; a3=st3;
                                         
                    //-- gestureDetectStage=1;
#ifndef CONFIG_TARGET_BINSX2
                    uiShowNewCycle();
#else
                    touchPowerdown(FALSE);
                    touchSessionEnd(TOUCH_WAKE_WITH_NOACTION);
#endif
                    //-- gDetectTimer=TimerCreate((160*3*MILLISECOND), TRUE, gestureDetectTimeoutTimerHandler);
 
                 }
                    
             }   //-- End of if Gesture found
                                        
        }   //-- End of WAKE and stage = 0 or 2 
        

        
    } //-- at least two or more samples
}


#endif  //-- HANG GESTURE











/* 09.22
static void xThresholdUpdateTimerHandler(timer_id tid)
{
    gStepCheckX.isThresholdCountingTimeout=TRUE;
}

static void yThresholdUpdateTimerHandler(timer_id tid)
{
    gStepCheckY.isThresholdCountingTimeout=TRUE;
}

static void zThresholdUpdateTimerHandler(timer_id tid)
{
    gStepCheckZ.isThresholdCountingTimeout=TRUE;
}
*/
static void stepCheckDataInit(GsensorStepCheck *stepCheckData, int16 iValue)  //-- 09.22 , timer_callback_arg tHandler)
{ 
    stepCheckData->thresholdHigh=iValue+STEP_CHECK_THRESHOLD_DEFAULT;
    stepCheckData->thresholdLow=iValue-STEP_CHECK_THRESHOLD_DEFAULT;
 
    stepCheckData->min=stepCheckData->max=GSENSOR_NOT_VALUE;
    stepCheckData->stepFlag=0;

    stepCheckData->averageThreshAdderHigh=0;
    stepCheckData->averageThreshAdderLow=0;    
    stepCheckData->adderHighCounter=0;
    stepCheckData->adderLowCounter=0;
    
/* 09.22    
    stepCheckData->thresholdUpdateTimerHandler=tHandler;
    stepCheckData->isThresholdCountingTimeout=TRUE;
    
    stepCheckData->gsensorRiseCount=0;
    stepCheckData->gsensorDownCount=0;
    stepCheckData->gsLastRiseCount=0;
    stepCheckData->gsLastDownCount=0;
*/
    
}

static void stepCheckInit(int16 ix, int16 iy, int16 iz)
{
    stepCheckDataInit(&gStepCheckX, ix); //-- 09.22 , xThresholdUpdateTimerHandler);
    stepCheckDataInit(&gStepCheckY, iy); //-- 09.22 , yThresholdUpdateTimerHandler);
    stepCheckDataInit(&gStepCheckZ, iz); //-- 09.22 , zThresholdUpdateTimerHandler);
}

void dayStepCounterModeSwitch()
{
    if(gDayStepCountBaseRecord==NULL){
        dayStepCounterReset(TRUE);
    }else{
        dayStepCounterReset(FALSE);
    }    
}

//-- 08.19
void dayStepCounterReset(bool enable)
{
   
    uint16 currentTime=systemTimeInMinute(); // 2015.01.15
    
    if(enable==FALSE){
        //-- gDayStepCounterBaseTime=0;
        //-- gDayStepCountStart=0;   
        gDayStepCountBaseRecord=NULL; 
        dayActivityNumber=activityTheDay();
    }else {
        stepRecordAdd(currentTime,0);
        //-- gDayStepCountStart=0;        
        //-- gDayStepCounterBaseTime=currentTime;
        gDayStepCountBaseRecord=stepRecordTop();
        dayActivityNumber=0;
    }    
    
#ifdef CONFIG_TARGET_BINSX2  
    // uiDisplayActivityUpdate(0);
    uiDisplayShow(FALSE);
#endif
    
}

int aUpdateData=0;
int aResetData=0;
//-- 07.28 bool isFirstThreshCheckFromInit=TRUE;

static void updateStepCheckItemThreshold(GsensorStepCheck *stepCheckData)
{
    int thresholdCount;
/*    
    if(isFirstThreshCheckFromInit==TRUE){
        isFirstThreshCheckFromInit=FALSE;
        thresholdCount=MAX_NUMBER_STEP_CHECK_THRESHOLD_ADDER/2;
    }
    else
*/
    {
        thresholdCount=MAX_NUMBER_STEP_CHECK_THRESHOLD_ADDER>>1; 
                            /* 05.10 also reduce for faster reaction */        
    }
    
    if(stepCheckData->adderHighCounter>=thresholdCount)
    {
        //-- if(stepCheckData->isThresholdCountingTimeout==FALSE)
        {
            int16 oldThresholdGap=stepCheckData->thresholdHigh-stepCheckData->thresholdLow;
            
            stepCheckData->thresholdHigh=(((stepCheckData->averageThreshAdderHigh<<10) 
                                            / stepCheckData->adderHighCounter)) >> 11; //-- 09.26, 09.22 remove /2
            stepCheckData->thresholdLow=(((stepCheckData->averageThreshAdderLow<<10) 
                                            / stepCheckData->adderLowCounter)) >> 11; //-- 09.26 09.22 remove /2   

            // stepCheckData->thresholdHigh--; // 09.22 minor adjustment
            // stepCheckData->thresholdLow++;
            
#if 1  //-- 09.22 //-- 08.19  //-- 07.30 no need to set the lower bound 
            {
                int16 gapThreshold;
                int16 moreThreshold;

                gapThreshold=stepCheckData->thresholdHigh-stepCheckData->thresholdLow;

#if 1  // 10.12              
                if(oldThresholdGap>(gapThreshold<<1)){  // 10.12 from 1
                    stepCheckData->min=GSENSOR_NOT_VALUE;
                    stepCheckData->max=GSENSOR_NOT_VALUE;
                    stepCheckData->stepFlag=0;
                }
#endif
                
                if(gapThreshold<0x40) //-- 09.220x80= 0x1000/32 STEP_CHECK_THRESHOLD_MIN){
                {             
                    moreThreshold=(abs(0x40-gapThreshold))>>1;
                    stepCheckData->thresholdHigh+=moreThreshold;
                    stepCheckData->thresholdLow-=moreThreshold;
                }
            }
#endif            
            aUpdateData++;
        }
#if 0   //++ do nothing if timeout before reach of enough samples 
        else
        {
            int16 gapThreshold;    
            int16 moreThreshold;
        
            aResetData++;
                    
            gapThreshold=abs(stepCheckData->thresholdHigh-stepCheckData->thresholdLow);        
            moreThreshold=(abs((STEP_CHECK_THRESHOLD_DEFAULT<<1)-gapThreshold))>>1;
            stepCheckData->thresholdHigh+=moreThreshold;
            stepCheckData->thresholdLow-=moreThreshold;
        }    
#endif
        
        stepCheckData->averageThreshAdderHigh=0;
        stepCheckData->averageThreshAdderLow=0;
        stepCheckData->adderHighCounter=0;
        stepCheckData->adderLowCounter=0;
        
        //--09.22 stepCheckData->isThresholdCountingTimeout=FALSE;
        //++ TimerCreate(STEP_CHECK_THRESHOLD_TIMEOUT, TRUE, stepCheckData->thresholdUpdateTimerHandler);  
    }
}

static void updateStepThreshold(void)
{
    updateStepCheckItemThreshold(&gStepCheckX);
    updateStepCheckItemThreshold(&gStepCheckY);
    updateStepCheckItemThreshold(&gStepCheckZ);
}

/* G Sensor TURN point detection functions */
//-- 09.26 volatile int16 dnow, dlast, dbefore;
//-- volatile int32 sum, sumx, sumy, sumz;

static bool gsensorDetectTurn(GsensorStepCheck *stepCheckItem, int16 now, int16 last, int16 before)
{
    bool turnDetected=FALSE;
    
    //-- 09.26 dnow=now, dlast=last, dbefore=before;

    if((now-last)==0) return FALSE;
    
/* 09.22    
    if((now-last>0) && (last-before)>0)
    {
       stepCheckItem->gsensorRiseCount++;
    }
    else if((now-last<0) && (last-before)<0)
    {  
       stepCheckItem->gsensorDownCount++;
    }
    else 
*/         
    if((now-last>0) && (last-before)<0)  // 08.19 >=  //--07.31
    {
        if(stepCheckItem->min==GSENSOR_NOT_VALUE || last<stepCheckItem->min) 
            stepCheckItem->min=last;
        
        stepCheckItem->averageThreshAdderLow+=last;
        stepCheckItem->adderLowCounter++;
/* 09.22        
        stepCheckItem->gsensorDownCount++;
        stepCheckItem->gsLastDownCount=stepCheckItem->gsensorDownCount;
        stepCheckItem->gsensorRiseCount=0;
*/
    }    
    else if((now-last<0) && (last-before)>0) //-- 08.19 <=
    {    
        if(stepCheckItem->max==GSENSOR_NOT_VALUE || last>stepCheckItem->max) 
            stepCheckItem->max=last;
        
        stepCheckItem->averageThreshAdderHigh+=last;
        stepCheckItem->adderHighCounter++;
/* 09.22        
        stepCheckItem->gsensorRiseCount++;
        stepCheckItem->gsLastRiseCount=stepCheckItem->gsensorRiseCount;
        stepCheckItem->gsensorDownCount=0;
*/        
    }

#if 0    
    //-- if under threshold average, then reset to start again
    if((stepCheckItem->min!=GSENSOR_NOT_VALUE && stepCheckItem->min>=stepCheckItem->thresholdLow) || 
                (stepCheckItem->min!=GSENSOR_NOT_VALUE && stepCheckItem->max<=stepCheckItem->thresholdHigh))
    {
        stepCheckItem->min=GSENSOR_NOT_VALUE;
        stepCheckItem->max=GSENSOR_NOT_VALUE;
        stepCheckItem->stepFlag=0;
    }
#endif                
    if(stepCheckItem->max!=GSENSOR_NOT_VALUE &&
       (stepCheckItem->max>stepCheckItem->thresholdHigh && stepCheckItem->stepFlag==0))
    {
        //-- bool tmpResult;
        
        stepCheckItem->stepFlag=1;
        
        //-- 09.02
        //-- stepCheckItem->min=stepCheckItem->max=GSENSOR_NOT_VALUE; 
        stepCheckItem->max=GSENSOR_NOT_VALUE;        

        //-- tmpResult=TRUE;

        //-- stepCheckItem->gsLastRiseCount=stepCheckItem->gsLastDownCount=0;
        
        //-return TRUE;
    }    
    if(stepCheckItem->min!=GSENSOR_NOT_VALUE &&
       (stepCheckItem->min<stepCheckItem->thresholdLow && stepCheckItem->stepFlag==1))
    {
        // bool tmpResult;
        //-- restart for next step
        //  stepCheckItem->min=stepCheckItem->max=GSENSOR_NOT_VALUE;
        
        stepCheckItem->min=GSENSOR_NOT_VALUE; 
        
        stepCheckItem->stepFlag=0;

        /* 08.19, from test, remark this part, improved the accuracy to 90%
        if(abs(stepCheckItem->gsLastRiseCount-stepCheckItem->gsLastDownCount)>1){ 
            tmpResult=FALSE;
        }               
        else
            */
        //{
        //    tmpResult=TRUE;
        //}
        
        //-- stepCheckItem->gsLastRiseCount=stepCheckItem->gsLastDownCount=0;
        
        turnDetected=TRUE; // tmpResult; //-- report a new step has been detected 
        
    }
     
    return turnDetected;
}
#ifdef CONFIG_STEP_UNIFIED //-- 09.12 not work  
//-- 09.11 
GsensorStepCheck *stepLatestAxis;
uint32 stepLatestTime; 

static bool stepCheckValidity(GsensorStepCheck *stepAxis, uint32 nowTime)
{
    int32 stepDuration;
    bool foundNewStep=FALSE;
    
    stepAxis->stepTime=nowTime; //-- update time First;
    
    if(nowTime<stepLatestTime){ // overflow condition
        stepDuration=(int32)nowTime;
        stepLatestTime=nowTime;
    }
    else{
        stepDuration=(int32)(nowTime-stepLatestTime);
    }
   if(stepLatestAxis!=stepAxis){
       if(stepDuration>STEP_DURATION_MIN_DELTA){
           stepLatestAxis=stepAxis;
           stepLatestTime=nowTime;
           foundNewStep=TRUE;
       }
       foundNewStep=TRUE;   //-- 09.12 for test, proven works !
   }
   else{
           stepLatestTime=nowTime;
           foundNewStep=TRUE;
   }       
   return foundNewStep;
}
#endif //-- not work
 
static bool gsensorDataTurnDetection(void)
{
#if 0   //-- no need for normalization   
    GsensorData nowData;
    GsensorData lastData;
    GsensorData sLastData;
    if(gsensorNormalizedDataGet(1,&nowData) && 
       gsensorNormalizedDataGet(2, &lastData) &&
       gsensorNormalizedDataGet(3, &sLastData)      )
    {      
        turnDetected |= gsensorDetectTurn(&gStepCheckX, nowData.x, lastData.x, sLastData.x);
        turnDetected |= gsensorDetectTurn(&gStepCheckY, nowData.y, lastData.y, sLastData.y);
        turnDetected |= gsensorDetectTurn(&gStepCheckZ, nowData.z, lastData.z, sLastData.z);
    }           
    
#else
    //-- 09.13 moved from global         
    bool stepDetected=FALSE; 
    GsensorData *nowData;
    GsensorData *lastData;
    GsensorData *sLastData;

    nowData=(gsensorDataGet(1));
    lastData=(gsensorDataGet(2));
    sLastData=(gsensorDataGet(3));
        
    if(nowData && lastData && sLastData)
    {
        stepDetected=FALSE;
#ifdef CONFIG_STEP_UNIFIED        
        uint32 currentTime;
        bool turnResult;
        
        currentTime= TimeGet32();
        
        turnResult= gsensorDetectTurn(&gStepCheckX, nowData->x, lastData->x, sLastData->x);
        if(turnResult==TRUE){
            stepDetected |= stepCheckValidity(&gStepCheckX,currentTime);
        }
        turnResult= gsensorDetectTurn(&gStepCheckY, nowData->y, lastData->y, sLastData->y);
        if(turnResult==TRUE){
            stepDetected |= stepCheckValidity(&gStepCheckY,currentTime);
        }

        turnResult= gsensorDetectTurn(&gStepCheckZ, nowData->z, lastData->z, sLastData->z);
        if(turnResult==TRUE){
            stepDetected |= stepCheckValidity(&gStepCheckZ,currentTime);
        }
#else        
        stepDetected |= gsensorDetectTurn(&gStepCheckX, nowData->x, lastData->x, sLastData->x);
        stepDetected |= gsensorDetectTurn(&gStepCheckY, nowData->y, lastData->y, sLastData->y);
        stepDetected |= gsensorDetectTurn(&gStepCheckZ, nowData->z, lastData->z, sLastData->z);
        
#endif        
    }           
    
#endif
      
    return stepDetected;
}
    
/* Detect Step Functions */

#ifdef CONFIG_GSENSOR_AUTOSLEEP   //-- 07.31

timer_id stepSleepTimerHandler_tid;
static void stepSleepTimerHandler(timer_id tid)
{
    if(stepSleepTimerHandler_tid!=tid) return;  //-- 08.19
    stepSleepTimerHandler_tid=TIMER_INVALID;
    motionSleep();   
}
#endif

#ifdef CONFIG_STEP_CHECK_SMART

#define STEP_CHECK_IDLE 0
#define STEP_CHECK_EARLY 1
#define STEP_CHECK_ENTERING_STABLE 2
#define STEP_CHECK_STABLE 3

#define STEP_CHECK_BACKTRACK_COUNT 4
#define STEP_CHECK_INTERVAL_THOLD (2*SECOND)    //-- 09.10 4, 08.19 reduced from 4 
#define STEP_CHECK_INTERVAL_THOLD_STABLE (2*SECOND)    //-- 09.10 3 seconds, 08.19 reduced from 4 

uint32 stepDuration[3];  //-- 09.11, to calculate the average duration between steps

uint8 stepCheckStageCount;
uint8 stepCheckStage;

timer_id stepCheckStageTimerHandler_tid;
static void stepCheckStageTimerHandler(timer_id tid)
{
    if(tid!=stepCheckStageTimerHandler_tid) return;
    
    stepCheckStageTimerHandler_tid=TIMER_INVALID;
    stepCheckStage=STEP_CHECK_IDLE;  
}
#endif

#ifdef CONFIG_GSENSOR_AUTOSLEEP
static void stepCheckTimerStart(void)
{
    if(stepSleepTimerHandler_tid!=TIMER_INVALID){
        TimerDelete(stepSleepTimerHandler_tid);
        stepSleepTimerHandler_tid=TIMER_INVALID;
    }
    //-- 09.28 from 30 seconds to 3
    stepSleepTimerHandler_tid=TimerCreate((3*SECOND), TRUE, stepSleepTimerHandler);
}
#endif

static void stepCheck(void)
{                
    if(gsensorDataTurnDetection() )
    {
#ifdef CONFIG_GSENSOR_AUTOSLEEP        
        //-- 09.28
        stepCheckTimerStart();        
#endif

#ifdef CONFIG_STEP_CHECK_SMART           
            {                
                 
                if(stepCheckStage==STEP_CHECK_IDLE){
                    stepCheckStage=STEP_CHECK_EARLY;
                    stepCheckStageCount=0; 
                    stepCheckStageTimerHandler_tid=TIMER_INVALID;                    
                }else if(stepCheckStage==STEP_CHECK_EARLY){
                    stepCheckStageCount++;
                    if(stepCheckStageCount>=STEP_CHECK_BACKTRACK_COUNT){
                        stepCheckStage=STEP_CHECK_ENTERING_STABLE;    
                    }
                }else if(stepCheckStage==STEP_CHECK_ENTERING_STABLE){
                    stepAdd(stepCheckStageCount); 
                    stepCheckStage=STEP_CHECK_STABLE;
                }else if(stepCheckStage==STEP_CHECK_STABLE){
                    stepAdd(1);
                }

                {
                    if(stepCheckStage==STEP_CHECK_STABLE){
                        //-- 08.19 from(STEP_CHECK_INTERVAL_THOLD)*3
                       stepCheckStageTimerHandler_tid=
                            TimerCreate(STEP_CHECK_INTERVAL_THOLD_STABLE, TRUE, stepCheckStageTimerHandler);
                    }else{    
                        stepCheckStageTimerHandler_tid=
                            TimerCreate(STEP_CHECK_INTERVAL_THOLD, TRUE, stepCheckStageTimerHandler);
                    }
                    //-- 07.30 4 second is long enough.?
                }
            }
#else
            stepAdd(1);          
#endif            
        gStepCheckX.min=gStepCheckX.max=GSENSOR_NOT_VALUE;
        gStepCheckY.min=gStepCheckY.max=GSENSOR_NOT_VALUE;
        gStepCheckZ.min=gStepCheckZ.max=GSENSOR_NOT_VALUE;
    }
    updateStepThreshold();
}

/* Pedometer entry Functions */

//-- extern void strengthDataReset(void);

void pedometerInit(void)
{
    gUILocked=FALSE; // 10.09;
    gsensorDataInit();
    stepRecordInit();
    //-- delay this action by using first gsensor data, stepCheckInit();    
#ifndef CONFIG_TARGET_BINSX2
    uiIsRunning=FALSE;            
    clockStay_tid=TIMER_INVALID;
    ledPlay_tid=TIMER_INVALID;
    ledFlash_tid=TIMER_INVALID;
#endif
    
    hadStepCheckInit=FALSE;
    
    //-- gHasAlertEvent=FALSE;    //-- 09.18
                   
    gestureDetectStage=0;

    gGsensorMode=GSENSOR_WAKE;
      
    clockInit();    
   
#ifndef CONFIG_TARGET_BINSX2
    strengthDataReset();
#endif
    
    //-- ledFullOn();  //-- test
    //-- ledTestOn();
    
    systemTimeReset();

#if 0   // 09.12    
    isNewStep=TRUE;
#endif
    
#ifdef CONFIG_STEP_CHECK_SMART
    stepCheckStage=STEP_CHECK_IDLE;
#endif
    
#ifndef CONFIG_TARGET_BINSX2    
    ledPlayEnd_tid=TIMER_INVALID;
    ledPlay_tid=TIMER_INVALID;
    ledAlertTimer_tid=TIMER_INVALID;
#endif

#ifdef CONFIG_GSENSOR_AUTOSLEEP //-- 07.31   
    stepSleepTimerHandler_tid=TIMER_INVALID;
#endif

    // 10.11 gShakeThreshold=SHAKE_THRESHOLD_LEVEL_DEFAULT;

    TimerCreate((30*SECOND), TRUE, minuteCounterTimerHandler);
}

extern void clockSync(uint16 newClockTime)
{        
    gLocalClock=newClockTime;
    gLocalHour=   (gLocalClock & 0xFF00 ) >> 8;
    gLocalMinute= (gLocalClock & 0x00FF );
   
    gLocalClockBaseSystemTimeInMinute=sysTimeMinutes; // 08.20
               
    //-- 08.19, it is not necessory to reset it, and shouldn't . 
    //-- systemTimeReset();
/*    
    TimeGet48WithOffset(syncedSystemTime,0);    //-- bonding system clock with local clock
*/
    clockUpdate();  //-- 20150721
}



//-- only for first time to initiate values 
extern void pedometerReset()
{
    //-- initial values, should be stored to NVM for factory setting only
    
    gUiSenseStrength=GSENSOR_STRENGTH_THRESHOLD;
    gActivityGoal=1000;     //-- 1000 steps
    
    clockSync(0x0600);  // 20150721
 
    gConfiguration=BINS_CONFIG_MASK_RECORD_NOTIFY ;   //-- 10.09
    
    //-- 07.30
    gConfiguration |= BINS_CONFIG_HEARTRATE_ENABLE_MASK |
                      BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK;

#ifdef CONFIG_TARGET_BINSZ3
    //-- 150603
    gConfiguration |= BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK; // set as default
#endif
    
#ifdef CONFIG_TARGET_BINSZ3_HBP    
    // 150410 -- default to set sleep monitoring    
    gConfiguration |= BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK;
#endif
                      
    //-- bit [8-15] for led patterns 
    
    //-- 08.19
    // gDayStepCounterBaseTime=0;
    // gDayStepCountStart=0;
    
    // 10.11 
    gShakeThreshold=SHAKE_THRESHOLD_LEVEL_DEFAULT;    
    setShakeThresholdToConfig(gShakeThreshold);
    
    gStepTotalToday=0;  // 150721
    
#ifdef CONFIG_FREEFALL_DETECTION
    ffInit();
#endif
    
}

static int16 signedForm(uint16 v)
{
    int16 sv;
#if 0    //-- 10bit valid data
    if((v & 0x0200)!=0) sv= v | 0xFC00;
    else sv= v & 0x3FF;
#else   //-- 07.31 12bit valid data
    if((v & 0x0800)!=0) sv= v | 0xF000;
    else sv= v & 0x0FFF;    
#endif
    
    return sv;    
}

bool gsensorIsAwake()
{
    return (gGsensorMode==GSENSOR_WAKE);
}

void pedometerSleep(bool isWakeup)
{
#ifdef CONFIG_HAND_GESTURE
    strengthDataReset();
#endif
    //-- 07.28 no need, hadStepCheckInit=FALSE;  //-- reset again after a long sleep
#if 0 // 09.12    
    isNewStep=TRUE;
#endif
    
#ifdef CONFIG_STEP_CHECK_SMART    
    stepCheckStage=STEP_CHECK_IDLE;
#endif

#ifdef CONFIG_GSENSOR_AUTOSLEEP     
    if(isWakeup==TRUE){                   
        stepCheckTimerStart();
    }
#endif
    
}

#ifdef CONFIG_UI_GESTURE_TRACK

//-- 150506

typedef struct {
    int32 low;
    int32 high;
   }  GestureTrackAxisData;   

typedef struct {
    GestureTrackAxisData x;
    GestureTrackAxisData y;
    GestureTrackAxisData z;
}  GestureTrackData;

GestureTrackData gestureTrackData;
GestureTrackData gestureTrackPosition;

timer_id gGestureTrackTimer_tid;
bool gGestureTrackStartFlag;
bool gGestureTrackTriedFlag;
uint32 gGestureTrackStartTime;

static void gestureTrackTimerHandler(timer_id tid)
{
    if(tid!=gGestureTrackTimer_tid) return;
    
    gGestureTrackTimer_tid=TIMER_INVALID;
    
    if(gGestureTrackTriedFlag==FALSE){
        // do nothing
    }else if(gGestureTrackStartFlag==TRUE){                
        uiDisplayShow(TRUE);
    }
    gGestureTrackStartFlag=FALSE; // not track anymore
}

void gestureTrackStart(void)
{
    gGestureTrackTriedFlag=FALSE;
    gGestureTrackStartFlag=FALSE;

    gGestureTrackStartTime=TimeGet32();
    
    if(gGestureTrackTimer_tid!=TIMER_INVALID){
        TimerDelete(gGestureTrackTimer_tid);
        gGestureTrackTimer_tid=TIMER_INVALID;
    }
    gGestureTrackTimer_tid=TimerCreate((1*SECOND), TRUE, gestureTrackTimerHandler);
    
}

#define GSENSOR_MAX_VALUE2   0x3FFL
#define GSENSOR_SWING_FACTOR (2*GSENSOR_MAX_VALUE2/100)

static void gestureTrackSetPosition(GestureTrackAxisData *axis, int32 v, int32 range)
{
    axis->low=v-range; // v*985/1000;
    axis->high=v+range; // 1015/1000;   
}
static void gestureTrackSetRange(GestureTrackAxisData *axis, int32 v, int32 range)
{
    axis->low=v-range; // v*985/1000;
    axis->high=v+range; // 1015/1000;   
    
}    

static bool gestureTrackInRange(GestureTrackAxisData *axis, int32 v)
{
    if(v>=axis->low && v<=axis->high) return TRUE;
    
    return FALSE;
}    

static bool gestureTrackRightPosition(int16 x, int16 y, int16 z)
{
    if(gestureTrackInRange(&gestureTrackPosition.x, x) &&
       gestureTrackInRange(&gestureTrackPosition.y, y) && 
       gestureTrackInRange(&gestureTrackPosition.z, z)      )   //-- test z, z,
        return TRUE;  
    return FALSE;
}

static void gestureTrackInput(int16 x, int16 y, int16 z)
{
    
    // always have a timer to run during the gesture track
    if(gGestureTrackTimer_tid==TIMER_INVALID) return;

#ifdef CONFIG_DEBUG_OUTPUT
    
    if(g_ht_data.state==app_state_connected){
        unsigned char  gVar[8];
        unsigned char  *p_value;

        p_value=gVar;
        
        ShortToByteArray(p_value,   (x));  // 10 bits to 16 bits
        ShortToByteArray(p_value+2, (y));
        ShortToByteArray(p_value+4, (z));  
    
        GattCharValueNotification(g_ht_data.st_ucid, 
                              HANDLE_AT_DEBUG_OUTPUT, 
                              6, 
                              gVar);
 
    }
#endif
    
    if(gGestureTrackTriedFlag==FALSE){
        if(gestureTrackRightPosition(x,y,z)==TRUE){
            gGestureTrackTriedFlag=TRUE;
            gGestureTrackStartFlag=TRUE;
                        
            if(gGestureTrackTimer_tid!=TIMER_INVALID){
                TimerDelete(gGestureTrackTimer_tid);
                gGestureTrackTimer_tid=TIMER_INVALID;
            }
            // keep track 2 seconds
            gGestureTrackTimer_tid=TimerCreate((g_at_serv_data.ui_gesture_wait_time*MILLISECOND), 
                                               TRUE, gestureTrackTimerHandler);
            
            gestureTrackSetRange(&gestureTrackData.x, x, g_at_serv_data.ui_gesture_angle*GSENSOR_SWING_FACTOR);
            gestureTrackSetRange(&gestureTrackData.y, y, g_at_serv_data.ui_gesture_angle*GSENSOR_SWING_FACTOR);
            gestureTrackSetRange(&gestureTrackData.z, z, g_at_serv_data.ui_gesture_angle*GSENSOR_SWING_FACTOR);

            //-- uiDisplayShow(TRUE);
            
        }
        else{            
            // do nothing
        }
    }else if(gGestureTrackStartFlag==TRUE){ // means tried == true too
        
        if( gestureTrackInRange(&gestureTrackData.x, x) &&
            gestureTrackInRange(&gestureTrackData.y, y) &&
            gestureTrackInRange(&gestureTrackData.z, z)    ){   // 150506 test z, z
            
             // continue without action
            
        }else{
            uint32 shiftFromTrackStart=TimeGet32()-gGestureTrackStartTime;
            
            gGestureTrackStartFlag=FALSE;   // reset it
            
            if(gGestureTrackTimer_tid!=TIMER_INVALID){
                TimerDelete(gGestureTrackTimer_tid);
                gGestureTrackTimer_tid=TIMER_INVALID;
            }

            if(shiftFromTrackStart<(800*MILLISECOND)){ // left long enough to restart
                gGestureTrackTriedFlag=FALSE;   // retry                    
                gGestureTrackTimer_tid=TimerCreate(shiftFromTrackStart, 
                                            TRUE, gestureTrackTimerHandler);                   
            }
        }
    }                        
}

static void gestureTrackInit(void)
{
    gestureTrackSetPosition(&gestureTrackPosition.x, 0, g_at_serv_data.ui_gesture_angle*GSENSOR_SWING_FACTOR);
    gestureTrackSetPosition(&gestureTrackPosition.y, 0, g_at_serv_data.ui_gesture_angle*GSENSOR_SWING_FACTOR);
    gestureTrackSetPosition(&gestureTrackPosition.z, -GSENSOR_MAX_VALUE2, g_at_serv_data.ui_gesture_angle*GSENSOR_SWING_FACTOR);     
    
    gGestureTrackTimer_tid=TIMER_INVALID; 
    gGestureTrackTriedFlag=TRUE;   
    gGestureTrackStartFlag=FALSE;

}

#endif //-- CONFIG_UI_GESTURE_TRACK
    
extern void pedometerNewDataInput(uint16 x, uint16 y, uint16 z)
{ 
    int16 sx, sy, sz;
#if 0
    if(uiInProgress()==FALSE)
    {
        uiActionStart();
    }
#endif
    
    sx=signedForm(x);
    sy=signedForm(y);
    sz=signedForm(z);
    
    if(hadStepCheckInit==FALSE)
    {
        hadStepCheckInit=TRUE;
        //-- 07.28 isFirstThreshCheckFromInit=TRUE; // to reduce the wait time from measuring.
        stepCheckInit(sx, sy, sz);  //-- first time initialization, to use the data collected
        

#ifdef CONFIG_UI_GESTURE_TRACK  
        gestureTrackInit();
#endif

        
    }
    
    //-- test
    //-- systemTimeInMinute();
    
    gsensorDataAdd(sx,sy,sz);
    //-- gsensorDataAdd(x,y,z);

#ifdef CONFIG_UI_GESTURE_TRACK
    //-- 150506
    gestureTrackInput(sx, sy, sz);
#endif

    
#ifdef CONFIG_SLEEP_TRACKING    //-- 09.13
    {
        GsensorData *nowData=gsensorDataGet(0);
        GsensorData *lastData=gsensorDataGet(1);  //-- last one

        if(nowData!=NULL && lastData!=NULL) //--  && beforeData!=NULL)
        {
            uint32 strengthValue;
            strengthValue=getSignalStrength(nowData, lastData);
            sleepCheckUpdateEntropy(strengthValue);
            
#ifdef CONFIG_HAND_GESTURE            
            strengthDataPut(strengthValue);   //-- saved to buffer queue to  use later.
#endif

#ifdef CONFIG_FREEFALL_DETECTION
            ffDetectingByStrength(strengthValue);            
#endif
            
#ifdef CONFIG_DEBUG_OUTPUT
    
    if(g_ht_data.state==app_state_connected){
        unsigned char  gVar[8];
        unsigned char  *p_value;

        uint32 tmp;
        uint16 xx,yy,zz;
        
        p_value=gVar;
        
        xx=123;
        tmp=strengthValue& 0xFFFF;
        zz=tmp;
        tmp=(strengthValue>>16) & 0xFFFF;
        yy=tmp;
        
        ShortToByteArray(p_value,   (xx));  // 10 bits to 16 bits
        ShortToByteArray(p_value+2, (yy));
        ShortToByteArray(p_value+4, (zz));  
    
        GattCharValueNotification(g_ht_data.st_ucid, 
                              HANDLE_AT_DEBUG_OUTPUT, 
                              6, 
                              gVar);
 
    }
#endif
            
            
        }         
    }                             
#endif

#ifdef CONFIG_HAND_GESTURE    
    //-- if(touchIsPowerDown()==TRUE) //-- 09.03 no meaning
    {
        checkHandGesture(); // to check if there is a hand gesture of tapping the lens
    }
#endif
    
    if(gGsensorMode==GSENSOR_WAKE){ //-- 150511 only check step while gsensor is active
        stepCheck();
    }    
}