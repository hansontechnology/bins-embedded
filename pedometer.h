

/*  
    Platform condition:
        System timer in second the reoslution is.

    Data sructure :
        keep record of g-sensor values (weigthed averaged) up to 300.

    Data flow:    
    0. Value normalization  
        0.0 For each X, Y, Z, find the historical data in last two, and result a weighted average value.
        0.1 The averaging action must comes with consecutive data collection within a short time window, such as 1 second. 
            More thanthis period, will consider as an independent data.            
    1. Check for hand gesture
        1.1 calculate G sensor signal strength, if it is strong than a threshold, then trigger UI activities defined.
    2. Check for new step cycle
        2.1 if collected active data > 300, recalculate threshold
        2.1 calculate the slope of G sensor data.
        2.3 detect peak (a curve from high to low) event.
        2.4 if peak event is detected, and the values compliance to condition of high threshold and low threshold, 
            then add step counter for that time stamp in minute.
    
    Utilities:
    UI activities: first Time, second number of steps in percentage.
        Time: Hour, [flash three times in second], minutes. [flash three times in second]
        Steps: On sequentially, with 300ms for each light on to next.
               
        
*/

#ifndef _PEDOMETER_H_
#define _PEDOMETER_H_

#include <types.h>
#include "configuration.h"

//--10.09 #define BINS_CONFIG_LED_BIT_SHOW_LOCAL_CLOCK    10
#define BINS_CONFIG_LED_BIT_SHOW_UI_ONETIME     11
//--10.09 #define BINS_CONFIG_LED_BIT_DAILY_ACHIEVE       12
//--10.09 #define BINS_CONFIG_LED_BIT_DEMO_MODE           13
#define BINS_CONFIG_RECORD_NOTIFY               14

// #define BINS_CONFIG_LED_MASK_LOCAL_CLOCK        (1<<BINS_CONFIG_LED_BIT_SHOW_LOCAL_CLOCK)
#define BINS_CONFIG_LED_MASK_UI_ONETIME         (1<<BINS_CONFIG_LED_BIT_SHOW_UI_ONETIME)
// #define BINS_CONFIG_LED_MASK_DAILY_ACHIEVE      (1<<BINS_CONFIG_LED_BIT_DAILY_ACHIEVE)
// #define BINS_CONFIG_LED_MASK_DEMO_MODE       (1<<BINS_CONFIG_LED_BIT_DEMO_MODE)
#define BINS_CONFIG_MASK_RECORD_NOTIFY          (1<<BINS_CONFIG_RECORD_NOTIFY)


typedef struct StStepRecord
{
    uint16 relMinute;
    uint16 stepCount;
} StepRecord;

#define STEP_COUNT(sr) ((sr)->stepCount&0x7FFF)
#define SET_STEP_COUNT(sr,v) ((sr)->stepCount= ( ((sr)->stepCount&0x8000) | ((v)&0x7FFF) ))
#define ADD_STEP_COUNT(sr,v) (SET_STEP_COUNT(sr,STEP_COUNT(sr)+v))
#define ACTIVITY_TYPE(sr) (((sr)->stepCount&0x8000)>>15)
#define ACTIVITY_TYPE_BIT15(sr) ((sr)->stepCount&0x8000)

extern StepRecord *stepRecordGetFromTail(void);
extern StepRecord *stepRecordAvailGetFromTail(void);
extern void stepRecordDeleteFromTail(void);

extern uint16 StepRecordMinute(StepRecord *sr);
extern void StepRecordMarkUploaded(StepRecord *sr);
extern uint16 StepRecordCount(void);

extern void pedometerInit(void);
extern void pedometerNewDataInput(uint16 x, uint16 y, uint16 z);


extern uint32 timeInSecond(uint16 *timeInMs);

extern uint32 timeInMinute(uint16 *timeInMs);

extern uint16 systemTimeInMinute(void);

extern void  systemTimeReset(void);

extern void  setLastUploadTime(void);

extern uint8 gLocalClock, gLocalHour, gLocalMinute; //-- for local clock
// extern uint16 gFuncTimerBase[3];
extern uint32 gFuncTimerBase;

extern uint16 gActivityGoal;
extern uint16 gConfiguration;
extern uint32 gUiSenseStrength;
extern uint8  gGsensorMode;

#define GSENSOR_WAKE    (0x01)     //-- from 8653 g-sensor operation mode status 
#define GSENSOR_SLEEP   (0x02)

 

//-- only for first time to initiate values 
extern void pedometerReset(void);
extern void uiActionStart(void);
extern void uiAlertEnable(bool enable);
extern void uiMessageNotice(bool enable);
extern void uiShowNewCycle(void);
extern void clockSync(uint16 newClockTime);

extern void uiChargingEnable(bool enable);
extern void pedometerSleep(bool isWakeup);
extern bool gsensorIsAwake(void);


typedef struct stGsensorData
{
    int16 x, y, z;
} GsensorData;

extern GsensorData *gsensorDataGet(uint16 relIndex);


// local time
void clockGet(uint16 *clockHour, uint16 *clockMinute);
uint16 activityTheDay(void);
void dayStepCounterReset(bool enable); //-- 2015.01.15


extern StepRecord *stepRecordTop(void);
extern void stepRecordAdd(uint16 relMinute, uint8 numSteps);
extern uint32 sysTimeMinutes;
extern void setStepRecordNotify(StepRecord *sr);
extern int8 gShakeThreshold;   // 09.30
extern bool gUILocked; // 10.09
extern void shakeThresholdChange(int levelAdd); // 10.10
extern void shakeThresholdCheck(void);
extern void gestureTrackStart(void);
extern void clockAlarmStart(void);
extern void clockAlarmStop(void);
extern void dayStepCounterModeSwitch(void);

#define SHAKE_THRESHOLD_LEVEL_DEFAULT 1

#ifdef CONFIG_UI_GESTURE_TRACK

#define DEFAULT_UI_GESTURE_ANGLE   40       // in percentage of 
#define DEFAULT_UI_GESTURE_WTIME   2000     // in mili seconds

#endif

#define CLOCK_ALARM_ENABLED ((g_at_serv_data.clock_alarm & 0x8000)!=0)
#define CLOCK_ALARM_HOUR    ((g_at_serv_data.clock_alarm & 0x7F00)>>8)
#define CLOCK_ALARM_MINUTE  (g_at_serv_data.clock_alarm & 0x00FF)


#endif /* _PEDOMETER_H_ */