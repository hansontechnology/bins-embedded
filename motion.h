/*******************************************************************************
 *    Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 * FILE
 *    motion.h
 *
 *  DESCRIPTION
 *    
 *
 ******************************************************************************/

#ifndef __MOTION_H__
#define __MOTION_H__

#include "configuration.h"

/*=============================================================================
 *  Local Header Files
 *============================================================================*/

/*=============================================================================
 *  Public definitions
 *============================================================================*/

/* Possible return codes when trying to obtain new motion data */
typedef enum {
    MOTION_NEW_DATA,    /* New data is available */
    MOTION_NO_DATA,     /* No new data is available */
    MOTION_WARM_UP      /* No new data is available because the devices are not ready */
} MOTION_T;
/*=============================================================================
 *  Public function definitions
 *============================================================================*/
 
/* Initialise the "motion" module */
extern void motionInit(void);

/* Create the next HID report containing motion data */
extern MOTION_T motionCreateNextReport(void);

//--
extern bool motionSlow(void);

/* Set the correct state for sending motion data */
extern void motionSetState(void);

/* Set the accelerometer/gyroscope into low-power state */
extern void motionSetLowPowerMode(void);

extern void motionConfigureActiveMode(void);
extern void motionHandleInterrupt(uint32 pio);
extern void motionResetAccelerometerStabilityVariables(void);
extern void motionSensorWatchdog(void);
extern void motionSleep(void);
extern void motionWakeup(void);

#ifdef CONFIG_FREEFALL_DETECTION
void   ffDetectingByStrength(uint32 strengthValue);   
void   ffInit(void);

#endif

#endif