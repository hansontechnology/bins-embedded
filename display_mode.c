/**
---------------------------------------------------------------------
**
**  This device driver was created by 
**  8-Bit Single-Chip Microcontrollers
**
**  Copyright(C) visionox display Corporation 2002 - 2009
**  All rights reserved by Kunshan visionox display Corporation.
**
**  This program should be used on your own responsibility.
**
**  Filename :	Display_Mode.c
**  Abstract :	This file implements device driver for Display_Mode function.
**
**  Device :	STC89LE516RD
**
**  Compiler :	KEIL uVision2 
**
**  Module:  M01030
**
**  Dot Matrix: 128*32
**
**  Display Color: White
**
**  Driver IC: SH1106G
**
**  Edit : 
**
**  Creation date:
---------------------------------------------------------------------
**/

#include <time.h>

#include "i2c_comms.h"

#include "display_hw.h"
#include "display_init.h"
#include "display_mode.h"
#include "Photo_Show.h"

#if 0//-- 07.28
//
//

uchar ROW[3][4]={
{0x49,0x92,0x24,0x49},
{0x92,0x24,0x49,0x92},
{0x24,0x49,0x92,0x24}
};

extern uint8 tab_1[];
extern uint8 tab_2[];

#endif

/******************************************************
//
//清屏显示模式
//
******************************************************/
//
//

void Display_Clear_Screen()
{
   uchar page_number,column_number;

   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
     {
        displayWriteData(STATE_MIN);
     }
   }
}

#if 0   //-- 07.28

/******************************************************
//
//全屏显示模式
//
******************************************************/
//
//

void Display_All_Screen()
{
   uchar page_number,column_number;
   
   i2cUsePeripheralBus();

   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
     {
        displayWriteData(STATE_MAX);
     }
   }
   
   i2cUseMainBus();

}



/******************************************************
//
//隔行135显示模式
//
******************************************************/
//
//

void Display_Row_Show135()
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
     {
        displayWriteData(STATE_55);
     }
   }
}

/******************************************************
//
//隔行246显示模式
//
******************************************************/
//
//

void Display_Row_Show246()
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
     {
        displayWriteData(STATE_AA);
     }
   }
}
/******************************************************
//
//隔行147显示模式
//
******************************************************/
//
//

void Display_Row_Show147()
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
     {
        displayWriteData(ROW[0][page_number]);
     }
   } 
}
/******************************************************
//
//隔行258显示模式
//
******************************************************/
//
//

void Display_Row_Show258()
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
     {
        displayWriteData(ROW[1][page_number]);
     }
   }
}

/******************************************************
//
//隔行369显示模式
//
******************************************************/
//
//

void Display_Row_Show369()
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
     {
        displayWriteData(ROW[2][page_number]);
     }
   }
}


/******************************************************
//
//隔列135显示模式
//
******************************************************/
//
//

static void Display_Column_Show1(void)
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX/2);column_number++)
     {
        displayWriteData(STATE_MAX);
        displayWriteData(STATE_MIN);
     }
   }
}

/******************************************************
//
//隔列246显示模式
//
******************************************************/
//
//

static void Display_Column_Show2(void)
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX/2);column_number++)
     {
        displayWriteData(STATE_MIN);
        displayWriteData(STATE_MAX);
     }
   }
}


static void Display_Column_Show3(void)
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX/6);column_number++)
     {
	    displayWriteData(STATE_MAX);
        displayWriteData(STATE_MIN);
        displayWriteData(STATE_MIN);
		displayWriteData(STATE_MAX);
		displayWriteData(STATE_MIN);
        displayWriteData(STATE_MIN);
     }
   }
}


static void Display_Column_Show4(void)
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX/6);column_number++)
     {
	    displayWriteData(STATE_MIN);
        displayWriteData(STATE_MAX);
        displayWriteData(STATE_MIN);
		displayWriteData(STATE_MIN);
        displayWriteData(STATE_MAX);
        displayWriteData(STATE_MIN);
     }
   }
}


static void Display_Column_Show5(void)
{
   uchar page_number,column_number;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX/6);column_number++)
     {
	    displayWriteData(STATE_MIN);
	    displayWriteData(STATE_MIN);
        displayWriteData(STATE_MAX);
        displayWriteData(STATE_MIN);
		displayWriteData(STATE_MIN);
        displayWriteData(STATE_MAX);        
     }
   }
}


/******************************************************
//
//边框显示模式128*36
//
******************************************************/
//
//

void Display_Frame_Show()
{
   uchar page_number,column_number;
   displayWriteCommand(START_PAGE);
   displayWriteCommand(START_HIGH_BIT);
   displayWriteCommand(START_LOW_BIT);
   displayWriteData(STATE_MAX);
   for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX-2);column_number++)
   {
      displayWriteData(FRAME_HIGH_ROW);
   }
   displayWriteData(STATE_MAX);
   for(page_number=DISPLAY_MIN+1;page_number<PAGE_TOTAL/2;page_number++)
   {
     displayWriteCommand(START_PAGE+page_number);
     displayWriteCommand(START_HIGH_BIT);
     displayWriteCommand(START_LOW_BIT);
     displayWriteData(STATE_MAX);
     for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX-2);column_number++)
     {
        displayWriteData(STATE_MIN);
     }
     displayWriteData(STATE_MAX);
   }
   displayWriteCommand(START_PAGE+3);
   displayWriteCommand(START_HIGH_BIT);
   displayWriteCommand(START_LOW_BIT);
   displayWriteData(STATE_MAX);
   for(column_number=DISPLAY_MIN;column_number<(COLUMN_MAX-2);column_number++)
   {
      displayWriteData(FRAME_LOW_ROW);
   }
   displayWriteData(STATE_MAX);

}

//
//

/******************************************************
//
//棋盘格1
//
******************************************************/
//
//

void Display_Chess_Board1()
{
   uchar page_number,column_number_1,column_number_2,board;
   board=0xFF;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
	  displayWriteCommand(START_PAGE+page_number);
      displayWriteCommand(START_HIGH_BIT);
      displayWriteCommand(START_LOW_BIT);
	  for(column_number_1=DISPLAY_MIN;column_number_1<COLUMN_MAX/8;column_number_1++)
	  {
		 for(column_number_2=DISPLAY_MIN;column_number_2<COLUMN_MAX/16;column_number_2++)
	     {
	   	    displayWriteData(board);
		 }
         board=~board;
	  }
	  board=~board;
   }
}	

/******************************************************
//
//棋盘格2
//
******************************************************/
//
//

void Display_Chess_Board2()
{
   uchar page_number,column_number_1,column_number_2,board;
   board=0x00;
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
	  displayWriteCommand(START_PAGE+page_number);
      displayWriteCommand(START_HIGH_BIT);
      displayWriteCommand(START_LOW_BIT);
	  for(column_number_1=DISPLAY_MIN;column_number_1<COLUMN_MAX/8;column_number_1++)
	  {
		 for(column_number_2=DISPLAY_MIN;column_number_2<COLUMN_MAX/16;column_number_2++)
	     {
	   	    displayWriteData(board);
		 }
         board=~board;
	  }
	  board=~board;
   }
}

/******************************************************
//
//列扫描显示模式
//
******************************************************/
//
//

void Display_Column_Scan()
{
   bool result; 
   uchar page_number,column_number_1,column_number_2;
   i2cUsePeripheralBus();

   for(column_number_1=DISPLAY_MIN;column_number_1<COLUMN_MAX;column_number_1++)
   {
      for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
      {
         displayWriteCommand(START_PAGE+page_number);
         displayWriteCommand(START_HIGH_BIT);
         displayWriteCommand(START_LOW_BIT);
         for(column_number_2=DISPLAY_MIN;column_number_2<COLUMN_MAX;column_number_2++)
         {
		    if(column_number_2==column_number_1)
		        displayWriteData(STATE_MAX);
		    else displayWriteData(STATE_MIN);
		 }
      } 
      TimeWaitWithTimeout16(FALSE, (60*MILLISECOND), result);
   }
   
   i2cUseMainBus();

}

//
//
/******************************************************
//
//程序版本信息
//
******************************************************/
//
//

void Display_Program_Edit()
{
   uchar page_number,column_number;
   uint piexl=DISPLAY_MIN;
   i2cUsePeripheralBus();
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
      displayWriteCommand(START_PAGE+page_number);
      displayWriteCommand(START_HIGH_BIT);
      displayWriteCommand(START_LOW_BIT);
      for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
      {
		 displayWriteData(tab_1[piexl]);
		 piexl++;
	  }
  }
   i2cUseMainBus();   
}


//
//
/******************************************************
//
//图片检测函数
//
******************************************************/
//
//

void Display_Photo_Show()
{
   uchar page_number,column_number;
   uint piexl=DISPLAY_MIN;
   
   i2cUsePeripheralBus();
   for(page_number=DISPLAY_MIN;page_number<PAGE_TOTAL/2;page_number++)
   {
      displayWriteCommand(START_PAGE+page_number);
      displayWriteCommand(START_HIGH_BIT);
      displayWriteCommand(START_LOW_BIT);
      for(column_number=DISPLAY_MIN;column_number<COLUMN_MAX;column_number++)
      {
		 displayWriteData(tab_2[piexl]);
		 piexl++;
	  }
  }
  i2cUseMainBus();
   
}


//
//

/******************************************************
//
//LED指示灯 和 VCC_Change电压控制函数
//
******************************************************/
//
//

void LED_VCC_Control(uchar Control_State)
{
  uchar value;
  value = Control_State;
  switch(value)
  {
     case 0: 
             {Delay_Ms(1);LED_Work=low;LED_Manual=high;LED_Auto=low;VCC_Change=low;} break;
     case 1: 
             {Delay_Ms(1);LED_Work=low;LED_Manual=low;LED_Auto=high;VCC_Change=low;} break;
	 default: ;
  }
}


/******************************************************
//
//按键处理函数
// *******手动按键检测
// *******自动按键检测
//
******************************************************/
//
//

void Key_Processor(uchar key_proc)
{
   uchar i,n;
   i=46;
   if((key_proc & 0x01) == 0)                     //自动检测
   {
      LED_VCC_Control(Auto);
      while(1)
	  {
	     Clear_Screen();
		 Delay_s(3);
		 All_Screen();
		 Delay_s(3);
		 Frame_Show();
		 Delay_s(3);
	     Column_Scan();
		 Clear_Screen();
		 Delay_s(1);
	     Photo_Show();
	     Delay_s(3);
	  }
      LED_Manual=high;LED_Auto=high;
      All_Screen();
   }
   else if((key_proc & 0x02) == 0)                           //手动检测
   {
      LED_VCC_Control(Manual);
      switch(n)
	  {
	     case 0:  
		         {Clear_Screen();Delay_Ms(10);Program_Edit();n++;} break;
		 case 1:
		         {Clear_Screen();Delay_Ms(10);All_Screen();n++;} break;
		 case 2:
		         {Clear_Screen();Delay_Ms(10);Clear_Screen();n++;} break;
		 case 3:
		         {Clear_Screen();Delay_Ms(10);Row_Show135();n++;} break;
         case 4:  
		         {Clear_Screen();Delay_Ms(10);Row_Show246();n++;} break;
		 case 5:
		         {Clear_Screen();Delay_Ms(10);Row_Show147();n++;} break;
		 case 6:
		         {Clear_Screen();Delay_Ms(10);Row_Show258();n++;} break;
		 case 7:
		         {Clear_Screen();Delay_Ms(10);Row_Show369();n++;} break;
		 case 8:  
		         {Clear_Screen();Delay_Ms(10);Column_Show1();n++;} break;
		 case 9:
		         {Clear_Screen();Delay_Ms(10);Column_Show2();n++;} break;
		 case 10:  
		         {Clear_Screen();Delay_Ms(10);Column_Show3();n++;} break;
		 case 11:
		         {Clear_Screen();Delay_Ms(10);Column_Show4();n++;} break;
		 case 12:  
		         {Clear_Screen();Delay_Ms(10);Column_Show5();n++;} break;
		 case 13:
		         {Clear_Screen();Delay_Ms(10);Frame_Show();n++;} break;
		 case 14:
		         {Clear_Screen();Delay_Ms(10);Chess_Board1();n++;} break;
		 case 15:  
		         {Clear_Screen();Delay_Ms(10);Chess_Board2();n++;} break;
		 case 16:
		         {Clear_Screen();Delay_Ms(10);Photo_Show();n = 0;} break;//*/

		 
		 default : break;
	 }
   }
}
	 
#endif //-- if 0

#if 0

uint8 tab_1[]={
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X04,0X3C,0XFC,0XC4,0X40,0X40,0XC4,0XFC,0X3C,0X04,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0XF0,0XF8,0X0C,0X04,0X04,
0X04,0X0C,0X38,0X30,0X00,0X00,0X30,0X38,0X0C,0X04,0X04,0X0C,0X38,0XF0,0XC0,0X00,
0X00,0X00,0X00,0X00,0X04,0XFC,0XFC,0X04,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0XE0,0XF8,0X1C,0X04,0X04,0X04,0X1C,0XF8,0XE0,
0X00,0X00,0X00,0X00,0X04,0X04,0XFC,0XFC,0X04,0X04,0X00,0X00,0X00,0X10,0X1C,0X0C,
0X84,0XC4,0X64,0X34,0X1C,0X0C,0X00,0X00,0X00,0X00,0X04,0X04,0XFC,0XFC,0X04,0X04,
0X00,0X00,0X00,0XE0,0XF8,0X1C,0X04,0X04,0X04,0X1C,0XF8,0XE0,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X01,0X0F,0X7E,0X7E,0X0F,0X01,0X00,0X00,0X00,
0X01,0X01,0X01,0X01,0X01,0X01,0X01,0X01,0X01,0X00,0X00,0X18,0X3D,0X67,0X42,0X42,
0X42,0X62,0X38,0X18,0X00,0X00,0X10,0X70,0X60,0X40,0X40,0X60,0X38,0X1F,0X07,0X00,
0X00,0X10,0X70,0X60,0X40,0X7F,0X7F,0X40,0X60,0X70,0X10,0X00,0X01,0X01,0X01,0X01,
0X01,0X01,0X01,0X01,0X01,0X00,0X00,0X0F,0X3F,0X70,0X40,0X40,0X40,0X70,0X3F,0X0F,
0X00,0X00,0X00,0X00,0X00,0X00,0X7F,0X7F,0X20,0X20,0X00,0X00,0X00,0X1C,0X3E,0X63,
0X41,0X40,0X40,0X60,0X38,0X18,0X00,0X00,0X00,0X00,0X00,0X00,0X7F,0X7F,0X20,0X20,
0X00,0X00,0X00,0X0F,0X3F,0X70,0X40,0X40,0X40,0X70,0X3F,0X0F,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
};

//
//
//

uint8 tab_2[]={
0X00,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,
0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0XF0,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X60,0X90,0X08,0X08,0X08,0X08,0X10,0X00,0X00,
0XE0,0X10,0X08,0X08,0X08,0X08,0XFF,0X00,0X00,0XF8,0X00,0X00,0X00,0X00,0XF8,0X00,
0X00,0X00,0X00,0XF8,0X00,0X00,0X00,0X18,0X00,0X00,0X10,0X08,0X08,0X08,0X10,0XE0,
0X00,0X00,0XF8,0X00,0X00,0X30,0X48,0XC8,0X88,0X88,0X10,0X00,0X00,0XF8,0X10,0X08,
0X08,0X08,0X18,0XF0,0X00,0X00,0XF8,0X00,0X00,0X00,0X00,0XF8,0X00,0X00,0X00,0X00,
0XF8,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X80,0X80,0X40,0X40,0X20,0X20,0X10,0X10,
0X00,0X00,0X1F,0X00,0X3F,0X00,0XFF,0X00,0X01,0X00,0X0F,0X00,0X7F,0X00,0XFF,0X00,
0XFF,0X00,0XFF,0X00,0X1F,0X00,0X03,0X00,0X3F,0X00,0X0F,0X00,0XFF,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X0C,0X1A,0X11,0X11,0X11,0X10,0X08,0X00,0X00,
0X01,0X02,0X04,0X04,0X04,0X02,0X07,0X00,0X00,0X03,0X04,0X04,0X04,0X02,0X03,0X04,
0X04,0X04,0X02,0X07,0X00,0X00,0X00,0X00,0X00,0X00,0X02,0X04,0X04,0X04,0X02,0X01,
0X00,0X00,0X17,0X00,0X00,0X02,0X04,0X04,0X04,0X04,0X03,0X00,0X00,0X07,0X00,0X00,
0X00,0X00,0X00,0X07,0X00,0X00,0X03,0X04,0X04,0X04,0X02,0X03,0X04,0X04,0X04,0X02,
0X07,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X01,0X01,0X02,0X02,0X04,0X04,0X08,0X08,
0X00,0X00,0X80,0X80,0X80,0X00,0X00,0X00,0X80,0X80,0X80,0X80,0X80,0X00,0X01,0X00,
0X8F,0X00,0X01,0X00,0X00,0X80,0X80,0X80,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X80,
0X40,0X20,0X20,0X20,0X20,0X40,0X80,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X80,0X80,0X00,0X00,0XA0,0X60,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X40,0X20,0X20,0X20,0X20,0X20,0X40,0X80,0X00,0X00,
0X00,0X1F,0X20,0X20,0X20,0X1F,0X00,0X00,0X00,0X00,0X3F,0X10,0X10,0X00,0X00,0X00,
0X0D,0X00,0X00,0X00,0X1F,0X20,0X20,0X20,0X1F,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X0E,0X15,0X24,
0X5F,0X8E,0X84,0X84,0X8E,0X5F,0X24,0X15,0X0E,0X00,0X00,0X00,0X00,0X00,0X00,0X3F,
0X21,0X21,0X29,0X21,0X29,0X29,0X2D,0X2F,0X2D,0X29,0X3F,0X00,0X00,0X00,0X00,0X00,
0X00,0X38,0X44,0XBA,0XAA,0XBA,0X43,0X3C,0X02,0X01,0X00,0X00,0X00,0X00,0X00,0X00,
0X00,0X00,0X04,0X06,0X1F,0X26,0X44,0X80,0X80,0X80,0X80,0X84,0X0C,0X1F,0X0C,0X04,
};

//
//
//
#endif