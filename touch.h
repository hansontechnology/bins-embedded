
#ifndef __TOUCH_H__
#define __TOUCH_H__

#include "configuration.h"
#include "ht_hw.h"
#include "pedometer.h"
#include "activity_tracking_service.h"
#include "health_thermometer.h"



#define	TOUCH_I2C_ADDR 0x33  //-- 0x33 of 7 bits ID0==0, ID1==0

#define TOUCH_I2C_WRITE_ADDR 0x66 //--0xD6 0x66 //-- shift 1
#define TOUCH_I2C_READ_ADDR  0x67 // 0x67  // a | 0x01
void touchInit(void);
void touchSleep(void);
void touchPowerdown(bool onoff);
void touchSessionEnd(uint8 timeWaitBeforeDown);

void touchNewSession(void);
bool touchFastSwitch(void);
bool touchPressed(void);
bool touchSensorStatus(void);

void touchHeartRateUpdate(uint8 currentHR);
uint8 getLatestHeartRate(void);
void touchHeartRateStartMeasure(void);
void touchHeartRateStartAll(void);
void touchRestartHeartRateReading(void);
bool touchHeartRatePollRunning(void);

uint8 getCurrentHeartRate(void);

void touchAlert(bool onoff);
extern bool touchAlertIsRunning(void);
extern void touchHeartRateBatchCycle(bool isDisconnectAfterCycle); // 150712
extern uint32 gTouchSleepTime;
extern bool gIsTouchHeartRateQueued;

extern bool gIsTouchLocked;   //-- 08.20
extern uint8 gCurrentHR;    //-- 2015.01.31

#define TOUCH_WAKE_PERIOD       5   // 09.02 10  //-- in second, 15 seconds are too long
                                    // once after touch is covered, stay wake longer 
#define TOUCH_WAKE_WITH_NOACTION 3  // 09.02 5  
                                    // For the case just wokeup, 
                                    // if no touch action happens, then go sleep earlier

#endif