/**
---------------------------------------------------------------------
**
**  This device driver was created by 
**  8-Bit Single-Chip Microcontrollers
**
**  Copyright(C) visionox display Corporation 2002 - 2009
**  All rights reserved by Kunshan visionox display Corporation.
**
**  This program should be used on your own responsibility.
**
**  Filename :	IC_Init.h
**  Abstract :	This file implements device driver for Initial SH1106 IC function.
**
**  Device :	STC89LE516RD
**
**  Compiler :	KEIL uVision2 
**
**  Module:  M01030
**
**  Dot Matrix: 128*32
**
**  Display Color: White
**
**  Driver IC: SH1106G
**
**  Edit : 
**
**  Creation date:	
---------------------------------------------------------------------
**/
#include "display_hw.h"

#ifndef  _DISPLAY_IC_Init_

#define  _DISPLAY_IC_Init_

//-- void IC_IIC_Start();
//-- void IC_IIC_Stop();
//-- not necessary bit Write_IIC_Data(uchar Data);
extern bool displayWriteCommand(uchar command);    // Write_Command(uchar command);
extern bool displayWriteData(uchar data);  // Write_Data(uchar data);
extern void displayReset(void);    // Reset_IC();
extern void displayVcc(bool onoff);  //Enable or Disable display VCC
extern bool displayInit(void);     // Init_IC();

#endif
