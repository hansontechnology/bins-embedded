/*******************************************************************************
 *    Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 * FILE
 *    accel_mma8653fc.c
 *
 *  DESCRIPTION
 *    This file implements the device driver for the MMA8653FC 
 *    accelerometer.
 *
 ******************************************************************************/

#include "configuration.h"

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <mem.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/
#include "accelerometer.h"
#include "accel_mma8653fc.h"
#include "registers_accel_mma8653fc.h"
#include "i2c_comms.h"

/*============================================================================*
 *  Private Definitions
 *============================================================================*/
#define ALL_BITS    0xff

#define readRegister(_r_, _b_)  i2cReadRegister(MMA8653FC_WRITE_ADDR, _r_, _b_)
#define readRegisters(_r_, _l_, _b_)    i2cReadRegisters(MMA8653FC_WRITE_ADDR, _r_, _l_, _b_)

/*============================================================================*
 *  Private Function Implementations
 *============================================================================*/
/*----------------------------------------------------------------------------*
 *  NAME
 *      writeRegister
 *  DESCRIPTION
 *      Write the supplied value to the specified register of the MMA8653FC.
 *  RETURNS
 *      TRUE: transaction appeared to be successful
 *---------------------------------------------------------------------------*/
static bool writeRegister(uint8 reg, uint8 mask, uint8 values)
{
    uint8 registerValue;
    
    if (mask == ALL_BITS)
    {
        registerValue = values;
    }
    else
    {
        readRegister(reg, &registerValue);
    
        registerValue &= ~mask;
        registerValue |= values;
    }
    
    return i2cWriteRegister(MMA8653FC_WRITE_ADDR, reg, registerValue);
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      isWorking
 *  DESCRIPTION
 *      Verify that the MMA8653FC is present and responding to I2C communications.
 *  RETURNS
 *      TRUE: the MMA8653FC appears to be working.
 *---------------------------------------------------------------------------*/
bool gResult;
uint8 gVer;
static bool isWorking(void)
{
    uint8 verificationByte;
    
#if 0    
    return ( readRegister(MMA8653FC_WHO_AM_I, &verificationByte) &&
            (verificationByte == 0x5A));    //-- 5A means 8653
#else
    uint8 result;

    result=readRegister(MMA8653FC_WHO_AM_I, &verificationByte);
    gResult=result;
    gVer=verificationByte;
    if(result && (verificationByte == 0x5A))    //-- 5A means 8653
        return TRUE;
    else   
        return FALSE;
    
#endif
    
}

/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/
 
/*----------------------------------------------------------------------------*
 *  NAME
 *      mma8653fcInit
 *  DESCRIPTION
 *      General initialisation of the MMA8653FC for this application.
 *  RETURNS
 *      TRUE: the MMA8653FC appears to be working
 *---------------------------------------------------------------------------*/
extern bool mma8653fcInit(void)
{
    bool result;
    result=isWorking();
    if(result == 0)
    {
        /* Disable the device */
        //-- result= mma8653fcEnableDataCollection(FALSE);
            
        //-- start to monitor g-sensor data
        //-- not use it anymore, mma8653fcEnableShallowSleepWithInterrupts();       
        mma8653fcEnableLowPowerDataCollection(TRUE);
    }
    return result;
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      mma8653fcClearInterrupt
 *  DESCRIPTION
 *      Clear outstanding interrupts.
 *  RETURNS
 *      TRUE
 *---------------------------------------------------------------------------*/
extern bool mma8653fcClearInterrupt(void)
{
    uint8 intReason;
    uint16 x, y, z;
    
    /* Reading the interrupt source and data registers should clear the 
     * interrupt.
     */
    return(readRegister(MMA8653FC_INT_SOURCE, &intReason) &&
           readRegister(MMA8653FC_FF_MT_SRC, &intReason) &&
           mma8653fcReadData(&x, &y, &z));
}
       
/*----------------------------------------------------------------------------*
 *  NAME
 *      mma8653fcInterruptHandler
 *  DESCRIPTION
 *      The MMA8653FC interrupt handler.
 *  RETURNS
 *      The return value indicates whether new data is available (or not).
 *---------------------------------------------------------------------------*/
extern bool mma8653fcInterruptHandler(void)
{
    uint8 intReason;

    /* Read the interrupt source register to determine why we are here */
    readRegister(MMA8653FC_INT_SOURCE, &intReason);

    if(intReason == 0x04)   /* SRC_FF_MT */
    {
            /* This is a motion interrupt which means the user is moving the 
             * device. It is active only when we are in shallow-sleep mode.
             */
            readRegister(MMA8653FC_FF_MT_SRC, &intReason);


            //-- mma8653fcExitShallowSleep(); 
            //-- we don't exit from ShallowSleep mode always
    }
    
    
    return TRUE;
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      mma8653fcReadData
 *  DESCRIPTION
 *      Read 6-bytes of X,Y,Z data from the accelerometer.
 *  RETURNS
 *      The number of bytes of data read.
 *---------------------------------------------------------------------------*/
extern uint8 mma8653fcReadData(uint16 *x, uint16 *y, uint16 *z)
{
#define ACCELEROMETER_DATA_LENGTH   6   /* 2-bytes each of x,y,z */

    bool success;
    uint8 dataBuffer[ACCELEROMETER_DATA_LENGTH];
    

    /* Read the new data */
    success = readRegisters(MMA8653FC_OUT_X_MSB, ACCELEROMETER_DATA_LENGTH, dataBuffer);    

#if 0    
    {
        uint8 status;
        readRegister(MMA8653FC_STATUS, &status); //-- to make sure clear status 
    }
#endif
    
    if(success)
    {
#if 0  //-- 10 bit valid data      
        /* The data is returned MSB first.
         * Since this data is 10-bit, that means the top 6 bits of the MSB
         * will be zero in all cases.
         */
    
        *x = dataBuffer[0]; // MSB byte 10-2 bits
        *x <<= 2;
        *x |= (dataBuffer[1] >> 6);  // LSB only 2 bits available for 8653
        
        *y = dataBuffer[2];
        *y <<= 2;
        *y |= (dataBuffer[3] >> 6);
        
        *z = dataBuffer[4];
        *z <<= 2;
        *z |= (dataBuffer[5] >> 6);
#else
        // 12 bit valid data
        
        *x = dataBuffer[0];         // 12-4 bits
        *x <<= 4;
        *x |= (dataBuffer[1] >> 4);  // LSB 4 bits available for 8652, 
                                     // should be compatible to 8643
        
        *y = dataBuffer[2];
        *y <<= 4;
        *y |= (dataBuffer[3] >> 4);
        
        *z = dataBuffer[4];
        *z <<= 4;
        *z |= (dataBuffer[5] >> 4);        
        
#endif        
    }

#if 0
    {
        volatile int16 pp=-0x100;
        volatile int16 pp2=-0x1ff;
        volatile int16 pp3=-0x1;
        
        *x=pp;
        *y=pp2;
        *z=pp3;
    }
#endif
    
    return success;
}

//-------------- for BINS


/*----------------------------------------------------------------------------*
 *  NAME
 *      mma8653fcEnableDataCollection
 *  DESCRIPTION
 *      Enable/disable full-rate data collection.
 *  RETURNS
 *      TRUE
 *---------------------------------------------------------------------------*/
extern bool mma8653fcEnableLowPowerDataCollection(bool enable)
{
    writeRegister(MMA8653FC_CTRL_REG1,
                  MMA8653FC_ACTIVE,
                  0x00);
    /* Power-down the chip */  //-- entering standby mode
    
    if(enable)
    {
       writeRegister(MMA8653FC_CTRL_REG1,
                  (MMA8653FC_ASLP1 | MMA8653FC_ASLP0 | MMA8653FC_DR2 | MMA8653FC_DR1 | MMA8653FC_DR0),
                  (MMA8653FC_ASLP1 | MMA8653FC_ASLP0 | MMA8653FC_DR2 | MMA8653FC_DR1));
        /* Sleep  ASLP_RATE rate is 1.56Hz
        * Active output rate is 6.25Hz
        */
        /* Output Data rate: 6.25Hz (110), 1.56Hz is (111) */
 
 
        writeRegister(MMA8653FC_CTRL_REG2,
                  ALL_BITS,
         //--  disable sleep (MMA8653FC_SMODS1 | MMA8653FC_SMODS0 | MMA8653FC_MODS0));
                (MMA8653FC_SMODS1 | MMA8653FC_SMODS0 | MMA8653FC_SLPE | MMA8653FC_MODS0));
        /* Auto-sleep enabled
        * Sleep power mode: low power : 11: Low power
        * Active mode power mode is : 01 : low power low noise 
        */
        
        writeRegister(MMA8653FC_FF_MT_THS,  //-- 0x17 Threshold
                      ALL_BITS, 
                      0x80 | MMA8653_GSENSOR_SLEEP_DEFAULT );      //-- 09.26 set to 20, from 16,  64 50 42(h) 40 36 (ok to l) 25 (l) 16 is 1g, 17,, 8 17 25 is around 1.575g
        /* --Freefall Motion Threshold
         * DBCNTM bit = 1: 0x80 clear the debounce, 0;increments or decrements debounce 
         * 16 counts = 0.063g/LSB * 16 , threshold resolution is 0.063/LSB
         */

        writeRegister(MMA8653FC_FF_MT_COUNT,    //-- debounce counter register
                      ALL_BITS, 
                      0);  //++ Must be set to 0, wake and last until counter seconds
        /* Counter * timestep of 80ms to 160ms depends onthe ODR rate */
        //-- Debounce register
        
        writeRegister(MMA8653FC_FF_MT_CFG,
                  ALL_BITS, 
                  (MMA8653FC_ELE | MMA8653FC_OAE | MMA8653FC_ZEFE | MMA8653FC_YEFE | MMA8653FC_XEFE));
        /* Enable motion interrupts on x,y,z axes.//-- Use it as a motion detection
         * Interrupt is latched.
         */    

        
        //-- time in seconds to transit from wake to sleep
        writeRegister(MMA8653FC_ASLP_COUNT,
                      ALL_BITS, 
                      25);    //--** 0x10);    //-- in seconds, to sleep from wake   

        //-- interrupt control register
        writeRegister(MMA8653FC_CTRL_REG3,
                     (MMA8653FC_TRANS | MMA8653FC_LNDPRT | MMA8653FC_PULSE | MMA8653FC_FF_MT | MMA8653FC_IPOL),    
                    (MMA8653FC_FF_MT | MMA8653FC_IPOL));
        /*  0x08 WAKE_FF_MT motion interrupt can wake system
        *   0x02 IPOL interrupt is active HIGH
        */ 
        
        
        writeRegister(MMA8653FC_CTRL_REG4,
                    ALL_BITS,
                    MMA8653FC_IASLP | MMA8653FC_IFF_MT | MMA8653FC_IDRDY);   //-- Auto Sleep / Wake Interrupt Enable
        
        /* //-- only two interrupt sources wake the system: FF_MT and LNDPRT */
        /* Data Ready Interrupt too */
        
        //--** assign to interrupt line, INT1 == WAKE, INT2 == PIO9   
        writeRegister(MMA8653FC_CTRL_REG5,
                  ALL_BITS,
                  MMA8653FC_CASLP | MMA8653FC_LNDPRT | MMA8653FC_CFF_MT | MMA8653FC_CDRDY);
        /* FF_MT interrupt routed to int1 pin
         * Others data ready landscape/portrait, sleep/wake 
         * 1 means INT1, 0 means INT2
         */
                
        writeRegister(MMA8653FC_CTRL_REG1,
                      MMA8653FC_ACTIVE,
                      MMA8653FC_ACTIVE);
        /* Switch to "normal" mode */
    }
    
    return TRUE;
}

//-- settings that, if no movement detected that has stronger than the predefined threshold, then go to sleep mode.

void mma8653EnterSleep()
{
    
        writeRegister(MMA8653FC_CTRL_REG1,
                  MMA8653FC_ACTIVE,
                  0x00);
     
        // back to lower g value
        writeRegister(MMA8653FC_FF_MT_THS,  //-- 0x17 Threshold
                      ALL_BITS, 
                      0x80 | MMA8653_GSENSOR_SLEEP_DEFAULT    );      //-- 09.26 20 set to, from 16, 08.19 from 16 to 10 to 16, 16 is 1g, 17,, 8 17 25 is around 1.575g
        /* --Freefall Motion Threshold
         * DBCNTM bit = 1: 0x80 clear the debounce, 0;increments or decrements debounce 
         * 16 counts = 0.063g/LSB * 16 , threshold resolution is 0.063/LSB
         */

        // 09.28
        //-- time in seconds to transit from wake to sleep
        writeRegister(MMA8653FC_ASLP_COUNT,
                      ALL_BITS, 
                      1);    //--** 0x10);    //-- in seconds, to sleep from wake   

        //-- interrupt control register
        writeRegister(MMA8653FC_CTRL_REG3,
                      MMA8653FC_FF_MT ,    
                      MMA8653FC_FF_MT); //-- 10.08 enable motion interrupt
        
        writeRegister(MMA8653FC_CTRL_REG2,
                      MMA8653FC_SLPE,
                      MMA8653FC_SLPE);
        /* Auto-sleep enabled
        */        
        
        writeRegister(MMA8653FC_CTRL_REG1,
                  MMA8653FC_ACTIVE,
                  MMA8653FC_ACTIVE);
        
        
}

void mma8653Wakeup()
{
    
        writeRegister(MMA8653FC_CTRL_REG1,
                  MMA8653FC_ACTIVE,
                  0x00);
#ifdef CONFIG_FREEFALL_DETECTION
        
        
        /* Freefall/Motion Configuration Register
         * For Freefall setting, ELE=1, OAE = 0  
         * For Motion setting, ELE=1, OAE = 1
         * where ELE means Event Latch Enable
         */    
        writeRegister(MMA8653FC_FF_MT_CFG,
                  ALL_BITS, 
                  (MMA8653FC_ELE | MMA8653FC_ZEFE | MMA8653FC_YEFE | MMA8653FC_XEFE));
        
        // set it to low enough g value  
        writeRegister(MMA8653FC_FF_MT_THS,  //-- 0x17 Threshold
                      ALL_BITS, 
                     0x80 | 5 );      //-- 150605 from 5, set to 5 for freefall
                                        //-- useful set is 100-500mg
                                        // for amount of time
                                        // only can be set again by the FF_MT_COUNT delay passed
                                        // EA bit of FF_MTG_CFG bit 7
                                        // 6.7
                                        // 8 is working, 
                                        // with 4 too weak, 
                                        // interrupt happens every ~100ms, and continue >600ms
        /* --Freefall Motion Threshold
         * DBCNTM bit = 1: 0x80 clear the debounce, 0;increments or decrements debounce 
         * 16 counts = 0.063g/LSB * 16 , threshold resolution is 0.063/LSB
         * maximum range is to +- 8g, for every cases
         * 0 to 127 the count range is 
         * 500 / 63 = 7,  100 / 63 = 2, for freefall the range should be 2 to 7
         */

        writeRegister(MMA8653FC_FF_MT_COUNT,    //-- debounce counter register
                      ALL_BITS, 
                      1);  
        /* Counter * timestep of 80ms to 160ms depends onthe ODR rate */
        //-- Debounce register

        
        //-- interrupt control register
        writeRegister(MMA8653FC_CTRL_REG3,
                      MMA8653FC_FF_MT ,    
                      MMA8653FC_FF_MT); //-- the freefall motion can generate interrupt
        
#else  // NOT FREEFALL
        // set it to higher g value  
        writeRegister(MMA8653FC_FF_MT_THS,  //-- 0x17 Threshold
                      ALL_BITS, 
                     0x80 | MMA8653_NEW_SENSITIVITY );      //-- 09.02 56, 36, 16 is 1g, 17,, 8 17 25 is around 1.575g
        /* --Freefall Motion Threshold
         * DBCNTM bit = 1: 0x80 clear the debounce, 0;increments or decrements debounce 
         * 16 counts = 0.063g/LSB * 16 , threshold resolution is 0.063/LSB
         */

        //-- interrupt control register
        writeRegister(MMA8653FC_CTRL_REG3,
                      MMA8653FC_FF_MT ,    
                      0); //-- 10.08 disable motion interrupt
#endif  // NOT FREEFALL
        
        writeRegister(MMA8653FC_CTRL_REG2,
                      MMA8653FC_SLPE,
                      0);
        /* Auto-sleep disabled
        */

        writeRegister(MMA8653FC_CTRL_REG1,
                  MMA8653FC_ACTIVE,
                  MMA8653FC_ACTIVE);
        
}

void mma8653DataNotifyEnable(bool isEnable)
{
        writeRegister(MMA8653FC_CTRL_REG1,
                  MMA8653FC_ACTIVE,
                  0);
    
        if(isEnable==TRUE){ 
            
            /* data rate selection 
                DR2:1:0 = 100:50Hz, 110:6.25Hz, 011:100Hz 
            */
            writeRegister(MMA8653FC_CTRL_REG1,
                  (MMA8653FC_DR2 | MMA8653FC_DR1 | MMA8653FC_DR0),
                  (MMA8653FC_DR2 | MMA8653FC_DR1));     // 6.25Hz

            writeRegister(MMA8653FC_CTRL_REG4,
                    MMA8653FC_IDRDY,
                    MMA8653FC_IDRDY);   //-- Data Interrupt Enable

        }
        else{
            
            /* data rate selection 
                DR2:1:0 = 100:50Hz, 110:6.25Hz, 011:100Hz 
            */
            writeRegister(MMA8653FC_CTRL_REG1,
                  (MMA8653FC_DR2 | MMA8653FC_DR1 | MMA8653FC_DR0),
                  (MMA8653FC_DR2));     // 50Hz
           
            writeRegister(MMA8653FC_CTRL_REG4,
                    MMA8653FC_IDRDY,
                    0);   //-- Data Interrupt is disabled temporarily
  
        }    
        writeRegister(MMA8653FC_CTRL_REG1,
                  MMA8653FC_ACTIVE,
                  MMA8653FC_ACTIVE);
}

//------------ for BINS

