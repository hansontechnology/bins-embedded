/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2012-2014
 *  Part of CSR uEnergy SDK 2.3.0
 *  Application version 2.3.0.0
 *
 *  FILE
 *      hr_sensor.c
 *
 *  DESCRIPTION
 *      This file defines a simple Heart Rate Sensor application.
 *
 *****************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/


#include <main.h>
#include <pio.h>
#include <mem.h>
#include <Sys_events.h>
#include <Sleep.h>
#include <timer.h>
#include <security.h>
#include <gatt.h>
#include <gatt_prim.h>
#include <panic.h>
#include <nvm.h>
#include <buf_utils.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/

#include "app_gatt.h"
#include "heart_rate_service.h"
#include "gap_service.h"
#include "battery_service.h"
#include "hr_sensor.h"
//-- #include "hr_sensor_hw.h"
//-- #include "hr_sensor_gatt.h"
#include "app_gatt_db.h"
#include "nvm_access.h"
//-- #include "user_config.h"
#include "configuration.h"  //-- 11.27
#include "touch.h" //-- 11.27
/*============================================================================*
 *  Private Definitions
 *============================================================================*/
/* Maximum number of timers */
#define MAX_APP_TIMERS                 (6)

/*Number of IRKs that application can store */
#define MAX_NUMBER_IRK_STORED          (1)

/* Time after which HR Measurements will be transmitted to the connected 
 * host.
 */
#define HR_MEAS_TIME                   (1 * SECOND)

/* Magic value to check the sanity of NVM region used by the application */
#define NVM_SANITY_MAGIC               (0xAB04)

/* NVM offset for NVM sanity word */
#define NVM_OFFSET_SANITY_WORD         (0)

/* NVM offset for bonded flag */
#define NVM_OFFSET_BONDED_FLAG         (NVM_OFFSET_SANITY_WORD + 1)

/* NVM offset for bonded device bluetooth address */
#define NVM_OFFSET_BONDED_ADDR         (NVM_OFFSET_BONDED_FLAG + \
                                        sizeof(g_hr_data.bonded))

/* NVM offset for diversifier */
#define NVM_OFFSET_SM_DIV              (NVM_OFFSET_BONDED_ADDR + \
                                        sizeof(g_hr_data.bonded_bd_addr))

/* NVM offset for IRK */
#define NVM_OFFSET_SM_IRK              (NVM_OFFSET_SM_DIV + \
                                        sizeof(g_hr_data.diversifier))

/* Number of words of NVM used by application. Memory used by supported 
 * services is not taken into consideration here.
 */
#define NVM_MAX_APP_MEMORY_WORDS       (NVM_OFFSET_SM_IRK + \
                                        MAX_WORDS_IRK)


/* Slave device is not allowed to transmit another Connection Parameter 
 * Update request till time TGAP(conn_param_timeout). Refer to section 9.3.9.2,
 * Vol 3, Part C of the Core 4.0 BT spec. The application should retry the 
 * 'connection paramter update' procedure after time TGAP(conn_param_timeout)
 * which is 30 seconds.
 */
#define GAP_CONN_PARAM_TIMEOUT         (30 * SECOND)


/* TGAP(conn_pause_peripheral) defined in Core Specification Addendum 3 Revision
 * 2. A Peripheral device should not perform a Connection Parameter Update proc-
 * -edure within TGAP(conn_pause_peripheral) after establishing a connection.
 */
#define TGAP_CPP_PERIOD                (5 * SECOND)

/* TGAP(conn_pause_central) defined in Core Specification Addendum 3 Revision 2.
 * After the Peripheral device has no further pending actions to perform and the
 * Central device has not initiated any other actions within TGAP(conn_pause_ce-
 * -ntral), then the Peripheral device may perform a Connection Parameter Update
 * procedure.
 */
#define TGAP_CPC_PERIOD                (1 * SECOND)


#ifdef NO_ACTUAL_MEASUREMENT


/* Dummy RR interval in milliseconds */
#define RR_INTERVAL_IN_MS              (0x1e0)

#else /* NO_ACTUAL_MEASUREMENT */


/* RR intervals are stored in the circular queue in units of 1/1024 seconds,
 * that is, an RR interval of 1 means that the RR interval is 1/1024 s. When
 * heart rate is calculated in beats per minute, the
 * number of RR intervals in queue/sum of RR intervals will have to be
 * multiplied by 60 * 1024. This macro stores this conversion factor
 */
#define CONVERSION_FACTOR              (60 * 1024UL)

#define H_RATE_FORMAT_CONDITION_VALUE  (255)
#endif /* ! NO_ACTUAL_MEASUREMENT */

/* Static value for Energy Expended in KJoules used by example application */

/*============================================================================*
 *  Public Data
 *============================================================================*/

/* HR Sensor application data instance */
//-- HR_DATA_T g_hr_data;

static uint8* receivedHRMeasReading(uint8 *p_length);

/*============================================================================*
 *  Private Function Implementations
 *============================================================================*/


/*----------------------------------------------------------------------------*
 *  NAME
 *      receivedHRMeasReading
 *
 *  DESCRIPTION
 *      This function formulates HR Measurement data in a format
 *      given by HR service specification. It collates received RR Intervals 
 *      and derives HR measurement value from it. Energy expended is added 
 *      to every 10th measurement.
 *
 *  RETURNS
 *      uint8 Pointer - HR Measurement data 
 *
 *---------------------------------------------------------------------------*/
uint8   hr_meas_data[MAX_ATT_HR_NOTI_LEN];

static uint8* receivedHRMeasReading(uint8 *p_length)
{
        uint8 heart_rate;
        
        uint8 *p_hr_meas = hr_meas_data;

        uint8  *p_hr_meas_flags = NULL;

        p_hr_meas_flags = &p_hr_meas[(*p_length)++];

        *p_hr_meas_flags = SENSOR_MEASUREVAL_FORMAT_UINT8 | 
                       SENSOR_IN_CONTACT ;


#ifdef CONFIG_TARGET_BINSX2 // CONFIG_BINSX2
        heart_rate=getCurrentHeartRate();  //-- from touch.c
#endif                      
        p_hr_meas[(*p_length)++] = (uint8)heart_rate;        
    
    return p_hr_meas;
}


/*----------------------------------------------------------------------------*
 *  NAME
 *      sendHRMeasurement
 *
 *  DESCRIPTION
 *      This function sends out one HR Measurement.
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/


void sendHRMeasurement(void)
{
    uint8 *p_hr_meas_data = NULL;
    uint8   hr_meas_len = 0;

    /* If notifications are  configured on the Heart Rate 
     * Measurement characteristic, the application should take 
     * following actions:
     * 1. configure Radio event on Tx data,
     * 2. send a notification.
     * 3. wait for the radio event.
     */
     if(IsHeartRateNotifyEnabled())
     {
       /* Received HR measurement reading */
        p_hr_meas_data = receivedHRMeasReading(&hr_meas_len);
    
        if(hr_meas_len)
        {
            /* Send the Heart Rate Measurement notification. */
            HeartRateSendMeasValue(g_ht_data.st_ucid, 
                                                   hr_meas_len, 
                                                   p_hr_meas_data);
        }

    }
}

