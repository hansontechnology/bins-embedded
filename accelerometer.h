/*******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 *  FILE
 *      accelerometer.h
 *
 *  DESCRIPTION
 *    This file defines the API for a generic accelerometer.
 *
 ******************************************************************************/
#ifndef _ACCELEROMETER_H
#define _ACCELEROMETER_H

/*=============================================================================*
 *  SDK Header File
 *============================================================================*/
#include <types.h>

/*=============================================================================
 *  Local Header Files
 *============================================================================*/
#include "configuration.h"

#if defined(ACCELEROMETER_PRESENT)

/*=============================================================================
 *  Public function prototypes
 *============================================================================*/
 
/* This function initialises the accelerometer. 
 * This needs to be called only once.
 */
extern bool AccelInit(void);

/* This function enables the accelerometer in "normal" mode. 
 * Note that the codec may first require powering-on, if this is 
 * applicable to the HW design.
 */
extern void AccelEnableDataCollection(bool enable);

/* This function configures the XAP (not the accelerometer), to
 * allow/deny interrupts from the accelerometer.
 * Note that denying the interrupt is not the same as clearing the interrupt.
 */
extern void AccelEnableInterruptsIntoXap(bool enable);

/* This function clears the "interrupt" state on the accelerometer.
 * Note that circumstances and configuration may mean that the accelerometer
 * re-asserts the interrupt immediately after it is cleared.
 */
extern void AccelClearInterrupt(void);

/* This function handles the interrupts from the accelerometer.
 */
extern bool AccelInterruptHandler(void);

/* Put the accelerometer into "shallow sleep" state, to save power, but
 * still generate an interrupt when movement is detected. 
 * This mode of operation is not supported by all devices.
 */
extern void AccelEnableShallowSleepWithInterrupts(void);

/* Determine whether the accelerometer is in "shallow sleep" mode. */
extern bool AccelShallowSleepEnabled(void);


/* Read X,Y,Z data from the accelerometer. */
extern bool AccelReadData(uint16 *x, uint16 *y, uint16 *z);

/* Exit the "shallow sleep" state.
 * This function restores the accelerometer to the same state as initialisation
 * followed by enabling.
 * This function must be called when an interrupt is received following enabling
 * of shallow-sleep mode.
 * This state is not supported by all devices.
 */
extern void AccelExitShallowSleep(void);

#endif /* ACCELEROMETER_PRESENT */
#endif /* _ACCELEROMETER_H */
