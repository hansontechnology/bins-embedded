/******************************************************************************
 *  Copyright Cambridge Silicon Radio Limited 2012-2014
 *  Part of CSR uEnergy SDK 2.4.3
 *  Application version 2.4.3.0
 *
 *  FILE
 *      ht_hw.c
 *
 *  DESCRIPTION
 *      This file defines the Health Thermometer hardware specific routines.
 *
 *****************************************************************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <pio.h>
#include <pio_ctrlr.h>
#include <timer.h>
#include <aio.h>       
#include <reset.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/

#include "display_ui.h"
#include "ht_hw.h"
#include "health_thermometer.h"
#include "health_thermo_service.h"
#include "ht_gatt.h"
#include "app_gatt_db.h"
#include "app_gatt.h"
#include "user_config.h"
#include "activity_tracking_service.h"

#ifdef CONFIG_TARGET_BINSZ3
#include "button_ui.h"
#endif

/*============================================================================*
 *  Private Definitions
 *============================================================================*/

/* PIO direction */
#define PIO_DIRECTION_INPUT     (FALSE)
#define PIO_DIRECTION_OUTPUT    (TRUE)

/* PIO state */
#define PIO_STATE_HIGH          (TRUE)
#define PIO_STATE_LOW           (FALSE)


#ifndef CONFIG_TARGET_BINS

/* Setup PIOs
 *  PIO3    Buzzer
 *  PIO11   Button
 */

#define BUZZER_PIO              (15) //-- (3)
#define BUTTON_PIO              (28) //-- (11)

#define BUZZER_ON_PIO           (16) //--

#define PIO_BIT_MASK(pio)       (0x01UL << (pio))

#define BUZZER_PIO_MASK         (PIO_BIT_MASK(BUZZER_PIO))
#define BUTTON_PIO_MASK         (PIO_BIT_MASK(BUTTON_PIO))

/* Extra long button press timer */
#define EXTRA_LONG_BUTTON_PRESS_TIMER \
                                (4*SECOND)
#endif  // ! CONFIG_TARGET_BINS
                                        
                                        
#ifdef ENABLE_BUZZER

/* The index (0-3) of the PWM unit to be configured */
#define BUZZER_PWM_INDEX_0      (0)

/* PWM parameters for Buzzer */

/* Dull on. off and hold times */
#define DULL_BUZZ_ON_TIME       (2)    /* 60us */
#define DULL_BUZZ_OFF_TIME      (15)   /* 450us */
#define DULL_BUZZ_HOLD_TIME     (0)

/* Bright on, off and hold times */
#define BRIGHT_BUZZ_ON_TIME     (2)    /* 60us */
#define BRIGHT_BUZZ_OFF_TIME    (15)   /* 450us */
#define BRIGHT_BUZZ_HOLD_TIME   (0)    /* 0us */

#define BUZZ_RAMP_RATE          (0xFF)

/* TIMER values for Buzzer */
#define SHORT_BEEP_TIMER_VALUE  (100* MILLISECOND)
#define LONG_BEEP_TIMER_VALUE   (500* MILLISECOND)
#define BEEP_GAP_TIMER_VALUE    (25* MILLISECOND)

#endif /* ENABLE_BUZZER */

//-- 07.28                                        
uint32 rebootCheckTime;
uint8 rebootCheckStage;
#define REBOOT_CHECK_STAGE_IDLE     0
#define REBOOT_CHECK_STAGE_ONE      1
#define REBOOT_CHECK_STAGE_TWO      2
#define REBOOT_CHECK_STAGE_THREE    3
#define REBOOT_CHECK_STAGE_FOUR     4
                                        
                                        
                                        
/*============================================================================*
 *  Public data
 *============================================================================*/

/* Blood pressure application hardware data instance */
APP_HW_DATA_T                   g_app_hw_data;

/*============================================================================*
 *  Private Function Implementations
 *============================================================================*/

#ifdef ENABLE_BUZZER

/*----------------------------------------------------------------------------*
 *  NAME
 *      appBuzzerTimerHandler
 *
 *  DESCRIPTION
 *      This function is used to stop the Buzzer at the expiry of 
 *      timer.
 *
 *  RETURNS/MODIFIES
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
static void appBuzzerTimerHandler(timer_id tid)
{
    uint32 beep_timer = SHORT_BEEP_TIMER_VALUE;

    g_app_hw_data.buzzer_tid = TIMER_INVALID;

    switch(g_app_hw_data.beep_type)
    {
        case buzzer_beep_short: /* FALLTHROUGH */
        case buzzer_beep_long:
        {
            g_app_hw_data.beep_type = buzzer_beep_off;

            /* Disable buzzer */
            PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);
        }
        break;
        case buzzer_beep_twice:
        {
            if(g_app_hw_data.beep_count == 0)
            {
                /* First beep sounded. Start the silent gap*/
                g_app_hw_data.beep_count = 1;

                /* Disable buzzer */
                PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);

                /* Time gap between two beeps */
                beep_timer = BEEP_GAP_TIMER_VALUE;
            }
            else if(g_app_hw_data.beep_count == 1)
            {
                g_app_hw_data.beep_count = 2;

                /* Enable buzzer */
                PioEnablePWM(BUZZER_PWM_INDEX_0, TRUE);

                /* Start another short beep */
                beep_timer = SHORT_BEEP_TIMER_VALUE;
            }
            else
            {
                /* Two beeps have been sounded. Stop buzzer now*/
                g_app_hw_data.beep_count = 0;

                /* Disable buzzer */
                PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);

                g_app_hw_data.beep_type = buzzer_beep_off;
            }
        }
        break;
        case buzzer_beep_thrice:
        {
            if(g_app_hw_data.beep_count == 0 ||
               g_app_hw_data.beep_count == 2)
            {
                /* First beep sounded. Start the silent gap*/
                ++ g_app_hw_data.beep_count;

                /* Disable buzzer */
                PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);

                /* Time gap between two beeps */
                beep_timer = BEEP_GAP_TIMER_VALUE;
            }
            else if(g_app_hw_data.beep_count == 1 ||
                    g_app_hw_data.beep_count == 3)
            {
                ++ g_app_hw_data.beep_count;

                /* Enable buzzer */
                PioEnablePWM(BUZZER_PWM_INDEX_0, TRUE);

                beep_timer = SHORT_BEEP_TIMER_VALUE;
            }
            else
            {
                /* Two beeps have been sounded. Stop buzzer now*/
                g_app_hw_data.beep_count = 0;

                /* Disable buzzer */
                PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);

                g_app_hw_data.beep_type = buzzer_beep_off;
            }
        }
        break;

        default:
        {
            /* No such beep type */
            ReportPanic(app_panic_unexpected_beep_type);
            /* Though break statement will not be executed after panic but this
             * has been kept to avoid any confusion for default case.
             */
        }
        break;
    }

    if(g_app_hw_data.beep_type != buzzer_beep_off)
    {
        /* start the timer */
        g_app_hw_data.buzzer_tid = TimerCreate(beep_timer, TRUE, 
                                               appBuzzerTimerHandler);
    }
}

#endif /* ENABLE_BUZZER*/


/*============================================================================*
 *  Public Function Implementations
 *============================================================================*/


uint16 BatteryVoltageGet()
{
    uint16 mvs;

    // GPIO 1 for control
    // AIO 0 for ADC input
    // Lemuel 6/12 email updates a report for details
    
    // 07.15 design change to remove the MOSFET,
    // test shows it will limit the current through this, and as a result,
    // the voltage check becomes meaningless, the measurement shows it has unit
    // by unit different results too.    
    //-- PioSet(BATTERY_LEVEL_DETECT_PIO, TRUE); //-- Active High
    {
        aio_select port = AIO0;
        mvs = AioRead(port); /* Read the analogue voltage... */      
    }  
    //-- PioSet(BATTERY_LEVEL_DETECT_PIO, FALSE); //-- disable to save power
    
    return mvs;
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      HtInitHardware
 *
 *  DESCRIPTION
 *      This function is called to initialise Health Thermometer hardware
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/
#include "pedometer.h"
#include "battery_service.h"

uint16 gAio;
extern void HtInitHardware(void)
{

#ifndef CONFIG_TARGET_BINS    
    /* Setup PIOs
     * PIO3  - Buzzer
     * PIO11 - Button
     */
    
    PioSetModes(BUTTON_PIO_MASK, pio_mode_user);
    PioSetDir(BUTTON_PIO, PIO_DIRECTION_INPUT);
    PioSetPullModes(BUTTON_PIO_MASK, pio_mode_strong_pull_up);

    /* Setup button on PIO11 */
    PioSetEventMask(BUTTON_PIO_MASK, pio_event_mode_both);
#endif

#if 1
    PioSetMode(CHARGING_STATUS_PIO, pio_mode_user);
    PioSetDir(CHARGING_STATUS_PIO, FALSE); //-- input
    //-- PioSetModes(CHARGING_STATUS_PIO_MASK, pio_mode_strong_pull_down);
    //-- PioSetModes(CHARGING_STATUS_PIO_MASK, pio_mode_strong_pull_up);
    PioSetEventMask(CHARGING_STATUS_PIO_MASK, pio_event_mode_both);

#endif
    
#ifdef CONFIG_TARGET_BINSZ3
    // 12.18 for UI button setup
    PioSetMode(BUTTON_UI_PIO, pio_mode_user);
    PioSetDir(BUTTON_UI_PIO, PIO_DIRECTION_INPUT); //-- input 
    PioSetPullModes(BUTTON_UI_PIO_MASK, pio_mode_strong_pull_up);
    PioSetEventMask(BUTTON_UI_PIO_MASK, pio_event_mode_both);

#endif    
    
    /* Save power by changing the I2C pull mode to pull down */
    PioSetI2CPullMode(pio_i2c_pull_mode_strong_pull_down);

       
    /* Re-enable wake on g-sensor interrupt signal */
    SleepWakePinEnable(wakepin_mode_high_level); //--

#if 0  // 10.14
#ifndef CONFIG_TARGET_BINSX2    
    if(second_boot==49){
        uiAlertEnable(TRUE);            
    }
#else
    if(second_boot==49){
        gLocalClock=0x0404;
        gLocalHour=4;
        gLocalMinute=4;
    }    
#endif

#endif
    
#ifdef CONFIG_VIBRATION
    PioSetMode(VIBRATOR_PIO, pio_mode_user);
    PioSetDir(VIBRATOR_PIO, TRUE); //-- output
    PioSet(VIBRATOR_PIO, FALSE); //-- active High
#endif


    
#ifdef CONFIG_TARGET_BINSX2    
    
    PioSetMode(BATTERY_LEVEL_DETECT_PIO, pio_mode_user);
    PioSetDir(BATTERY_LEVEL_DETECT_PIO, TRUE); //-- output

#if 0 // for test
    //-- 2014.6.11 for battery test
    // GPIO 1 for control
    // AIO 0 for ADC input
    // Lemuel will update a report for details

    //-- PioSet(BATTERY_LEVEL_DETECT_PIO, TRUE); //-- the pin is Active High    
    {
         const aio_select port = AIO0;
         /* Read the analogue voltage... */
         
         //-- AioDrive(port,1000);	
         //-- AioOff(port);
         const uint16 mvs = AioRead(port);
         gAio=mvs;         
    }    
#endif
    //-- PioSet(BATTERY_LEVEL_DETECT_PIO, FALSE); //-- disable to save power, the pin is Active High

#endif

        
     rebootCheckStage=REBOOT_CHECK_STAGE_IDLE;   //-- 07.28
    
}

// 07.11
bool CheckPowerCharging()
{
    uint32 pios;
    bool isCharging;
    pios = PioGets();
    
    if((pios & CHARGING_STATUS_PIO_MASK)==0) isCharging=TRUE; //-- Active Low
    else isCharging=FALSE;
    
    //-- uiDisplayCharging(isCharging); 150620 no need to set display yet
    
    isUiDisplayCharging=isCharging; // 150620 set global value instead, for delayed reaction
    
    return isCharging;
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      EnablePairLED  -  Enable/Disable the Pair LED mapped to PWM0
 *
 *  DESCRIPTION
 *      This function is called to enable/disable the Pair LED controlled 
 *      through PWM0
 *
 *  RETURNS/MODIFIES
 *      Nothing.
*------------------------------------------------------------------------------*/
/* Use PWM0 to control the Pair LED */
#define PAIR_LED_PWM_INDEX            (0)

extern void newFlashLED(uint16 pio, int8 pwm_no)
{
    PioEnablePWM(pwm_no, FALSE);
    
    if(pwm_no==0)
        PioSetMode(pio, pio_mode_pwm0);               
    else if(pwm_no==1)
        PioSetMode(pio, pio_mode_pwm1);
    else 
        PioSetMode(pio, pio_mode_pwm2);
#if 0
    
    PioConfigPWM(pwm_no, pio_pwm_mode_push_pull, 
            0, 15,5,
            15,0,5,
            4);
#endif  

#if 1
    
    PioConfigPWM(pwm_no, pio_pwm_mode_push_pull, 
            30, 100,30,
            100,30,30,
            150);
    
#endif  
    
    
    /* Enable the PWM */
    PioEnablePWM(pwm_no, TRUE);

}


void EnableLED(uint16 pio, int8 pwm_no, bool enable)
{
    if(enable)
    {

#if 0   //-- original with reversed on/off
        /* Configure the PWM0 parameters */
        PioConfigPWM(PAIR_LED_PWM_INDEX, pio_pwm_mode_push_pull,
                     PAIR_LED_DULL_ON_TIME, PAIR_LED_DULL_OFF_TIME,
                     PAIR_LED_DULL_HOLD_TIME, PAIR_LED_BRIGHT_OFF_TIME,
                     PAIR_LED_BRIGHT_ON_TIME, PAIR_LED_BRIGHT_HOLD_TIME,
                     PAIR_LED_RAMP_RATE);
#endif

#if 0        
        /* Configure the PWM0 parameters */
        PioConfigPWM(PAIR_LED_PWM_INDEX, pio_pwm_mode_push_pull,
                     PAIR_LED_DULL_OFF_TIME, PAIR_LED_DULL_ON_TIME,
                     PAIR_LED_DULL_HOLD_TIME, PAIR_LED_BRIGHT_ON_TIME,
                     PAIR_LED_BRIGHT_OFF_TIME, PAIR_LED_BRIGHT_HOLD_TIME,
                     PAIR_LED_RAMP_RATE);
#endif

        if(pwm_no>=0) //-- means no use of pwm, strait on
        {
            PioEnablePWM(pwm_no, FALSE);
            if(pwm_no==0)
                PioSetMode(pio, pio_mode_pwm0);               
            else if(pwm_no==1)
                PioSetMode(pio, pio_mode_pwm1);
            else 
                PioSetMode(pio, pio_mode_pwm2);
                
#if 0       //-- 2013.11.20, smooth transition         
            PioConfigPWM(pwm_no, pio_pwm_mode_push_pull, 
            10, 100, 124,
            10, 1,10,
            100);
#endif        

        
#if 0       //-- 2013.11.20, smooth transition         
            //-- PioConfigPWM(pwm_no, pio_pwm_mode_inverted_push_pull, 
            PioConfigPWM(pwm_no, pio_pwm_mode_push_pull, 
            0, 0,80,
            0,40,40,
            80);
#endif  
#if 0       //-- 2013.11.20, smooth transition         
            //-- PioConfigPWM(pwm_no, pio_pwm_mode_inverted_push_pull, 
            PioConfigPWM(pwm_no, pio_pwm_mode_push_pull, 
            0, 0,31,
            0,100,31,
            200);
#endif  
#if 1       //-- 2013.11.20, smooth transition         
            //-- PioConfigPWM(pwm_no, pio_pwm_mode_inverted_push_pull, 
            PioConfigPWM(pwm_no, pio_pwm_mode_push_pull, 
            0, 50,15,
            50,0,15,
            100);
#endif  

            /* Enable the PWM */
            PioEnablePWM(pwm_no, TRUE);
        }
        else
        {
            PioSetMode(pio, pio_mode_user);
            PioSetDir(pio, PIO_DIRECTION_OUTPUT);
            PioSet(pio, TRUE);
        }

    }
    else
    {
        /* Disable the PWM */
        if(pwm_no>=0 && pwm_no<=3) PioEnablePWM(pwm_no, FALSE);
        /* Reconfigure PAIR_LED_PIO to pio_mode_user. This reconfiguration has
         * been done because when PWM is disabled, PAIR_LED_PIO value may remain
         * the same as it was, at the exact time of disabling. So if
         * PAIR_LED_PIO was on, it may remain ON even after disabling PWM. So,
         * it is better to reconfigure it to user mode. It will get reconfigured
         * to PWM mode when we re-enable the LED.
         */
        PioSetMode(pio, pio_mode_user);
        PioSetDir(pio, PIO_DIRECTION_OUTPUT);
        PioSet(pio, FALSE);        
    }

}


/*----------------------------------------------------------------------------*
 *  NAME
 *      HtHwDataInit
 *
 *  DESCRIPTION
 *      This function initialises Health Thermometer hardware data structure
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

extern void HtHwDataInit(void)
{

    /* Delete button press timer */
    TimerDelete(g_app_hw_data.button_press_tid);
    g_app_hw_data.button_press_tid = TIMER_INVALID;

}

#ifndef CONFIG_TARGET_BINS
/*----------------------------------------------------------------------------*
 *  NAME
 *      SoundBuzzer
 *
 *  DESCRIPTION
 *      This function is called to trigger beeps of different types 
 *      'buzzer_beep_type'.
 *
 *  RETURNS/MODIFIES
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

extern void SoundBuzzer(buzzer_beep_type beep_type)
{
#ifdef ENABLE_BUZZER
    uint32 beep_timer = SHORT_BEEP_TIMER_VALUE;

    PioEnablePWM(BUZZER_PWM_INDEX_0, FALSE);
    PioSet(BUZZER_ON_PIO,FALSE); //--

    TimerDelete(g_app_hw_data.buzzer_tid);
    g_app_hw_data.buzzer_tid = TIMER_INVALID;

    g_app_hw_data.beep_count = 0;

    /* Store the beep type in some global variable. It will used on timer 
     * expiry to check the type of beeps being sounded.
     */
    g_app_hw_data.beep_type = beep_type;

    switch(g_app_hw_data.beep_type)
    {
        case buzzer_beep_off:
        {
            /* Don't do anything */
        }
        break;

        case buzzer_beep_short: /* One short beep will be sounded */
            /* FALLTHROUGH */
        case buzzer_beep_twice: /* Two short beeps will be sounded */
            /* FALLTHROUGH */
        case buzzer_beep_thrice: /* Three short beeps will be sounded */
        {
            beep_timer = SHORT_BEEP_TIMER_VALUE;
        }
        break;

        case buzzer_beep_long:
        {
            /* One long beep will be sounded */
            beep_timer = LONG_BEEP_TIMER_VALUE;
        }
        break;

        default:
        {
            /* No such beep type defined */
            ReportPanic(app_panic_unexpected_beep_type);
            /* Though break statement will not be executed after panic but this
             * has been kept to avoid any confusion for default case.
             */
        }
        break;
    }

    if(g_app_hw_data.beep_type != buzzer_beep_off)
    {
        /* Initialize beep count to zero */
        g_app_hw_data.beep_count = 0;

        /* Enable buzzer */
        PioEnablePWM(BUZZER_PWM_INDEX_0, TRUE);
        PioSet(BUZZER_ON_PIO,TRUE);

        TimerDelete(g_app_hw_data.buzzer_tid);
        g_app_hw_data.buzzer_tid = TimerCreate(beep_timer, TRUE, 
                                               appBuzzerTimerHandler);
    }
#endif /* ENABLE_BUZZER */
}

#endif

/*----------------------------------------------------------------------------*
 *  NAME
 *      HandlePIOChangedEvent
 *
 *  DESCRIPTION
 *      This function handles PIO Changed event
 *
 *  RETURNS
 *      Nothing.
 *
 *---------------------------------------------------------------------------*/

#include <time.h>
#include "display.h"
#include "touch.h"

uint32 tapThisTime, tapLastTime;    //--09.15
bool hadDoubleTapLastTime;

extern void HandlePIOChangedEvent(uint32 pio_changed)
{
    
    if(pio_changed & CHARGING_STATUS_PIO_MASK)
    {
        /* PIO changed */
        uint32 pios;
        uint32 currentTime;
                
        currentTime=TimeGet32();

        pios = PioGets();

        if(pios & CHARGING_STATUS_PIO_MASK)  //-- means release from charging
        {
#ifdef CONFIG_PUSH_ADVERTISING
            advertisingDisable(); 
            // 150716 - when release from charging, to stop advertising
#endif            
            
#ifndef CONFIG_TARGET_BINSZ3    //-- 150615 for Z3 using buttong long press instead
           
           if(currentTime-rebootCheckTime<(2*SECOND)){
                if(rebootCheckStage==REBOOT_CHECK_STAGE_ONE){
                    rebootCheckStage=REBOOT_CHECK_STAGE_TWO;  
                }
                else if(rebootCheckStage==REBOOT_CHECK_STAGE_THREE){
                        systemResetAndRecovery();
                }
                rebootCheckTime=currentTime;                
            }            
            else{
               rebootCheckStage=REBOOT_CHECK_STAGE_IDLE;    
            }
#endif  //-- END of NOT CONFIG_TARGET_BINSZ3
            
#ifndef CONFIG_TARGET_BINSX2 
            uiChargingEnable(FALSE);
#else
            uiDisplayCharging(FALSE);
#endif
            CheckBatteryLevel(TRUE);
            //-- uiDisplayStart();   //-- update battery status by this chance too.
        }
        else //-- active low
        {
#ifdef CONFIG_PUSH_ADVERTISING
            advertisingEnable(); 
            // 150716 - when in charging, to start advertising continuously
#endif
            
            rebootCheckTime=currentTime;
            if(rebootCheckStage==REBOOT_CHECK_STAGE_IDLE){
                rebootCheckStage=REBOOT_CHECK_STAGE_ONE;   
            }
            else{
                rebootCheckStage=REBOOT_CHECK_STAGE_THREE;
            }
#ifndef CONFIG_TARGET_BINSX2 
            uiChargingEnable(TRUE);
#else
            uiDisplayCharging(TRUE);
#endif
        }   // end of active low 
        
    }   // end of if pio_changed true
    
    
#ifndef CONFIG_TARGET_BINS
    if(pio_changed & BUTTON_PIO_MASK)
    {
        /* PIO changed */
        uint32 pios;

        TimeGet48WithOffset(gSystemTime,0);
        gSystemTime32=TimeGet32();
        
                
        pios = PioGets();
               
        if(!(pios & BUTTON_PIO_MASK))
        {
            /* This event comes when a button is pressed */

            /* Start a timer for LONG_BUTTON_PRESS_TIMER seconds. If timer expi-
             * res before we receive a button release event then it was a long -
             * press and if we receive a button release pio changed event, it -
             * means it was a short press.
             */
            TimerDelete(g_app_hw_data.button_press_tid);

            g_app_hw_data.button_press_tid = TimerCreate(
                                           EXTRA_LONG_BUTTON_PRESS_TIMER,
                                           TRUE,
                                           HandleExtraLongButtonPress);
        }
        else
        {
            /* This event comes when a button is released */
            if(g_app_hw_data.button_press_tid != TIMER_INVALID)
            {
                /* Timer was already running. This means it was a short button 
                 * press.
                 */
                TimerDelete(g_app_hw_data.button_press_tid);
                g_app_hw_data.button_press_tid = TIMER_INVALID;

                HandleShortButtonPress();
            }
        }
    }
#endif //-- ! CONFIG_TARGET_BINS
 
#ifdef CONFIG_TARGET_BINSZ3
    if(pio_changed & BUTTON_UI_PIO_MASK)
    {
        
        /* PIO changed */
        uint32 pios;
        
        pios = PioGets();       
        //-- Active Low
        
        if((pios & BUTTON_UI_PIO_MASK)==0) //-- press button, active low
        {
            /*
            uiDisplayShow(FALSE);
            } else if((pios & BUTTON_UI_PIO_MASK)!=0){
            }
            */
            
            clockAlarmStop();   //-- 150718 
            
            //-- no need of this, touchWakeup();
            buttonNewSession();            
        }
        else{  // release button
            bool wasFastSwitch=buttonFastSwitch();

            buttonSessionEnd();
            
            tapThisTime=TimeGet32();
            if(wasFastSwitch==TRUE){

                if(hadDoubleTapLastTime==FALSE && (tapThisTime-tapLastTime)<(650*1000L)){ //-- 1 second
#ifndef CONFIG_BUTTON_ONE_CLICK_SWITCH 
                    uiDisplayShowDelayed(0);
                    uiDisplayStart();   // switch mode, one step move forward
#else
                    uiDisplayShowDelayed(0);
                    uiDisplayShowAndDone();     // show and execute action right away                   
#endif
                    hadDoubleTapLastTime=TRUE;
                }
                else{
#ifndef CONFIG_BUTTON_ONE_CLICK_SWITCH   
                    //-- uiDisplayShow(FALSE);
                    uiDisplayShowDelayed(650);  // delay 0.5 seconds, and display the UI
#else
                    uiDisplayShowDelayed(0);      // stop previous scheduled display
                    uiDisplayStartDelayed(650);   // delayed execution, switch mode, one step move forward 
#endif                    
                    if(uiDisplayIsOn()==TRUE){
                        hadDoubleTapLastTime=FALSE; // wait further
                    } else {
                        hadDoubleTapLastTime=TRUE;  // bypass the double click detection
                    }
                }
            }
            else{
                tapLastTime=tapThisTime=0;
                uiDisplayShow(FALSE);          
                hadDoubleTapLastTime=FALSE;
            }
            tapLastTime=tapThisTime; 
         }    
    }
#endif
    
#ifdef CONFIG_TARGET_BINSX2
    if(pio_changed & TOUCH_INT_PIO_MASK)
    {
        /* PIO changed */
        uint32 pios;

#ifndef CONFIG_VIBRATION        
        if(touchAlertIsRunning()==TRUE) return;  //-- 09.22 ignore if when alert ON
#endif
        
        pios = PioGets();       
        //-- Active High
        
        if((pios & TOUCH_INT_PIO_MASK)==0) //-- release from touch
        {            
#ifdef CONFIG_TARGET_BINSZ3    // 12.22  
            touchSessionEnd(TOUCH_WAKE_PERIOD);
#else           
            bool wasFastSwitch=touchFastSwitch();
            
            touchSessionEnd(TOUCH_WAKE_PERIOD);

          
            tapThisTime=TimeGet32();
            if(wasFastSwitch==TRUE){

                if(hadDoubleTapLastTime==FALSE && (tapThisTime-tapLastTime)<(500*1000L)){ //-- 1 second
                    uiDisplayShowDelayed(0);
                    uiDisplayStart();
                    hadDoubleTapLastTime=TRUE;
                }
                else{
                    //-- uiDisplayShow(FALSE);
                    uiDisplayShowDelayed(500);
                    hadDoubleTapLastTime=FALSE;
                }
            }
            else{
                tapLastTime=tapThisTime=0;
                uiDisplayShow(FALSE);          
                hadDoubleTapLastTime=FALSE;
            }
            tapLastTime=tapThisTime; 
#endif
            
        }
        else{ // Active High Case
            
            //-- no need of this, touchWakeup();
            touchNewSession();
            
        }    
    }
#endif
    
}


