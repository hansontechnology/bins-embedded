
#include <time.h>
#include <gatt.h>
   
 
#include "display.h"
#include "configuration.h"
#include "ht_hw.h"
#include "pedometer.h"
#include "activity_tracking_service.h"
#include "health_thermometer.h"
#include "motion.h"
#include "i2c_comms.h"
#include "touch.h"
#include "battery_service.h"
#include "lightsensor.h"

/* ----------------- globals ----------------------------------- */

timer_id  uiDisplayTimeoutTimer_tid;
int8 gUiDisplayColumn;
int8 uiDisplayMode;

bool gHasAlertEvent;
bool gHasMessageEvent;

#ifdef CONFIG_VIBRATION  // 12.22
static timer_id vibrationAlertTimer_tid;
#endif

static bool isDisplayTimeout;
bool isUiDisplayCharging;

/*
uint8 binsFontDigitIndex[10]={
    6,7,8,9,10,11,12,13,14,15
};
*/

#define UI_DISP_COL_POS_BATTERY_OUT_TIME 59
#define UI_DISP_COL_POS_BATTERY_OUT 0   //-- 20150527 from 4 to 0
#define UI_DISP_COL_POS_HEARTRATE 15   //-- 40
#define UI_DISP_COL_POS_HEARTRATE_LABEL (UI_DISP_COL_POS_HEARTRATE+64+10) 
#define UI_DISP_COL_POS_ACTIVITY 25
#define UI_DISP_COL_POS_STATUS 0    //-- (127-11)
#define UI_DISP_COL_GAP   4 //-- 09.18 2

#define UI_DISPLAY_ALERT_TIMEOUT_SECOND     10
#define UI_DISPLAY_TIMEOUT_SECOND           7

/* ------------------ main functions --------------------------- */

#ifdef CONFIG_VIBRATION
static void vibrationAlert(bool onoff); 
#endif
 

static void uiDisplayHeartRateRequest(void);
static void uiDisplayHeartRateRequestRunning(void);
static void uiDisplayActionDone(void);
static timer_id uiDisplayIdTimer_tid;   //-- 150712
//-- static void uiDisplayHeartRateProcessingClear(void);

//-- #include "display_font.c"
   
#include "_bins.c"

#if 1
 

void displayCharRaw(uint8 font_index)
{
    if(gUiDisplayColumn>=UI_DISP_COL_GAP) gUiDisplayColumn-=UI_DISP_COL_GAP;
    displayChar(font_index);
    gUiDisplayColumn-=UI_DISP_COL_GAP;
}
    
void displayChar(uint8 font_index)
{
    uint8 charWidth;
    uint8 i;
    const uint8 *charData;
    uchar page_number,column_number;
   
    i2cUsePeripheralBus();
    
    charWidth=font_bins_CharInfo[font_index].Width;
    charData=font_bins_CharInfo[font_index].pData8;
        
    gUiDisplayColumn+=UI_DISP_COL_GAP;
   
    i=0;
    for(page_number=0;page_number<4;page_number++)
    {
        displayWriteCommand(START_PAGE+page_number);
        displayWriteCommand(START_HIGH_BIT| ((gUiDisplayColumn & 0xF0)>>4));
        displayWriteCommand(START_LOW_BIT | (gUiDisplayColumn & 0x0F));
        for(column_number=gUiDisplayColumn;
                column_number<gUiDisplayColumn+charWidth;column_number++)
        {
            displayWriteData(charData[i]);
            i++;
        }
   }
   
   gUiDisplayColumn+=charWidth+UI_DISP_COL_GAP;
   
   i2cUseMainBus();

}
#endif

static bool gIsDisplayScrolling;
        
static void uiDisplayScroll(bool onoff)
{
    if(gIsDisplayScrolling==onoff) return;  //-- 150620
    gIsDisplayScrolling=onoff;

    i2cUsePeripheralBus();
    if(onoff==TRUE){                
        displayWriteCommand(0xAF);     //Set Display ON 
        displayWriteCommand(0x2E);      //Set Scroll Off         
        displayWriteCommand(0x29);      // Vertical and Right horizental scroll
        displayWriteCommand(0x00);      // Dummy
        displayWriteCommand(0x00);      // Page start
        displayWriteCommand(0x00);      // Time Interval
        displayWriteCommand(0x03);      // Page end
        displayWriteCommand(0x00);      // Verticalscrolling as 0 row 
       
        displayWriteCommand(0x2F);      //Set Scroll On
 
    }
    else{
        displayWriteCommand(0x2E);      //Set Scroll Off 
        displayWriteCommand(0xAF);     //Set Display Off  
    }
    i2cUseMainBus();
    
}

static bool hasDisplayReset;    //--09.20, purpose to tell if had other display page after alerts

static void uiDisplayClearAndStart(void)
{
    i2cUsePeripheralBus();
    
#ifdef CONFIG_TARGET_BINSZ3_HBP
    {
        uint8 displayContrastSet;
        
        if((gConfiguration & BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK)!=0){ // set to strong 
            displayContrastSet=0xC0;  // range from 00 to FF
        } else {           
            //-- AmbientLightStatus currentLightCondition;

            displayContrastSet=0x20;
/* 
            if(lightSensorReadDirect()>100){
                displayContrastSet=0xC0;
            }else{
                displayContrastSet=0x20;
            }
*/
/* 150603 -- not support yet            
            currentLightCondition=lightSensorAmbientLevel();
            if(currentLightCondition==AMBIENT_LIGHT_BRIGHT){
                displayContrastSet=0x99;
            }else if(currentLightCondition==AMBIENT_LIGHT_NORMAL){
                displayContrastSet=0x40;            
            } else {  // AMBIENT_LIGHT_WEAK
                displayContrastSet=0x20;
            }
*/
        }
        displayWriteCommand(0x81);     //Contrast control
        displayWriteCommand(displayContrastSet);
    }
#else

#ifdef CONFIG_TARGET_Z3    
    displayWriteCommand(0x81);     //Contrast control
    displayWriteCommand(((gConfiguration & BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK)!=0) ? 0x90 : 0x40 ); 
    
#else
    displayWriteCommand(0x81);     //Contrast control
    displayWriteCommand(((gConfiguration & BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK)!=0) ? 0x80 : 0x20 ); // 10.09       
#endif
    
#endif // HBP    
    
    displayWriteCommand(0x2E);      //Set Scroll Off  150619
    gIsDisplayScrolling=FALSE;
    
#ifdef CONFIG_TARGET_BINSZ3    
    displayVcc(TRUE);              //20150126
#endif
    Display_Clear_Screen();
    displayWriteCommand(0xAF);     //Set Display ON   
    

    //-- 150619 uiDisplayScroll(FALSE);

    i2cUseMainBus();
    
    gUiDisplayColumn=0; // 24 reset the column;
    
    hasDisplayReset=TRUE;
}

void uiDisplayTimeoutTimerHandler(timer_id tid);

void uiDisplayTimeoutTimerHandler(timer_id tid)
{       
    if(uiDisplayTimeoutTimer_tid!=tid) return;
    
    uiDisplayTimeoutTimer_tid=TIMER_INVALID;
                     
    //-- no need to reset, keep the mode, uiDisplayMode=UI_DISPLAY_MODE_OFF;    // reset to 0
    //-- no need to replay the alert or message display, to save power
        
    i2cUsePeripheralBus();

    //-- Display_Clear_Screen();
    displayWriteCommand(0xAE);     //Set Display OFF 
#ifdef CONFIG_TARGET_BINSZ3
    displayVcc(FALSE);             //20150126
#endif    
    i2cUseMainBus();
    
    isDisplayTimeout=TRUE;
    
    if(isUiDisplayCharging==TRUE) uiDisplayCharging(TRUE);
}

void uiDisplayNumber(uint32 displayNumber, int8 length, bool isShrink)
{
    uint32 dispNum;
    int i;
    uint32 tenTimes;
    uint8 dispDigit[8];
    bool hadNonzeroNumber;
    uint8 paddingFactor=0;
     
    if(length>6) length=6;
    
    if(displayNumber>999999){ 
         displayNumber=displayNumber%1000000L;    //-- 09.18
    }
    
    dispNum=displayNumber;
    
    // if(dispNum>=10000) paddingFactor=2;
    
    if(length>=5) paddingFactor=2;
    else paddingFactor=0;
    
    for(i=6, tenTimes=100000; i>0; tenTimes=tenTimes/10,i--)
    {
        if(dispNum>=tenTimes){ 
            if(tenTimes>0){
                dispDigit[i-1]=dispNum/tenTimes; 
                dispNum=dispNum%tenTimes;
            }
            else{
                dispDigit[i-1]=dispNum;  
            }
        }
        else{
            dispDigit[i-1]=0;
        }    
    }
    
    hadNonzeroNumber=FALSE;
    for(i=length; i>0; i--)
    {           
        gUiDisplayColumn-=paddingFactor;
        
        if(isShrink==TRUE && i>1 && dispDigit[i-1]==0 && hadNonzeroNumber==FALSE){
            displayChar(DISPLAY_FONT_INDEX_SPACE);  
        }           
        else{
            hadNonzeroNumber=TRUE;
            //-- displayChar(binsFontDigitIndex[dispDigit[i-1]]); 
            displayChar(dispDigit[i-1]+DISPLAY_UI_CHAR_INDEX_NUMBER_ZERO);
        }
        gUiDisplayColumn-=paddingFactor;            
    }        
}

#if 0
static void displayAlphabet(uchar letter)
{  
    int8 index;
    
    index=(letter-0x41);
    index=index+DISPLAY_FONT_INDEX_LETTER_A;
    displayChar(index);                     
}
#endif 

static void uiDisplayDown(void)
{
    if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){
        TimerDelete(uiDisplayTimeoutTimer_tid);        
        uiDisplayTimeoutTimer_tid=TIMER_INVALID;
    }
       
    i2cUsePeripheralBus();

    //-- Display_Clear_Screen();
    displayWriteCommand(0xAE);     //Set Display OFF 
#ifdef CONFIG_TARGET_Z3
    displayVcc(FALSE);             //20150126
#endif
    i2cUseMainBus();
}

void uiDisplayTime(void)
{    
    uint16 clockHour, clockMinute;

    
    uiDisplayClearAndStart();

#if 0 //-- only for test
    uint16 lightValue;
     
    lightValue=lightSensorRead();    
    uiDisplayNumber(lightValue, 7, FALSE);
    return;        
#endif    
    
    clockGet(&clockHour, &clockMinute);        
   
    //-- 09.18 displayChar(DISPLAY_FONT_INDEX_CLOCK);
    gUiDisplayColumn+= 5;

#if 0 // test
    {
        uint16 voltage;

        voltage=BatteryVoltageGet();
        uiDisplayNumber(voltage, 4, TRUE);
    }
#endif
    
#if 0 // test 150501
    {
        uint16 testV=g_ht_data.state;
        // if(cyclicAdvertTimerId!=TIMER_INVALID) testV=testV+100;
        // else testV=testV+200;
        testV=cyclicTestCounter*10+testV;    
        uiDisplayNumber(testV, 4, TRUE);
    }
#else
    uiDisplayNumber(clockHour,2, TRUE);
    gUiDisplayColumn+= 3;
    displayChar(DISPLAY_FONT_INDEX_COLON);
    gUiDisplayColumn+= 3;    
    uiDisplayNumber(clockMinute,2, FALSE);    
#endif       

}

void uiDisplayTimeUpdate(void)
{
    if(uiDisplayMode==UI_DISPLAY_MODE_TIME && isDisplayTimeout==FALSE){
        uiDisplayTime();
    }
}

static bool uiDisplayNewCycle(uint8 timeoutSecond)
{
    bool isNewlyOn=TRUE;
                     
    if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){
        TimerDelete(uiDisplayTimeoutTimer_tid);
        isNewlyOn=FALSE;
    }
    uiDisplayTimeoutTimer_tid=TimerCreate((timeoutSecond*SECOND), TRUE, uiDisplayTimeoutTimerHandler);

    if(uiDisplayTimeoutTimer_tid==TIMER_INVALID) //-- 09.22 prevent deadlock
        isDisplayTimeout=TRUE;
    else                    
        isDisplayTimeout=FALSE;
    
    return isNewlyOn;
}

uint16 dayActivityNumber;

void uiDisplayActivityUpdate(uint8 steps)
{
    if(uiDisplayMode!=UI_DISPLAY_MODE_ACTIVITY) return;

    if(isDisplayTimeout==TRUE) return;
    
    dayActivityNumber+= steps;  

    gUiDisplayColumn=UI_DISP_COL_POS_ACTIVITY;
    uiDisplayNumber(dayActivityNumber, 5, TRUE);
    
}
void uiDisplayGoalAchieved()
{
    uiDisplayMode=UI_DISPLAY_MODE_ACTIVITY;
    uiVibrationStart(500);
    uiDisplayShow(TRUE);        
}

static void uiDisplayActivityShow(int steps)
{
    uiDisplayClearAndStart();        
    displayChar(DISPLAY_FONT_INDEX_STEP);
    gUiDisplayColumn=UI_DISP_COL_POS_ACTIVITY;    
    uiDisplayNumber(steps, 5, TRUE);   
}
static void uiDisplayActivity(void)
{
    dayActivityNumber=activityTheDay();  
    uiDisplayActivityShow(dayActivityNumber);
}

static void uiDisplayBoard(void)
{   
    if(gHasAlertEvent==TRUE || gHasMessageEvent==TRUE){  
#ifdef CONFIG_VIBRATION
      vibrationAlert(FALSE);
#else
      touchAlert(FALSE);
#endif
    }
    uiDisplayClearAndStart();
        
    if(gHasAlertEvent==TRUE || gHasMessageEvent==TRUE)
    {
        // 0 - 15 for connect or battery status
        gUiDisplayColumn=16;
                          
        if(gHasAlertEvent==TRUE)    // higher priority to show
        {
            displayCharRaw(DISPLAY_FONT_INDEX_ALERT);
            gHasAlertEvent=FALSE;       
            gUiDisplayColumn+= UI_DISP_COL_GAP;
            
        } 
        if(gHasMessageEvent==TRUE) 
        {
            displayCharRaw(DISPLAY_FONT_INDEX_MESSAGE);
            gHasMessageEvent=FALSE;
            //-- gUiDisplayColumn+= UI_DISP_COL_GAP;
        }        
        if(gUiDisplayColumn<55) gUiDisplayColumn=55;    // hardwired to proper position for following             
    }
    else{
        gUiDisplayColumn=45; // shift for next content 150505  
    }

#if 0 //-- 150712 #ifdef CONFIG_IBEACON    
    {
        // total 15+10+14+4 width=43
        // 128-43=85
        DeviceEventStatus *eStatus=atGetDeviceEvent();
        
        displayChar(DISPLAY_FONT_INDEX_ARROWTO);
    
        if(g_ht_data.state==app_state_connected || eStatus->connectRequest==TRUE)
        {
          displayCharRaw(DISPLAY_FONT_INDEX_DISCONNECTED);
        }else {
          displayCharRaw(DISPLAY_FONT_INDEX_CONNECTED);    
        }    
        displayChar(DISPLAY_FONT_INDEX_QUESTION);   
    }
#endif
    
    //-- 150712
    {
        uint8 charBatteryIndex;
       
        uint8 batteryLevel=readBatteryLevel();

        if(batteryLevel>50){
            charBatteryIndex=DISPLAY_FONT_INDEX_BATTERY_FULL;
        }else if(batteryLevel>20){
            charBatteryIndex=DISPLAY_FONT_INDEX_BATTERY_HALF;
        }else
        {
            charBatteryIndex=DISPLAY_FONT_INDEX_BATTERY_OUT;            
        }
        displayChar(charBatteryIndex);                
    }

    if(g_ht_data.state!=app_state_connected)
    {
        displayChar(DISPLAY_FONT_INDEX_DISCONNECTED);
    }else{
        displayChar(DISPLAY_FONT_INDEX_CONNECTED);    
    }        
    
    if(gConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK){  //-- check if sleep is enabled
        if(gUiDisplayColumn<(128-20)){
            displayChar(DISPLAY_FONT_INDEX_SLEEP_RUNNING);
        }
    }
}

static void uiDisplayShowConnectStatus(void)
{
    if(gIsDisplayScrolling==TRUE) return;
    
    gUiDisplayColumn=UI_DISP_COL_POS_STATUS;
    if(g_ht_data.state==app_state_connected){
        displayChar(DISPLAY_FONT_INDEX_CONNECTED);      
    }
    else{
        displayChar(DISPLAY_FONT_INDEX_DISCONNECTED);            
    }
}

void uiDisplayChangeState(void)
{
    if(g_ht_data.state==app_state_connected){
        DeviceEventStatus *eStatus=atGetDeviceEvent();
        if(eStatus->heartRateRequest==TRUE){
            uiDisplayMode=UI_DISPLAY_MODE_HRD; // now set to heart rate display mode            
            uiDisplayHeartRateRequest();    // reset back to false to stop request
            touchHeartRateBatchCycle(TRUE);     // start touch batch, disconnect after batch cycle
        }
        // TODO 20150721 worth to alert user by turning on the display
        
    }
    if(gIsDisplayScrolling==TRUE){
        return;   // if in scrolling, no need to update the change
    }
    
    if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){   // means the display is ON
        if(uiDisplayMode==UI_DISPLAY_MODE_HRD){
            uiDisplayShowConnectStatus();
        }      
        if(uiDisplayMode==UI_DISPLAY_MODE_OFF){
            uiDisplayBoard();               
        }
    }
}    

#if 0 //-- 2015.01.31
static bool isUiDisplayHeartRateReadingShow;

void uiDisplayHeartRateReading(void)
{   
    if(uiDisplayMode==UI_DISPLAY_MODE_HRD && isDisplayTimeout==FALSE){
        
        gUiDisplayColumn=UI_DISP_COL_POS_STATUS;
        if(isUiDisplayHeartRateReadingShow==FALSE){
           displayChar(DISPLAY_FONT_INDEX_CONNECTED);
        }
        else{
           displayChar(DISPLAY_FONT_INDEX_SPACE_NARROW);            
        }
        
        if(isUiDisplayHeartRateReadingShow==TRUE)
           isUiDisplayHeartRateReadingShow=FALSE;
        else
           isUiDisplayHeartRateReadingShow=TRUE;            
    }        
}
#endif

void uiDisplayConnectStatusUpdate(void)
{   
    if(uiDisplayMode==UI_DISPLAY_MODE_HRD && isUiDisplayCharging==FALSE){        
        uiDisplayShowConnectStatus();        
    }
}

static void clearHeartRateNumberArea(void);

void uiDisplayHeartRateNotice(bool isUpdateAnyway)
{   
    if(uiDisplayMode==UI_DISPLAY_MODE_HRD && isUiDisplayCharging==FALSE){
        uint8 currentHR;
        
        currentHR=getLatestHeartRate();
        if(currentHR>0)
        {
            //-- uiDisplayHeartRateProcessingClear();            
            gUiDisplayColumn=UI_DISP_COL_POS_HEARTRATE;    // constant of shift from logo
            uiDisplayNumber(currentHR, 3, TRUE);
            
        } else {  //-- 150603
            if(isUpdateAnyway==TRUE && touchHeartRatePollRunning()==TRUE){
                uiDisplayHeartRateProcessing(TRUE); 
            }
        }
        /*
        else{
            //-- 2015.01.31
            int i=0;
            gUiDisplayColumn=UI_DISP_COL_POS_HEARTRATE;    // constant of shift from logo
            for(i=0; i<3; i++){
                displayChar(DISPLAY_FONT_INDEX_SPACE);
            }
        }
        */
        //-- 150603 reduce overhead, uiDisplayShowConnectStatus();        
    }
        
}

#if 0 //-- 10.10
static void uiDisplayShakeSetting(bool enterNew)
{
    int8 i;

    if(enterNew==FALSE){
        shakeThresholdChange(1);
    }
        
    if(enterNew==TRUE || gShakeThreshold==1){
        uiDisplayClearAndStart();
        displayChar(DISPLAY_FONT_INDEX_SHAKE);

        for(i=0;i<gShakeThreshold; i++){
            displayChar(DISPLAY_FONT_INDEX_LEVEL);
        }
    }
    else{
        //-- uint8 width=fontWidth(DISPLAY_FONT_INDEX_LEVEL);
        //-- gUiDisplayColumn+=(width+UI_DISP_COL_GAP)*(gShakeThreshold-1);
        displayChar(DISPLAY_FONT_INDEX_LEVEL);
    }    
}
#endif

static void clearHeartRateNumberArea()
{    
    // clear pulse number if already have
    gUiDisplayColumn=UI_DISP_COL_POS_HEARTRATE;    // constant of shift from logo
    int i;
    for(i=0;i<3; i++){
       displayChar(DISPLAY_FONT_INDEX_SPACE);
    }           
}

/* 150504
    It is called by touch polling time (per second) of pulse detection as well, 
    if no pulse detected, with isStart set to FALSE.
    If want to detect the case of connecting, and showing this, then, 
*/
#if 0
uint8 gIsDisplayHeartRateProcessingJustFinished;

static void uiDisplayHeartRateProcessingClear(void)
{
    if(gIsDisplayHeartRateProcessingJustFinished==FALSE) return;

    clearHeartRateNumberArea();
}
#endif

void uiDisplayHeartRateProcessing(bool isStart)
{    
    if(isStart==FALSE) return;
    
    if(!(uiDisplayMode==UI_DISPLAY_MODE_HRD && isDisplayTimeout==FALSE)){
        return; // no need to display in this case
    }
    
    clearHeartRateNumberArea();
    
    gUiDisplayColumn=UI_DISP_COL_POS_HEARTRATE;
    
    gUiDisplayColumn-=2; // padding factor
    gUiDisplayColumn+=UI_DISP_COL_GAP;
    gUiDisplayColumn+=16; //-- displayChar(DISPLAY_FONT_INDEX_SPACE);
    gUiDisplayColumn+=UI_DISP_COL_GAP;
    gUiDisplayColumn-=2;
    //-- gUiDisplayColumn-=2;
    gUiDisplayColumn+=UI_DISP_COL_GAP;    
    displayChar(DISPLAY_FONT_INDEX_WORKING);

    //-- gIsDisplayHeartRateProcessingJustFinished=TRUE;
}

static void uiDisplayHeartRate(void)
{
    DeviceEventStatus *eStatus=atGetDeviceEvent();
    if(eStatus->heartRateRequest==TRUE){ //-- 150712
        uiDisplayHeartRateRequestRunning();     
        return;
    }
    uiDisplayClearAndStart();
    gUiDisplayColumn=UI_DISP_COL_POS_HEARTRATE_LABEL;
    displayChar(DISPLAY_FONT_INDEX_HEARTRATE);
      
    //-- 150504
    {
        uiDisplayHeartRateNotice(TRUE); // display previous content
        uiDisplayShowConnectStatus(); //-- 150603
    }
    //-- 150603
    if(touchHeartRatePollRunning()==TRUE){
        if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){
            TimerDelete(uiDisplayTimeoutTimer_tid);
            uiDisplayTimeoutTimer_tid=TIMER_INVALID;
        }
    }

}

//-- called by touch when start measuring pulse, get chance to display right
void uiDisplayHeartRateActive()
{
    //-- 150603 uiDisplayHeartRateProcessing(TRUE);     // 2015.01.31 reset first 
    
    if(uiDisplayMode==UI_DISPLAY_MODE_HRD){
        if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){
            TimerDelete(uiDisplayTimeoutTimer_tid);
            uiDisplayTimeoutTimer_tid=TIMER_INVALID;

            uiDisplayHeartRate();
            
        }
    }
}

void uiDisplayHeartRateStop()
{
    if(uiDisplayMode==UI_DISPLAY_MODE_HRD && isDisplayTimeout==FALSE){
        uiDisplayNewCycle(UI_DISPLAY_TIMEOUT_SECOND);    
    }                
}

#ifdef CONFIG_VIBRATION

timer_id vibrationTimer_tid;

static void vibrationOn(void) 
{
   PioSet(VIBRATOR_PIO, TRUE);   
}
static void vibrationOff(void) 
{
   PioSet(VIBRATOR_PIO, FALSE);   
}
static void uiVibrationTimeoutTimerHandler(timer_id tid)
{
   if(vibrationTimer_tid!=tid) return;
   vibrationTimer_tid=TIMER_INVALID;
   
   vibrationOff();
}

void uiVibrationStart(int period)
{    
   if(vibrationTimer_tid!=TIMER_INVALID){
       TimerDelete(vibrationTimer_tid);
       vibrationTimer_tid=TIMER_INVALID;
       vibrationOff();
   } 
   vibrationTimer_tid=TimerCreate((period*MILLISECOND), TRUE, uiVibrationTimeoutTimerHandler);
   if(vibrationTimer_tid!=TIMER_INVALID){
       vibrationOn();
   }
}

static uint8 vibrationAlertCounter;

static void vibrationAlertTimerHandler(timer_id tid)
{ 
    uint8 nextTimerInSecond=15;  // every 15 seconds
    
    if(vibrationAlertTimer_tid!=tid) return;

    uiVibrationStart(300);  // 300ms long vibration
    
    vibrationAlertCounter++;
    if(vibrationAlertCounter<=4){ //-- 150502 total max. 60 seconds
        vibrationAlertTimer_tid=TimerCreate((nextTimerInSecond*SECOND), 
                                        TRUE, vibrationAlertTimerHandler);   
    }
}

static void vibrationAlert(bool onoff)
{  
    if(vibrationAlertTimer_tid!=TIMER_INVALID){
        TimerDelete(vibrationAlertTimer_tid);        
        vibrationAlertTimer_tid=TIMER_INVALID;
    } 
    
    if(onoff==TRUE){    
        vibrationAlertCounter=0;
        vibrationAlertTimerHandler(vibrationAlertTimer_tid);        
    }        
    else{
        TimerDelete(vibrationAlertTimer_tid); 
        vibrationAlertTimer_tid=TIMER_INVALID;
        vibrationOff(); // just in case
    }
}


#endif

static void uiDisplayAlertBase(bool onoff, bool *event, uint8 symbolIndex)
{       
    if(onoff==TRUE)
    {      
        
        uiDisplayNewCycle(UI_DISPLAY_ALERT_TIMEOUT_SECOND);
        uiDisplayClearAndStart();
        uiDisplayScroll(FALSE);
        displayChar(symbolIndex);
        uiDisplayScroll(TRUE);
        hasDisplayReset=FALSE;
            
        //-- 150504 force to _OFF mode, to display this event, when next display is ON.
        uiDisplayMode=UI_DISPLAY_MODE_OFF;  

#ifdef CONFIG_VIBRATION        
        uiVibrationStart(300);
#endif
        if(*event==FALSE)
        {
#ifdef CONFIG_VIBRATION
            vibrationAlert(TRUE);
#else
            touchAlert(TRUE);
#endif
        }
    }
    else
    {   
        if(hasDisplayReset==FALSE){
            uiDisplayScroll(FALSE);
            uiDisplayClearAndStart();

 #ifdef CONFIG_VIBRATION
            vibrationAlert(FALSE);
#else
            touchAlert(FALSE);
#endif

        }
    }    
    *event=onoff;
}


void uiDisplayAlert(bool onoff)
{       
    uiDisplayAlertBase(onoff,&gHasAlertEvent, DISPLAY_FONT_INDEX_ALERT);
}

void uiDisplayMessage(bool onoff)
{
    uiDisplayAlertBase(onoff,&gHasMessageEvent, DISPLAY_FONT_INDEX_MESSAGE);
}    
    
void uiDisplayCharging(bool onoff)  
{   
    if(onoff==TRUE){        
        uiDisplayClearAndStart();
        uiDisplayScroll(FALSE);
        displayChar(DISPLAY_FONT_INDEX_CHARGING);
        uiDisplayScroll(TRUE);
    }
    else{
        uiDisplayScroll(FALSE);
        uiDisplayClearAndStart();
    }
    
    isUiDisplayCharging=onoff;
}

bool uiDisplayIsOn(void)
{
    if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){
        return TRUE;
    }
    return FALSE;
}

void uiDisplayShow(bool isNewPage)
{   
    bool wasDisplayTimeout=isDisplayTimeout;
    
    uiDisplayNewCycle(UI_DISPLAY_TIMEOUT_SECOND);
    
    if(isNewPage==TRUE || wasDisplayTimeout==TRUE) //--  || )
    { // not on display
                              
        if(uiDisplayMode==UI_DISPLAY_MODE_TIME){
            uiDisplayTime();
        }
        else if(uiDisplayMode==UI_DISPLAY_MODE_ACTIVITY){
            uiDisplayActivity();
        }
        else if(uiDisplayMode==UI_DISPLAY_MODE_HRD){
            uiDisplayHeartRate();
        }
        else if(uiDisplayMode==UI_DISPLAY_MODE_OFF){
#ifdef CONFIG_IBEACON
            uiDisplayBoard();  
#else
            // if has following event, then display them, otherwise, goto initial mode
            if(gHasAlertEvent==TRUE || gHasMessageEvent==TRUE ||
                CheckBatteryLevel(FALSE)==FALSE)
            {
                uiDisplayBoard();
            }            
            else
            {
                //-- uiDisplayDown();
                uiDisplayStart();   //-- 09.18
                return; // 09.19 ignore the battery part below;
            }
#endif
        }

    }
#if 0 // 10.10   
    if(uiDisplayMode==UI_DISPLAY_MODE_SHAKE){
        uiDisplayShakeSetting(isNewPage || (wasDisplayTimeout==TRUE));    
    }    
#endif     
    
    if(CheckBatteryLevel(FALSE)==FALSE && gIsDisplayScrolling==FALSE)
    {
        if(uiDisplayMode==UI_DISPLAY_MODE_TIME){
            gUiDisplayColumn=UI_DISP_COL_POS_BATTERY_OUT_TIME;
        }else if(uiDisplayMode!=UI_DISPLAY_MODE_HRD){
            gUiDisplayColumn=UI_DISP_COL_POS_BATTERY_OUT; 
        }else{
            gUiDisplayColumn=UI_DISP_COL_POS_HEARTRATE_LABEL+5;
        }
        displayChar(DISPLAY_FONT_INDEX_BATTERY_OUT);                
    }
}

#ifdef CONFIG_IBEACON

// 150505
static bool connectingDisplayFlag;
static timer_id gUiDisplayConnecting_tid;

static void uiDisplayFlashConnecting(void)
{
    int8 storedDisplayColumn;

    if(gIsDisplayScrolling==TRUE) return; // no need to update if in scrolling
    
    if( ! ((uiDisplayMode==UI_DISPLAY_MODE_HRD || uiDisplayMode==UI_DISPLAY_MODE_OFF) &&
           uiDisplayTimeoutTimer_tid!=TIMER_INVALID)
       )
        return;  

    if(uiDisplayIdTimer_tid!=TIMER_INVALID) return; //-- 150712 skip if display id
    
    storedDisplayColumn=gUiDisplayColumn;
    gUiDisplayColumn=UI_DISP_COL_POS_STATUS;

    gUiDisplayColumn=UI_DISP_COL_POS_STATUS;
    if(connectingDisplayFlag==TRUE){
        connectingDisplayFlag=FALSE;
        displayChar(DISPLAY_FONT_INDEX_SPACE);
    } else {
        connectingDisplayFlag=TRUE;
        if(g_ht_data.state!=app_state_connected){
            displayChar(DISPLAY_FONT_INDEX_DISCONNECTED);
        }else{
            displayChar(DISPLAY_FONT_INDEX_CONNECTED);
        }
    }
    gUiDisplayColumn=storedDisplayColumn;       
}    

static void uiDisplayConnectingTimerHandler(timer_id tid)
{
    DeviceEventStatus *eStatus=atGetDeviceEvent();
    
    if(tid!=gUiDisplayConnecting_tid) 
        return;

    gUiDisplayConnecting_tid=TIMER_INVALID;
    
    if(eStatus->connectRequest==FALSE){ // means, not try connecting any more
         //-- 150517 both cases need, if(g_ht_data.state!=app_state_connected)
         {
            connectingDisplayFlag=FALSE;    // need to redraw again to make sure
            uiDisplayFlashConnecting();
         }
         return;
     }
           
   
    // repeat timer
    gUiDisplayConnecting_tid=TimerCreate((1*SECOND), TRUE, uiDisplayConnectingTimerHandler);
   
    uiDisplayFlashConnecting();    
}    
 
#endif //--  CONFIG_IBEACON 

static timer_id uiDisplayShowDelayedTimer_tid;

#ifdef CONFIG_IBEACON

static void uiDisplayConnectRequest(void)
{
    DeviceEventStatus *eStatus=atGetDeviceEvent();
    if(eStatus->connectRequest==TRUE){  // if trying request to connect now
        atDeviceEvent(AT_DEVICE_EVENT_CONNECT_REQUEST, FALSE);  // then stop this request              
    }else{
        atDeviceEvent(AT_DEVICE_EVENT_CONNECT_REQUEST, TRUE);   // otherwise to start connect request
        connectingDisplayFlag=TRUE;    // reset the flag, change in every second
        uiDisplayConnectingTimerHandler(gUiDisplayConnecting_tid);
#ifdef CONFIG_PUSH_ADVERTISING
        PushAdvertisingEnable();    // 150517 to make sure adv. is working, 150513
#endif
    }
}                

static void uiDisplayHeartRateRequestRunning(void)
{
    uiDisplayNewCycle(UI_DISPLAY_TIMEOUT_SECOND);
    uiDisplayClearAndStart();
    gUiDisplayColumn=UI_DISP_COL_POS_HEARTRATE;
    displayChar(DISPLAY_FONT_INDEX_ARROWTO);
    uiDisplayScroll(TRUE);
}    

static void uiDisplayHeartRateRequest(void)
{
    DeviceEventStatus *eStatus=atGetDeviceEvent();
    
    if(eStatus->connectRequest==TRUE){ // call again to stop trying connect
          uiDisplayConnectRequest();    // to stop request
     }    
    
    if(eStatus->heartRateRequest==TRUE){  // if trying request to connect now
        atDeviceEvent(AT_DEVICE_EVENT_HEARTRATE_CYCLE, FALSE);  // then stop this request
        uiDisplayShow(TRUE);
    }else{
        atDeviceEvent(AT_DEVICE_EVENT_HEARTRATE_CYCLE, TRUE);   // otherwise to start connect request
#ifdef CONFIG_PUSH_ADVERTISING
        PushAdvertisingEnable();    // 150517 to make sure adv. is working, 150513
#endif
        uiDisplayHeartRateRequestRunning(); //-- 150712
    }
}

#endif



static void uiDisplayIdTimerTimeoutHandler(timer_id tid)
{
    if(tid!=uiDisplayIdTimer_tid) return;
    
    uiDisplayIdTimer_tid=TIMER_INVALID;
    
    if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){    //-- DISPLAY is ON
        uiDisplayShow(TRUE);
    }
}
    
static void uiPrompt(int periodSec)
{
    if(uiDisplayIdTimer_tid!=TIMER_INVALID){
        TimerDelete(uiDisplayIdTimer_tid);
        uiDisplayIdTimer_tid=TIMER_INVALID;
    }
    uiDisplayIdTimer_tid=TimerCreate((periodSec*SECOND), TRUE, uiDisplayIdTimerTimeoutHandler);            
}    
    
static void uiDisplayActionDone(void)
{
        if(uiDisplayMode==UI_DISPLAY_MODE_HRD) {      
            if(g_ht_data.state==app_state_connected){
                if(touchHeartRatePollRunning()==TRUE){  // 2015.01.31
                    // if already running polling, then stop
                    gCurrentHR=0;   // clear this value first                
                    touchSessionEnd(TOUCH_WAKE_WITH_NOACTION);
                    
                    //-- 150505 won't redraw uiDisplayShow(FALSE);  
                    uiDisplayHeartRate();
                }
                else
                {
                    // else, start it.
                    touchPowerdown(FALSE);  // leave power down mode
                    touchHeartRateStartMeasure();
                }               
            } 
#ifdef CONFIG_IBEACON
            else {  // if disconnected
                uiDisplayConnectRequest();
            }
#endif
        } else if(uiDisplayMode==UI_DISPLAY_MODE_OFF) { 
            //-- display system ID in 6 digits
            //-- setIbeaconUuid(); // 20150720 for test
            
            uiDisplayNewCycle(UI_DISPLAY_TIMEOUT_SECOND);
            uiDisplayClearAndStart();
            {
                uint32 systemId=0; // for total 24 bits;
                systemId=(g_at_serv_data.beacon_uuid[1]);    // upper bits of 16 bits
                systemId=systemId<<16;  // move to 16-23bits
                systemId+=g_at_serv_data.beacon_uuid[0];    // 16 bits for 0-15bits
                //-- systemId=12345678;
                systemId=systemId%1000000;
                uiDisplayNumber(systemId, 6, FALSE);
            }                  
            uiDisplayScroll(TRUE);
            uiPrompt(4);
                          
            touchPowerdown(FALSE);  // wakeup touch sensor
            touchSessionEnd(1); // 20150721 mainly for production test purpose
            
        } else if(uiDisplayMode==UI_DISPLAY_MODE_TIME){
            if(CLOCK_ALARM_ENABLED==FALSE) return;
            
            uiDisplayNewCycle(UI_DISPLAY_TIMEOUT_SECOND);
            uiDisplayClearAndStart();
            uiDisplayNumber(CLOCK_ALARM_HOUR, 2, TRUE);
            gUiDisplayColumn+= 3;
            displayChar(DISPLAY_FONT_INDEX_COLON);
            gUiDisplayColumn+= 3;    
            uiDisplayNumber(CLOCK_ALARM_MINUTE, 2, FALSE);    
            uiPrompt(2);            
        } else if(uiDisplayMode==UI_DISPLAY_MODE_ACTIVITY){            
            uiDisplayNewCycle(UI_DISPLAY_TIMEOUT_SECOND);
            uiDisplayActivityShow(gActivityGoal);  
            uiPrompt(2);            
        }
}

#ifdef CONFIG_TARGET_BINSZ3

static void uiHeartRateBatchCycle(void) 
{
    if(g_ht_data.state==app_state_connected){
        touchHeartRateBatchCycle(FALSE);    // do not disconnect after cycle
    }else {
        uiDisplayHeartRateRequest();        
    }
    
}
void uiLongPressAction(void)
{
    uiVibrationStart(30);
    
    if(isDisplayTimeout==TRUE){ // means display is off now                    
        uiHeartRateBatchCycle();      
    }else { // if display is on, then handle by each display mode
        if(uiDisplayMode==UI_DISPLAY_MODE_OFF){
            if(g_ht_data.state==app_state_connected){ 
                atDisconnectByDevice();
                // GattDisconnectReq(g_ht_data.st_ucid);
            } 
            else {
                uiDisplayConnectRequest();
            }
            uiDisplayBoard();  // display again with updated status
        }else if(uiDisplayMode==UI_DISPLAY_MODE_ACTIVITY){
            dayStepCounterModeSwitch();
            uiDisplayActivity();
        }else{
            uiHeartRateBatchCycle();                    
        }  
    }
}

#endif
         
void uiDisplayShowAndDone(void)
{
    bool wasDisplayTimeout=isDisplayTimeout;
    bool wasDisplayScrolling=gIsDisplayScrolling;
    uiDisplayShow(FALSE);      
    
    // 150602
    // will only able to reach here if from button press to run action
    
    if(wasDisplayTimeout==TRUE) return;  // only need to process button, if display is ON
    
    if(wasDisplayScrolling==TRUE ){
        // if was display scrolling from alert message, then skip too, 150620, 
        return;
    }
    
    uiDisplayActionDone();
    
}

//-- it is called eventually after a single button press, to handle button press case when HRD

static void uiDisplayShowDelayedTimerHandler(timer_id tid)
{       
    if(uiDisplayShowDelayedTimer_tid!=tid) return;  
    uiDisplayShowDelayedTimer_tid=TIMER_INVALID;
    
#ifndef CONFIG_TARGET_BINSZ3
    uiDisplayShow(FALSE);
#else  // for Z3
    uiDisplayShowAndDone();
#endif // END CONFIG_TARGET_BINSZ3
}

static void uiDisplayStartDelayedTimerHandler(timer_id tid)
{       
    if(uiDisplayShowDelayedTimer_tid!=tid) return;  
    uiDisplayShowDelayedTimer_tid=TIMER_INVALID;
    
    uiDisplayStart();
}

void uiDisplayShowDelayed(uint16 delayTimeMs)
{
    if(uiDisplayShowDelayedTimer_tid!=TIMER_INVALID){
        TimerDelete(uiDisplayShowDelayedTimer_tid);
        uiDisplayShowDelayedTimer_tid=TIMER_INVALID;
    }
    if(delayTimeMs==0) return;

    uiDisplayShowDelayedTimer_tid=TimerCreate((delayTimeMs*MILLISECOND), TRUE, 
                                              uiDisplayShowDelayedTimerHandler);   
}


void uiDisplayStart(void)
{   
    //-- if(isDisplayTimeout==FALSE || uiDisplayMode==UI_DISPLAY_MODE_OFF)
    if(isDisplayTimeout==FALSE)
    { 
        uiDisplayMode++;
        if(uiDisplayMode>=UI_DISPLAY_MODE_MAX) uiDisplayMode=UI_DISPLAY_MODE_MIN;
    }
        
    uiDisplayShow(TRUE);
    
}

void uiDisplayStartDelayed(uint16 delayTimeMs)
{
    if(uiDisplayShowDelayedTimer_tid!=TIMER_INVALID){
        TimerDelete(uiDisplayShowDelayedTimer_tid);
        uiDisplayShowDelayedTimer_tid=TIMER_INVALID;
    }
    if(delayTimeMs==0) return;

    uiDisplayShowDelayedTimer_tid=TimerCreate((delayTimeMs*MILLISECOND), TRUE, 
                                              uiDisplayStartDelayedTimerHandler);        
}
uint8 uiDisplayCurrentMode(bool *isActive)
{
    if(uiDisplayTimeoutTimer_tid!=TIMER_INVALID){
        *isActive=TRUE;
    }
    else{
        *isActive=FALSE;   
    }
    
    return uiDisplayMode;
}

void uiDisplayInit(void)
{
    uiDisplayMode=UI_DISPLAY_MODE_OFF;
    uiDisplayDown();
    uiDisplayTimeoutTimer_tid=TIMER_INVALID;
    uiDisplayShowDelayedTimer_tid=TIMER_INVALID;
    uiDisplayIdTimer_tid=TIMER_INVALID;
#ifdef CONFIG_IBEACON
    gUiDisplayConnecting_tid=TIMER_INVALID;
#endif
    
    isDisplayTimeout=FALSE;
    isUiDisplayCharging=FALSE;

    //-- for test
    // gHasAlertEvent=TRUE;
    // gHasMessageEvent=TRUE;
    
    gHasAlertEvent=FALSE;
    gHasMessageEvent=FALSE;
    
    gIsDisplayScrolling=FALSE;
    
#ifdef CONFIG_VIBRATION // 12.22  
    vibrationAlertTimer_tid=TIMER_INVALID;
    vibrationTimer_tid=TIMER_INVALID;
#endif     
    
    //-- gIsDisplayHeartRateProcessingJustFinished=FALSE;
}

void setUiDisplayMode(int displayMode)
{
    uiDisplayMode=displayMode;      
}