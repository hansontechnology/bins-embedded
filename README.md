# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the source of embedded software running on CSR's CSR101x microncontroller for BiNS wearable product.
* It works with counterpart iOS/Android mobile apps, connecting through Bluetooth, to complete user's health monitoring tasks.
* Version : The project is finished on September 2015.
* Algorithms to detect steps, activity, sleep quality are optimized for this 16bit 16MHz 64KB RAM microncontroller.
* For example, for detecting steps and running, a higly simplified highpass filtering is used, and it has to be adaptive enough to various scenarios.
* Product document can be found [here](/bins-documents/BiNS-Z3-Users-Manual-v2-150831.pdf), and [others](/bins-documents)

![BiNS Z3 Product](/BiNS-Z3-product-photos/BiNS-Z3-photo-1.JPG)
* Other product photos can be found [here](/BiNS-Z3-product-photos)

![BiNS App for Android](/bins-app-android-screenshots/bins-app-android-sleep.png)
* Screenshots of mobile app for Android is located [here](/bins-app-android-screenshots)
* Screenshots of mobile app for iOS is located [here](/bins-app-ios-screenshots)


### How do I get set up? ###

* CSR uEnergy SDK tool is required to build the image, and interact with target board through SDK's USB dungle.
* CSR's uEnergy SDK has a complete IDE software on Windows environment.
* CSRmesh and system libraries will be used to use its Bluetooth equipped microcontroller's features.
* BiNS target board needs to connect to SDK's USB dungle for image download and test (includes debugging).
* Load health_thermometer.xiw ( CSR xIDE Workspace file ) from the root folder.
* (health_thermometer template project was used to start BiNS project, that is why the name was come from)

### Contribution guidelines ###


### Who do I talk to? ###

* Following are the possible ways to utilize the codes for future applications.
* Not only for human's activity tracking purpose, but also it has the potential for live stock monitoring.
* Although it assumes working on Bluetooth LE connectivity, this can be migrated to future narrow band technologies.
