/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2012-2013
 *  Part of CSR uEnergy SDK 2.2.2
 *  Application version 2.2.2.0
 *
 *  FILE
 *      health_thermo_uuids.h
 *
 *  DESCRIPTION
 *      UUID MACROs for Health Thermometer service
 *
 *****************************************************************************/

#ifndef __ACTIVITY_TRACKING_UUIDS_H__
#define __ACTIVITY_TRACKING_UUIDS_H__

/*============================================================================*
 *  Public Definitions
 *============================================================================*/

/* Brackets should not be used around the value of a macro. The parser 
 * which creates .c and .h files from .db file doesn't understand
 * brackets and will raise syntax errors.
 */

/* For UUID values, refer http://developer.bluetooth.org/gatt/services/Pages/
 * ServiceViewer.aspx?u=org.bluetooth.service.health_thermometer.xml
 */


#define UUID_ACTIVITY_TRACKING_SERVICE          0x12345678123412341234123456789ABC

#define UUID_AT_ACTIVITY_TYPE                   0x20000001123412341234123456789ABC

#define UUID_AT_ACTIVITY_VALUE                  0x20000002123412341234123456789ABC

#define UUID_AT_ACTIVITY_TIME                   0x20000003123412341234123456789ABC

#define UUID_AT_SETTING_CFG                     0x20000004123412341234123456789ABC

#define UUID_AT_NEW_ACTIVITY                    0x20000005123412341234123456789ABC
#define UUID_AT_ACTIVITY_TIME_VALUE             0x20000006123412341234123456789ABC

#define UUID_AT_LOCAL_CLOCK                     0x20000010123412341234123456789ABC
#define UUID_AT_TIMER                           0x20000011123412341234123456789ABC
#define UUID_AT_ACTIVITY_GOAL                   0x20000012123412341234123456789ABC
#define UUID_AT_UI_SENSITIVITY                  0x20000013123412341234123456789ABC
#define UUID_AT_LINK_LOSS_ALERT                 0x20000014123412341234123456789ABC
#define UUID_HR_DATA_STREAM                     0x20000015123412341234123456789ABC
#define UUID_HR_HOST_UPDATE                     0x20000016123412341234123456789ABC
#define UUID_SLEEP_RECORDS                      0x20000017123412341234123456789ABC
#define UUID_SLEEP_RECORD_TIME_SHIFT            0x20000018123412341234123456789ABC
#define UUID_IBEACON_UUID                       0x20000019123412341234123456789ABC
#define UUID_AT_DEBUG_OUTPUT                    0x2000001A123412341234123456789ABC
#define UUID_AT_UI_GESTURE_ANGLE                0x2000001B123412341234123456789ABC
#define UUID_AT_UI_GESTURE_WTIME                0x2000001C123412341234123456789ABC
#define UUID_AT_CLOCK_ALARM                     0x2000001D123412341234123456789ABC


#endif /* __HEALTH_THERMO_UUIDS_H__ */

