/**
---------------------------------------------------------------------
**
**  This device driver was created by 
**  8-Bit Single-Chip Microcontrollers
**
**  Copyright(C) visionox display Corporation 2002 - 2009
**  All rights reserved by Kunshan visionox display Corporation.
**
**  This program should be used on your own responsibility.
**
**  Filename :	IC_Init.c
**  Abstract :	This file implements device driver for Initial SH1106 IC function.
**
**  Device :	STC89LE516RD
**
**  Compiler :	KEIL uVision2 
**
**  Module:  M01030
**
**  Dot Matrix: 128*32
**
**  Display Color: White
**
**  Driver IC: SH1106G
**
**  Edit : 
**
**  Creation date:	
---------------------------------------------------------------------
**/
//
//
//
/****************************包含头文件***************************/

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/

#include <pio.h>
#include <pio_ctrlr.h>
#include <timer.h>
#include <time.h>

#include "configuration.h"
#include "i2c_comms.h"
#include "display_hw.h"
#include "display_init.h"
#include "display_mode.h"

/**********************************************
//
//写命令函数

**********************************************/
//
//

bool displayWriteCommand(uchar command)
{
    return i2cWriteRegister(DISPLAY_I2C_ADDR, DISPLAY_COMMAND_REG, command);
}

/**********************************************
//
//写数据函数
//
**********************************************/
//
//

bool displayWriteData(uchar data)
{
    return i2cWriteRegister(DISPLAY_I2C_ADDR, DISPLAY_DATA_REG, data);
}

/******************************************************
//
//复位IC函数
//
******************************************************/
//
//

void displayReset()
{
    bool result;
#ifdef CONFIG_TARGET_BINSZ3
    displayVcc(FALSE);
    TimeWaitWithTimeout16(FALSE, (100*MILLISECOND), result);
#endif    
    PioSetMode(DISPLAY_RESET, pio_mode_user);
    PioSetDir(DISPLAY_RESET, TRUE);   //-- Output   
    TimeWaitWithTimeout16(FALSE, (100*MILLISECOND), result);
    PioSet(DISPLAY_RESET, FALSE);
    TimeWaitWithTimeout16(FALSE, (100*MILLISECOND), result);
    PioSet(DISPLAY_RESET, TRUE);
    TimeWaitWithTimeout16(FALSE, (100*MILLISECOND), result);
}

#ifdef CONFIG_TARGET_BINSZ3
/******************************************************
//
//
//Haley 20150126
*******************************************************/
void displayVcc(bool onoff)
{
    if(onoff)
    {
       PioSetMode(DISPLAY_ENVCC, pio_mode_user);  
       PioSetDir(DISPLAY_ENVCC, TRUE);           
       PioSet(DISPLAY_ENVCC, TRUE); 
    }
    else
    {
       PioSetMode(DISPLAY_ENVCC, pio_mode_user);  
       PioSetDir(DISPLAY_ENVCC, TRUE);           
       PioSet(DISPLAY_ENVCC, FALSE); 
   }
}
#endif

/******************************************************
//
//初始化IC函数
//
******************************************************/


bool displayInit()
{
    bool ret;
    
    PioSetMode(DISPLAY_RESET, pio_mode_user);
    PioSetDir(DISPLAY_RESET, TRUE);
    PioSet(DISPLAY_RESET, TRUE);

    i2cUsePeripheralBus();
    
    {
       ret=displayWriteCommand(0xAE);     //Set Display Off 

	   if(ret==FALSE) return ret;	// to detect if display is ready

       displayWriteCommand(0x00);     //Set lower column start address
       displayWriteCommand(0x10);     //Set higher column start address

       displayWriteCommand(0x20);		  //SET PAGE ADDRESS MODE
       displayWriteCommand(0x02);

       displayWriteCommand(0x21);		  //SET COLUMN ADDRESS START AND END
       displayWriteCommand(0x00);
       displayWriteCommand(0x7F);

       displayWriteCommand(0x22);		  //SET PAGE ADDRESS START END
       displayWriteCommand(0x00);
       displayWriteCommand(0x03);

       displayWriteCommand(0x2E);		  //SET PAGE ADDRESS START END
       displayWriteCommand(0x26);         // Right horizental scroll
       displayWriteCommand(0x00);         // Dummy
       displayWriteCommand(0x00);         // Page start
       displayWriteCommand(0x00);         // Time Interval
       displayWriteCommand(0x07);         // Page end
       // displayWriteCommand(0x2F);         // Activate scroll       

       displayWriteCommand(0x29);         // Vertical and Right horizental scroll
       displayWriteCommand(0x00);         // Dummy
       displayWriteCommand(0x00);         // Page start
       displayWriteCommand(0x00);         // Time Interval
       displayWriteCommand(0x07);         // Page end
       displayWriteCommand(0x00);         // Verticalscrolling as 1 row      
       // displayWriteCommand(0x2F);         // Activate scroll       
       
       displayWriteCommand(0xd5);     //display divide ratio/osc. freq. mode	
       displayWriteCommand(0x81);     //Divide ratio 2

       displayWriteCommand(0xA8);     //multiplex ration mode:32
       displayWriteCommand(0x1F);

       displayWriteCommand(0xD3);       // 05.29 Set Display Offset   
       displayWriteCommand(0x00);	    // 0x3A Set vertical shift
   
       displayWriteCommand(0x40);     //Set Display Start Line 
  
       displayWriteCommand(0xAD);     //Internal IREF Setting 
       displayWriteCommand(0x30);     //Selectting: 30uA

       displayWriteCommand(0xA1);     //Segment Remap	 

       displayWriteCommand(0xC8);     // Set COM Output Scan Direction 0-63	

       displayWriteCommand(0xDA);     //Common pads hardware: alternative	
       displayWriteCommand(0x12);     // 12 
#ifdef CONFIG_TARGET_BINSZ3
       displayWriteCommand(0x81);     //Contrast control 
       displayWriteCommand(0x99);     // 10.09 20, 33, 99
   	
       displayWriteCommand(0x8D);  	  //Internal charge pump:
       displayWriteCommand(0x10);	    //Disable Haley 01.26
       displayVcc(TRUE);
#else
       displayWriteCommand(0x81);     //Contrast control 
       displayWriteCommand(0x20);     // 10.09 20, 33, 99
   	
       displayWriteCommand(0x8D);  	//Internal charge pump:
       displayWriteCommand(0x95);	    //Enable
#endif                      
       displayWriteCommand(0xD9);	    //Set pre-charge period	  
       displayWriteCommand(0x22);

       displayWriteCommand(0xDB);     //VCOM deselect level mode 
       displayWriteCommand(0x10);	    //VCOM:0.71*VCC

       displayWriteCommand(0xA4);     //Set Entire Display On/Off	
    
       displayWriteCommand(0xA6);     //Set Normal Display 
       Display_Clear_Screen();			//Clear Screen
       //-- initial is off state -- displayWriteCommand(0xAF);     //Set Display On 
   }   
    i2cUseMainBus();
    
    return ret;
}


