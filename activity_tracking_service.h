/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2012-2013
 *  Part of CSR uEnergy SDK 2.2.2
 *  Application version 2.2.2.0
 *
 *  FILE
 *      health_thermo_service.h
 *
 *  DESCRIPTION
 *      Header definitions for Health Thermometer service
 *
 *****************************************************************************/

#ifndef __ACTIVITY_TRACKING_SERVICE_H__
#define __ACTIVITY_TRACKING_SERVICE_H__

/*============================================================================*
 *  SDK Header Files
 *===========================================================================*/

#include <types.h>
#include <bt_event_types.h>
#include "configuration.h"
#include "app_gatt.h"
#include "pedometer.h"

/*============================================================================*
 *  Public Function Prototypes
 *===========================================================================*/


/* This function is used to initialise Health Thermometer service data 
 * structure
 */
extern void ActivityTrackingDataInit(void);

/* This function handles read operation on health thermometer service 
 * attributes maintained by the application
 */
extern void ActivityTrackingHandleAccessRead(GATT_ACCESS_IND_T *p_ind);

/* This function handles write operation on health thermometer service 
 * attributes maintained by the application
 */
extern void ActivityTrackingHandleAccessWrite(GATT_ACCESS_IND_T *p_ind);

/* This function is used to send temperature reading as an indication 
 * to the connected host
 */
extern bool ActivityTrackingSendReading(uint16 ucid, int16 new_temp);

/* This function is used to set the status of pending confirmation for the 
 * transmitted temperature measurement indications
 */
extern void ActivityTrackingRegIndicationCfm(bool ind_state);


/* This function is used to check if the handle belongs to the health 
 * thermometer service
 */
extern bool ActivityTrackingCheckHandleRange(uint16 handle);

/* This function is used by application to notify bonding status to 
 * health themometer service
 */
extern void ActivityTrackingBondingNotify(void);

/* This function returns whether indications are configured for Temperature
 * Measurement characteristic
 */
extern bool ActivityTrackingClientSettingIndConfigStatus(void);

extern void ActivityTrackingReadDataFromNVM(bool nvm_fresh_start, uint16 *p_offset);

extern void activityValueNotify(uint16 ucid, StepRecord *sr, uint8 numSteps);

extern void activityLinkLossCheck(void);

extern void heartRateRawDataNotify(uint16 ucid, uint8 *hrdData);       

extern void ActivityTrackingRegNotificationCfm(GATT_CHAR_VAL_IND_CFM_T *event_data);


//** test
extern uint16 second_boot;

/* CSR1010 sleep modes
   
Deep sleep mode is good enough, Hibernate maybe considerable


Deep sleep
VDD_PADS = ON, REFCLK = OFF, SLEEPCLK = ON,
VDD_BAT = ON, RAM = ON, digital circuits = ON,
SMPS = ON (low-power mode), 1ms wake-up time
<5��A


Hibernate VDD_PADS = ON, REFCLK = OFF, SLEEPCLK = ON,
VDD_BAT = ON 
<1.5��A

Dormant All functions are shut down. To wake them up, toggle the
WAKE pin. 
<600nA
*/



// 08.19 disable #define BINS_CONFIG_COMMAND_SYS_RESET_BIT           0
#define BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_BIT           0
#define BINS_CONFIG_COMMAND_HRD_START_BIT           1
#define BINS_CONFIG_COMMAND_TOUCH_LOCK_BIT          2   //-- 08.20


#define BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK (1<< BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_BIT)
#define BINS_CONFIG_COMMAND_HRD_START_MASK      (1<< BINS_CONFIG_COMMAND_HRD_START_BIT)
#define BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK     (1<< BINS_CONFIG_COMMAND_TOUCH_LOCK_BIT)
#define BINS_CONFIG_HRD_ALWAYS_ON_BIT               7
#define BINS_CONFIG_HRD_ALWAYS_ON_MASK          (1<< BINS_CONFIG_HRD_ALWAYS_ON_BIT)



#define BINS_CONFIG_HEARTRATE_ENABLE_BIT            6
#define BINS_CONFIG_HEARTRATE_ENABLE_MASK       (1<< BINS_CONFIG_HEARTRATE_ENABLE_BIT)

#define BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_BIT      5
#define BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK (1<< BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_BIT)

#define BINS_CONFIG_STEP_WITH_HRD_ENABLE_BIT      4
#define BINS_CONFIG_STEP_WITH_HRD_ENABLE_MASK   (1<< BINS_CONFIG_STEP_WITH_HRD_ENABLE_BIT)

#define BINS_CONFIG_COMMAND_UI_ONETIME_BIT      3
#define BINS_CONFIG_COMMAND_UI_ONETIME_BIT_MASK (1<< BINS_CONFIG_COMMAND_UI_ONETIME_BIT)

#define BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT       9
#define BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK   (1<< BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT)

#define BINS_CONFIG_TOUCH_NOBLINK_BIT             10 
#define BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK   (1<< BINS_CONFIG_TOUCH_NOBLINK_BIT)

#define BINS_CONFIG_SLEEP_MONITORING_ENABLE_BIT      12 //-- 09.26
#define BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK   (1<< BINS_CONFIG_SLEEP_MONITORING_ENABLE_BIT)

#define BINS_CONFIG_MESSAGE_ALERT_ENABLE_BIT      13 //-- 10.08
#define BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK   (1<< BINS_CONFIG_MESSAGE_ALERT_ENABLE_BIT) 
                                                                //--11.13
#if 0

#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_INCREASE_BIT      15 //-- 10.10
#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_INCREASE_MASK   (1<< BINS_CONFIG_UI_WAKEUP_THRESHOLD_INCREASE_BIT)

#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_DECREASE_BIT      4 //-- 10.10
#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_DECREASE_MASK   (1<< BINS_CONFIG_UI_WAKEUP_THRESHOLD_DECREASE_BIT)

#endif

#define BINS_CONFIG_SETTING_ALL_MASK            (0xFFF0)    //-- 10.10 0xFFF0 to 0x7FE0

extern int getShakeThresholdFromConfig(uint8 config);  // 10.11
extern void setShakeThresholdToConfig(int8 threshValue);


#ifdef CONFIG_IBEACON
typedef enum {
    AT_DEVICE_EVENT_BATTERY,
    AT_DEVICE_EVENT_LONGPRESS,
    AT_DEVICE_EVENT_FREEDROP,
    AT_DEVICE_EVENT_CONNECT_REQUEST,
    AT_DEVICE_EVENT_HEARTRATE_CYCLE,
    AT_DEVICE_EVENT_HEARTRATE
} AtDeviceEvent;
typedef struct {
    bool batteryLow;
    bool longPress;
    bool freeDrop;
    bool connectRequest;
    bool heartRateRequest;
    bool heartRate;
} DeviceEventStatus;


#define BEACON_MAJOR_NORMAL  0
#define BEACON_MAJOR_BATTERY_LOW 1
#define BEACON_MAJOR_HEARTRATE 2
#define BEACON_MAJOR_FREEDROP 3
#define BEACON_MAJOR_BUTTON_SOS 4
#define BEACON_MAJOR_CONNECT_REQUEST 10
#define BEACON_MAJOR_HEARTRATE_CYCLE 11


extern void atDeviceEvent(AtDeviceEvent event, bool newStatus);
extern DeviceEventStatus *atGetDeviceEvent(void);
extern void atDisconnectByDevice(void);
extern void atCheckBatteryLow(void);
extern void setIbeaconUuid(void);

#endif


#define BEACON_UUID_LENGTH    20      // in bytes including Major and Minor

/* activity tracking service data type */
typedef struct
{

    /* Flag for pending indication confirm */
    bool                    ind_cfm_pending;

    /* Client configuration for activity tracking characteristic */
    gatt_client_config      activity_client_config;

    uint16                  local_clock_base;
    uint16                  activity_goal;
    uint16                  configuration;    
    uint32                  ui_sense_strength;
        /* Link Loss Alert Level */
    uint8                   link_loss_alert;

    /* Offset at which activity tracking data is stored in NVM */
    uint16                  nvm_offset;

#ifdef CONFIG_CLOCK_ALARM
    uint16                  clock_alarm;
#endif
    
#ifdef CONFIG_IBEACON    
    /* Beacon UUID and also Major and Minor data */
    
    uint16                  beacon_uuid[BEACON_UUID_LENGTH/2]; 
    DeviceEventStatus       eventStatus;
    bool                    connectByDevice;
#endif

    uint16                  sleep_cycle_time;
    
#ifdef CONFIG_UI_GESTURE_TRACK    
    uint16                  ui_gesture_angle;
    uint16                  ui_gesture_wait_time;
#endif    
    
} AT_SERV_DATA_T;

extern AT_SERV_DATA_T g_at_serv_data;


void ShortToByteArray(uint8 *bp, int16 sv); // library function


#endif /* __HEALTH_THERMO_SERVICE_H__ */
