/*******************************************************************************
 *    Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 * FILE
 *    motion.c
 *
 *  DESCRIPTION
 *    
 *
 ******************************************************************************/

#include "configuration.h"
#include "pedometer.h"
#include "touch.h"
#include "ht_hw.h"

#if !defined(REMOTE_IS_AIRMOUSE)
#if defined(ACCELEROMETER_PRESENT) || defined(GYROSCOPE_PRESENT)

/*=============================================================================
 *  SDK Header Files
 *============================================================================*/
#include <mem.h>
#include <gatt_uuid.h>
 
/*=============================================================================
 *  Local Header Files
 *============================================================================*/

#include "motion.h"
#include "pedometer.h"

#if defined(ACCELEROMETER_PRESENT)
#include "accelerometer.h"
#endif /* ACCELEROMETER_PRESENT */

/*=============================================================================
 *  Private Definitions
 *============================================================================*/

#if defined(ACCELEROMETER_PRESENT)
/* The remote may move to the connected idle state if there is no new 
 * accelerometer data for ACCELEROMETER_STABILITY_COUNT number of reads.
 */
#define ACCELEROMETER_STABILITY_COUNT       (10)
#endif /* ACCELEROMETER_PRESENT */


/* The device under consideration currently */
typedef enum {
    IS_ACCELEROMETER,
    IS_GYROSCOPE
} DEVICE_MEM_TYPE;

/*=============================================================================
 *  Private Data
 *============================================================================*/

#if 0 //-- 07.28 #if defined(ACCELEROMETER_PRESENT)
/* The following variables will be used while calculating (from 
 * the accelerometer) whether the remote is still being moved.
 */
static uint8 accelXHysterisis, accelYHysterisis, accelZHysterisis;
static uint8 accelLastX, accelLastY, accelLastZ;
#endif /* ACCELEROMETER_PRESENT */

uint8 gsensorWatchdog; //-- to detect gsensor is working or not

/*=============================================================================
 *  Private Function Prototypes
 *============================================================================*/
#if 0 //-- 07.28    
/*-----------------------------------------------------------------------------*
 *  NAME
 *      isSignificantlyDifferent
 *
 *  DESCRIPTION
 *      Decide whether the "old" and "new" readings are significantly 
 *      different.
 *
 *  RETURNS/MODIFIES
 *      TRUE: more than 10% difference between "old" and "new"
 *
 *----------------------------------------------------------------------------*/
 static bool isSignificantlyDifferent(int8 old, int8 new)
{
    /* We are checking the MSB of the accelerometer/gyroscope readings.
     * If the change is more than 2-points, call it significant; otherwise,
     * the change is not significant.
     */
#if defined(ACCELEROMETER_PRESENT)
#if defined(GYROSCOPE_PRESENT)
    int8 range = 2;
#else /* GYROSCOPE_PRESENT */
    int8 range = 2;     /* Only accelerometer is defined */
#endif /* GYROSCOPE_PRESENT */
#else
    int8 range = 1;     /* Only gyroscope is defined */
#endif /* ACCELEROMETER_PRESENT */
    
     /* TODO: it may be necessary to put in a more sophisticated
      * algorithm here, depending on the remote physical design.
      */
    
    /* If only 10% different, return "no difference" */
    return ((new < (old - range)) ||
            (new > (old + range)));
}
#endif
 
#if 0 //-- 07.28
/*-----------------------------------------------------------------------------*
 *  NAME
 *      checkForMotion
 *
 *  DESCRIPTION
 *      The following code block implements the logic where the application 
 *      tries to determine whether the user had kept the remote control stable 
 *      (in which case we could shut-down to save power), or whether it is still 
 *      moving.
 *
 *  RETURNS/MODIFIES
 *      TRUE: remote is still moving, FALSE: remote is stable
 *
 *----------------------------------------------------------------------------*/
static bool checkForMotion(DEVICE_MEM_TYPE device, uint8 newX, uint8 newY, uint8 newZ)
{
    bool isStatic;
    uint8 *lastX, *lastY, *lastZ;
    uint8 *xHyst, *yHyst, *zHyst;
    uint8 stabilityCount;
    
#if defined(ACCELEROMETER_PRESENT)
    if(device == IS_ACCELEROMETER)
    {
        lastX = &accelLastX;
        lastY = &accelLastY;
        lastZ = &accelLastZ;
        xHyst = &accelXHysterisis;
        yHyst = &accelYHysterisis;
        zHyst = &accelZHysterisis;
        stabilityCount = ACCELEROMETER_STABILITY_COUNT;
    }
#endif /* ACCELEROMETER_PRESENT */

    /* For every axis, see if the data is same as the last read 
     * data. If it is same as the last read data, increment a counter;
     * if it is not same as the last read data, reset the counter.
     *
     * If, for any axis, the counter goes above a pre-set value, we 
     * will assume that the user has kept the remote stable.
     */

    /* Check the x-axis data */
    (*xHyst) = (isSignificantlyDifferent((*lastX), newX)) ? 0 : ((*xHyst)+1);
    (*lastX) = newX;
        
    /* Check the y-axis data */
    (*yHyst) = (isSignificantlyDifferent((*lastY), newY)) ? 0 : ((*yHyst)+1);
    (*lastY) = newY;
        
    /* Check the z-axis data */
    (*zHyst) = (isSignificantlyDifferent((*lastZ), newZ)) ? 0 : ((*zHyst)+1);
    (*lastZ) = newZ;
        
    /* If any one counter has reached the maximum preset value,
     * we will assume that the user has put down the remote control.
     */
    isStatic = (((*xHyst) >= stabilityCount) &&
                ((*yHyst) >= stabilityCount) &&
                ((*zHyst) >= stabilityCount) );
    
    return (isStatic == 0);
}
#endif

/*=============================================================================
 *  Public Function Implementations
 *============================================================================*/
 
/*-----------------------------------------------------------------------------*
 *  NAME
 *      motionInit
 *
 *  DESCRIPTION
 *      Initialise the appropriate device derivers for motion sensing.
 *
 *  RETURNS/MODIFIES
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
extern void motionInit(void)
{
    gsensorWatchdog=0;
    
    /* Initialise the accelerometer. */
    (void)AccelInit();
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      motionHandleInterrupt
 *
 *  DESCRIPTION
 *      Handle motion-related interrupts.
 *
 *  RETURNS/MODIFIES
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
#include "accel_mma8653fc.h"
#include "registers_accel_mma8653fc.h"
#include "i2c_comms.h"
#include "ht_hw.h"
uint8 aIntSource, aEventSRC;
bool aPstatus;

//--mthis function will be called every minute, if watchdog > 0, something wrong happens
extern void motionSensorWatchdog(void)
{
    gsensorWatchdog++;

    if(gsensorWatchdog>1)   //-- more than 1 minute no input from gsensor, something wrong, reset it.
    {
        motionInit();
        //-- no need of this, motioInit included it already, motionConfigureActiveMode();
    }
}


extern void motionSleep(void)
{    
    i2cUsePeripheralBus();
#ifdef CONFIG_FREEFALL_DETECTION    //-- 150510
    // if for freefall detection, we won't allow gsensor go to sleep, 
    // on the other hand, the gsensor must be set earlier to detect freefall
    mma8653Wakeup();    // reset it, just in case
#else           
    mma8653EnterSleep();
#endif
    i2cUseMainBus();
}    

extern void motionWakeup(void)
{    
    i2cUsePeripheralBus();           
    mma8653Wakeup();
    i2cUseMainBus();                        
}

uint8 pulseSource;
bool hadShakeEvent;

uint16 shakeWaitCounterFromLastWake;

#ifdef CONFIG_FREEFALL_DETECTION

#ifdef CONFIG_DEBUG_OUTPUT
#include <gatt.h>
#include "app_gatt_db.h"
#endif

timer_id ffMonitoringTimer_tid;
timer_id ffMonitoringPhase2Timer_tid;
timer_id ffMonitoringPhase3Timer_tid;
timer_id ffMonitoringPhase4Timer_tid;

uint32 ffMonitoringStartTime;
uint32 ffAccumulatedTime;
 
//-- if arriving here, means already passed the silence period, of last phase of free fall detection
static void ffMonitoringPhase4TimeoutHandler(timer_id tid)
{
    if(ffMonitoringPhase4Timer_tid!=tid) return;
    ffMonitoringPhase4Timer_tid=TIMER_INVALID; 
    
    atDeviceEvent(AT_DEVICE_EVENT_FREEDROP, TRUE); // trigger beacon
}

// called after wait for 5 seconds from strong hit, to start silence check
static void ffMonitoringPhase3TimeoutHandler(timer_id tid)
{
    if(ffMonitoringPhase3Timer_tid!=tid) return;
    ffMonitoringPhase3Timer_tid=TIMER_INVALID;
    ffMonitoringPhase4Timer_tid=TimerCreate((5*SECOND), TRUE, ffMonitoringPhase4TimeoutHandler);
}

static void ffMonitoringPhase2TimeoutHandler(timer_id tid)
{
    if(ffMonitoringPhase2Timer_tid!=tid) return;
    ffMonitoringPhase2Timer_tid=TIMER_INVALID;  // reset at the same time
}

/*
  
static void ffTimerFree(timer_id *tid)
{
    if(tid !=TIMER_INVALID){
        TimerDelete(*tid);
        *tid=TIMER_INVALID;
    }
}
        
static void ffMonitoringTimerReset(void)
{
    ffTimerFree(&ffMonitoringTimer_tid); 
    ffTimerFree(&ffMonitoringPhase2Timer_tid);   
    ffTimerFree(&ffMonitoringPhase3Timer_tid);      
    ffTimerFree(&ffMonitoringPhase4Timer_tid);      

}
*/

//-- called by every time gsensor data is generated, to keep tracking the freefall status

void ffDetectingByStrength(uint32 strength)
{
    if(ffMonitoringPhase2Timer_tid!=TIMER_INVALID){ // wait for strong hit, vor over 1 second
        if(strength > 0x18000){ // found a strong hit
            atDeviceEvent(AT_DEVICE_EVENT_FREEDROP, TRUE); // 150608 for test, trigger beacon

            if(0 && ffMonitoringPhase3Timer_tid==TIMER_INVALID){  // if not yet created
                ffMonitoringPhase3Timer_tid=TimerCreate((2*SECOND), TRUE, ffMonitoringPhase3TimeoutHandler);
                //-- after 2 seconds, to start check for silence period
            }

            TimerDelete(ffMonitoringPhase2Timer_tid); // stop this phase
            ffMonitoringPhase2Timer_tid=TIMER_INVALID;
        }     
    } else if(ffMonitoringPhase3Timer_tid!=TIMER_INVALID){
        // do nothing, wait for stablize
    } else if(ffMonitoringPhase4Timer_tid!=TIMER_INVALID){
        if(strength>100){   // abort detection, if stronger movement found
            TimerDelete(ffMonitoringPhase4Timer_tid);
            ffMonitoringPhase4Timer_tid=TIMER_INVALID;
        }
    }
 
}

static void ffMonitoringTimeoutHandler(timer_id tid)
{
    if(ffMonitoringTimer_tid!=tid) return;
    ffMonitoringTimer_tid=TIMER_INVALID;    // reach here, to reset 
        
}

static void ffDetected(void)
{
    uint32 currentTime;
    
    //-- uiDisplayShow(FALSE);   //-- for test
            
    currentTime=TimeGet32();
     
    if(ffMonitoringTimer_tid==TIMER_INVALID){
        ffMonitoringStartTime=currentTime;
        ffAccumulatedTime=0;
    } else {
        TimerDelete(ffMonitoringTimer_tid); 
        ffMonitoringTimer_tid=TIMER_INVALID;
    }
    ffAccumulatedTime=currentTime-ffMonitoringStartTime;
    if(ffAccumulatedTime>=250){  // 400, wait long enough 
        //-- ffMonitoringTimerReset();   // start a new session, to reset all timers
        if(ffMonitoringPhase2Timer_tid!=TIMER_INVALID){ 
            TimerDelete(ffMonitoringPhase2Timer_tid);
        }
        //-- wait for strong hit
        ffMonitoringPhase2Timer_tid=TimerCreate((1*SECOND), TRUE, ffMonitoringPhase2TimeoutHandler);
        
        //-- the timer needs to be freed soon as possible
        TimerDelete(ffMonitoringTimer_tid); 
        ffMonitoringTimer_tid=TIMER_INVALID;

        //-- atDeviceEvent(AT_DEVICE_EVENT_FREEDROP, TRUE); // for test, trigger beacon
    }else{
        ffMonitoringTimer_tid=TimerCreate((150*MILLISECOND), TRUE, ffMonitoringTimeoutHandler); 
                                                                        // continue wait for successive freefall
    }
}

void ffInit(void)
{
    ffMonitoringTimer_tid=TIMER_INVALID;
    ffMonitoringPhase2Timer_tid=TIMER_INVALID;
    ffMonitoringPhase3Timer_tid=TIMER_INVALID;
    ffMonitoringPhase4Timer_tid=TIMER_INVALID;
}

#endif

extern void motionHandleInterrupt(uint32 pio)
{
    uint8 intReason;
    uint8 newMode;
    //-- uint8 pulseSource;
    uint8 motionSource; //-- 10.08

    bool hasSensorDataRead=FALSE;
    
    if(gUILocked==TRUE){        //-- 10.09
        AccelClearInterrupt();
        return;
    }
    
    i2cUsePeripheralBus(); 
    i2cReadRegister(MMA8653FC_WRITE_ADDR, MMA8653FC_INT_SOURCE, &intReason); 
    
    i2cReadRegister(MMA8653FC_WRITE_ADDR, MMA8653FC_FF_MT_SRC, &motionSource);  //-- 10.08

    i2cUseMainBus();

    aIntSource=intReason; //-- for debugging 
   
    
    hadShakeEvent=FALSE;    // initial setting
    
#ifdef CONFIG_SHAKE_WAKE_TOUCH
  
    if((motionSource & MMA8653FC_FF_MT_SRC_EA)!=0)  //-- 0x80, Event Active Flag
    {
      if(shakeWaitCounterFromLastWake<5)
      {
        shakeWaitCounterFromLastWake=0;
      } 
      else if(shakeWaitCounterFromLastWake>9)
      {
        hadShakeEvent=TRUE;
        shakeWaitCounterFromLastWake=0;
        
        if(gGsensorMode==GSENSOR_WAKE) // only wake touch if already g-sensor woke
        {
#ifdef CONFIG_TARGET_BINSX2 // 10.12                 
                if(touchSensorStatus()==FALSE) // if the sensor is sleeping
                {
                    // only disable motion interrupt if touch going to be waken up.            
                    //-- 10.08 2 mma8653MotionDetectStart(FALSE); 

                    touchPowerdown(FALSE);  //-- Wake up, 07.13 it is required.

                    if((gConfiguration & BINS_CONFIG_HRD_ALWAYS_ON_MASK)==0)
                    {
                        int waitTime= TOUCH_WAKE_WITH_NOACTION;
                        if(gShakeThreshold>=5) waitTime++;  // 09.30 wait longer seconds, if strengh high
                        touchSessionEnd(waitTime);                       
                    }
                }
#ifdef CONFIG_UI_GESTURE_TRACK      
            gestureTrackStart();                
#endif
            
#else       // 10.12 if NOT BINSX2
            uiShowNewCycle(); 
#endif                
        }
      }
    }    
#endif  //-- CONFIG_SHAKE_WAKE_TOUCH    

#ifdef CONFIG_FREEFALL_DETECTION
#ifdef CONFIG_IBEACON
    if((motionSource & MMA8653FC_FF_MT_SRC_EA)!=0)  //-- 0x80, Event Active Flag
    {
        // int eventAxisCount=0;
        
        ffDetected();
        
        //-- if(motionSource & MMA8653FC_FF_MT_SRC_XHE) eventAxisCount++;
        //-- if(motionSource & MMA8653FC_FF_MT_SRC_YHE) eventAxisCount++;
        //-- if(motionSource & MMA8653FC_FF_MT_SRC_ZHE) eventAxisCount++;
        
#ifdef CONFIG_DEBUG_OUTPUT
    
    if(g_ht_data.state==app_state_connected)
    {
        unsigned char  gVar[8];
        unsigned char  *p_value;
        uint16 x,y,z;
        x=y=z=0;

        p_value=gVar;

        if(motionSource & MMA8653FC_FF_MT_SRC_XHE) x=1;
        if(motionSource & MMA8653FC_FF_MT_SRC_YHE) y=1;
        if(motionSource & MMA8653FC_FF_MT_SRC_ZHE) z=1;

        
        ShortToByteArray(p_value,   (x));  // 10 bits to 16 bits
        ShortToByteArray(p_value+2, (y));
        ShortToByteArray(p_value+4, (z));  
    
        GattCharValueNotification(g_ht_data.st_ucid, 
                              HANDLE_AT_DEBUG_OUTPUT, 
                              6, 
                              gVar);
 
    }
#endif
        
        
        // if(eventAxisCount>=3){
        //    atDeviceEvent(AT_DEVICE_EVENT_FREEDROP, TRUE);
        // }
    }
#endif    
#endif // CONFIG_FREEFALL_DETECTION
    
    
    if(gGsensorMode==0x01) shakeWaitCounterFromLastWake++;
        
    i2cUsePeripheralBus();
    i2cReadRegister(MMA8653FC_WRITE_ADDR, MMA8653FC_SYSMOD, &newMode);
    i2cUseMainBus();
    
    if(newMode!=gGsensorMode)
    {
        gGsensorMode= 0x03 & newMode;
        
        if(gGsensorMode==GSENSOR_SLEEP)
            pedometerSleep(FALSE);
        else{
            pedometerSleep(TRUE);   // inform to update as a wakeup state.
            motionWakeup();     // 2015.01.31 moved up from SHAKE_WAKE_TOUCHs
        }
            
#ifdef CONFIG_SHAKE_WAKE_TOUCH        
        if(gGsensorMode==GSENSOR_SLEEP)  //-- Sleep mode
        {
            shakeWaitCounterFromLastWake=0;
        }
        else    // gsensor wokeup 
        {
            shakeWaitCounterFromLastWake=18;    // value big enough to check 
        } 
        
#endif  //-- CONFIG_SHAKE_WAKE_TOUCH
        
    }
    
    // only process step check if shake event is not occurred.
    // Prevent not take too much time for one interrupt.
    
    if(hadShakeEvent==FALSE &&
       newMode==gGsensorMode &&
       (intReason & MMA8653FC_INT_SRC_DRDY)!=0) /* 0x01, SRC_DRDY */
    {   
        gsensorWatchdog=0;  // reset the watchdog, eat least 1 data input per every second

                                    //-- 06.24 if touched, stop check steps
                                    //-- if(gGsensorMode==0x01 && touchPressed()==FALSE) //-- only call it if active /wake
#ifdef CONFIG_SLEEP_TRACKING  // 150510
        motionCreateNextReport();
        hasSensorDataRead=TRUE;
#else        
        if(gGsensorMode==GSENSOR_WAKE) //-- only call it if active /wake
        {
            motionCreateNextReport();
            hasSensorDataRead=TRUE;
        }
#endif
    }      
    
    //-- 10.09, save for redundant, 
    if(hasSensorDataRead==FALSE){
        AccelClearInterrupt();
    }
}

#if 0 //-- 07.28 
#if defined(ACCELEROMETER_PRESENT)
/*-----------------------------------------------------------------------------*
 *  NAME
 *      motionResetAccelerometerStabilityVariables
 *
 *  DESCRIPTION
 *      This function resets the remote accelerometer stability variables
 *
 *  RETURNS/MODIFIES
 *      Nothing.
 *
 *----------------------------------------------------------------------------*/
extern void motionResetAccelerometerStabilityVariables(void)
{
    accelXHysterisis = 0;
    accelYHysterisis = 0;
    accelZHysterisis = 0;
}
#endif /* ACCELEROMETER_PRESENT */
#endif 

/*-----------------------------------------------------------------------------*
 *  NAME
 *      motionCreateNextReport
 *
 *  DESCRIPTION
 *      This function formulates an input report and copies it into the buffer 
 *      pointed by the function arguement.
 *
 *  RETURNS/MODIFIES
 *      TRUE if data is present to be sent
 *
 *----------------------------------------------------------------------------*/
#include "pedometer.h"

extern MOTION_T motionCreateNextReport()
{    
    //-- 07.28 uint8 newDataAvailable = FALSE;
    uint16 x, y, z;
    MOTION_T retval = MOTION_WARM_UP;
    
    {
        /* Read the accelerometer data */
        if(AccelReadData(&x, &y, &z))
        {
            //-- 07.28 newDataAvailable += checkForMotion(IS_ACCELEROMETER, WORD_MSB(x), WORD_MSB(y), WORD_MSB(z));

            pedometerNewDataInput( x, y, z);  //--             

        }

#if 0 //-- 07.28
        if(newDataAvailable == FALSE)
        {
            /* Reset the stability variables */
            motionResetAccelerometerStabilityVariables();
        }
        
        retval = (newDataAvailable) ? MOTION_NEW_DATA : MOTION_NO_DATA;
#endif
        
    }

    
    return retval;    
}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      motionConfigureActiveMode
 *
 *  DESCRIPTION
 *      This function configures the acceleromter and/or gyroscope correctly
 *      for the RC being in ACTIVE mode.
 *
 *  RETURNS/MODIFIES
 *      None
 *
 *----------------------------------------------------------------------------*/
extern void motionConfigureActiveMode(void)
{

    /* The application may have put the accelerometer in shallow sleep to save 
     * power. Exit the shallow sleep (return to full power).
     */
    AccelEnableDataCollection(TRUE);

#if defined(ACCELEROMETER_INTERRUPT_PIO)
    /* Accelerometer interrupts are not required in active state.
     * Disable interrupts; the application will re-enable them while exiting 
     * the active mode
     */
    AccelEnableInterruptsIntoXap(FALSE);

#endif /* ACCELEROMETER_INTERRUPT_PIO */

}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      motionSetLowPowerMode
 *
 *  DESCRIPTION
 *      This function configures the acceleromter and/or gyroscope correctly
 *      for the RC being in IDLE mode.
 *
 *  RETURNS/MODIFIES
 *      None
 *
 *----------------------------------------------------------------------------*/
extern void motionSetLowPowerMode(void)
{
    AccelEnableShallowSleepWithInterrupts();
}

#endif /* ACCELEROMETER_PRESENT || GYROSCOPE_PRESENT */
#endif /* !REMOTE_IS_AIRMOUSE */

#include "accel_mma8653fc.h"

extern bool motionSlow()
{
    return mma8653fcInSleepMode();
}