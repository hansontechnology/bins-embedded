/*******************************************************************************
 *    Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 * FILE
 *    accelerometer.c
 *
 *  DESCRIPTION
 *    This file implements the device driver for a generic accelerometer.
 *
 ******************************************************************************/

#include "configuration.h"

#if defined(ACCELEROMETER_PRESENT)

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <types.h>
#include <pio.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/
#include "accelerometer.h"
#include "i2c_comms.h"

#include "accel_mma8653fc.h"


/*============================================================================*
 *  Private Definitions
 *============================================================================*/
#define CONFIGURE_I2C_BUS() i2cUsePeripheralBus()
 
/*=============================================================================
 *  Public Function Implementations
 *============================================================================*/
/*----------------------------------------------------------------------------*
 *  NAME
 *      AccelInit
 *  DESCRIPTION
 *      This function initialises the accelerometer. This needs to be called only
 *      once.
 *  RETURNS
 *      TRUE: the accelerometer appears to be working
 *---------------------------------------------------------------------------*/
extern bool AccelInit(void)
{
    bool result;
    
    i2cUsePeripheralBus();
    
    result=mma8653fcInit();
    
    i2cUseMainBus();
    
    return result;
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AccelEnableDataCollection
 *  DESCRIPTION
 *      Enable/disable full-rate data collection.
 *      Note that the codec may first require powering-on, if this is 
 *      applicable to the HW design (power-on is not currently handled by this 
 *      function).
 *  RETURNS
 *      None
 *---------------------------------------------------------------------------*/

//-- 150510 This function is called by motionConfigureActiveMode, and eventually by AppInit
extern void AccelEnableDataCollection(bool enable)
{
    i2cUsePeripheralBus();
     
    //-- mma8653fcEnableDataCollection(enable);
    //--** new trial, 1227 
    mma8653fcEnableLowPowerDataCollection(enable);

#ifdef CONFIG_FREEFALL_DETECTION  //-- 150510
    mma8653Wakeup();    // reset it to the right status for freefall detection
#endif
    
    i2cUseMainBus();
}

extern void AccelClearInterrupt(void)
{
    i2cUsePeripheralBus();
 
    mma8653fcClearInterrupt();
    
    i2cUseMainBus();
}

/*----------------------------------------------------------------------------*
 *  NAME
 *      AccelReadData
 *  DESCRIPTION
 *      Read X,Y,Z data from the accelerometer.
 *  RETURNS
 *      None
 *---------------------------------------------------------------------------*/
extern bool AccelReadData(uint16 *x, uint16 *y, uint16 *z)
{
    bool result;
    
    i2cUsePeripheralBus();
    
    result=mma8653fcReadData(x, y, z);
    
    i2cUseMainBus();
    
    return result;
}

#endif /* ACCELEROMETER_PRESENT */