
#include <time.h>

#include "configuration.h"
#include "ht_hw.h"
#include "pedometer.h"
#include "activity_tracking_service.h"
#include "health_thermometer.h"
#include "i2c_comms.h"
#include "touch.h"
#include "display.h"
#include "accel_mma8653fc.h" 
#include "accelerometer.h"
/*
bool gIsTouchHeartRateQueued;
*/
timer_id touchPeriodicTimer_tid;
timer_id touchHeartRatePollTimer_tid;
timer_id touchHeartRateModeCheckTimer_tid;
timer_id touchSleepTimer_tid;


bool isTouchNewSession;
uint8 frame_count;

#define TOUCH_MODE_SLEEP1 0xBA
#define TOUCH_MODE_SLEEP2 0xBC
#define TOUCH_MODE_SLEEP  (((gConfiguration & BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK)!=0) ? TOUCH_MODE_SLEEP1 : TOUCH_MODE_SLEEP2) 

#define CONFIG_TOUCH_SLEEP2     // power saving mode
#define TOUCH_HEARTRATE_READ_TRY_MAX  400 // 150603 for slower update 300 //-- 08.06 725  //-- 30 seconds = 25 times / sec * 30 = 725
uint16 gTouchHeartRateReadCounter;

uint32 gTouchSleepTime;

bool gIsTouchLocked;   //-- 08.20
bool gHasNewHeartRateStart;
static bool gToDisconnectAfterCycle; //-- 150712 
static timer_id pulseBatchCycleTimer_tid;
        
#ifndef CONFIG_TARGET_BINSZ3    // 12.22 no need for Z3
//-- 09.19
static bool touchAlertIsOn; 
                        // 10.05 flag to set when the touch alert event is set, reset when the alert event is cleared
static timer_id touchAlertTimer_tid;
static bool hadTouchAlertSet;   // 10.05 flag to set when the touch (actually a timer) is on becuase of an alert event.

#endif

#define TOUCH_HEARTRATE_READ_INTERVAL  (40*MILLISECOND) //-- (50*MILLISECOND)
#define TOUCH_HEARTRATE_UI_UPDATE_INTERVAL (30)

static void touchTimerReset(void);
static void touchSessionEndToDisconnect(uint8 tiimeWaitBeforeDown);

static void hal_pixart_writeRegister(uint8 reg, uint8 data)
{
    i2cWriteRegister(TOUCH_I2C_WRITE_ADDR, reg, data);   
}


static uint8 hal_pixart_readRegister(uint8 reg)
{
    uint8 data;
    bool result;
    result=i2cReadRegister(TOUCH_I2C_WRITE_ADDR, reg, &data);
    
    if(result!=TRUE) 
        return 0;
    
    return data;
}

static bool hal_pixart_readRegisters(uint8 reg, uint8 *data, uint8 len)
{
    bool result;
    result=i2cReadRegisters(TOUCH_I2C_WRITE_ADDR, reg, len, data);
    
    if(result!=TRUE) 
        return FALSE;
    
    return TRUE;
}

uint32 touchStartTime;

static bool touchReadData(uint8 *hrd)
{
    uint8 isDataReady;

    i2cUsePeripheralBus();
    
    hal_pixart_writeRegister(0x7F, 0x01);    // bank1
    isDataReady=hal_pixart_readRegister(0x68)&0x0f;    // check ready status
                                                       // 0 is not ready
    if(isDataReady==0){ 
        hal_pixart_writeRegister(0x7F, 0x00);    // bank0
        
        i2cUseMainBus();
        return FALSE;
    }    
    
    hrd[0]=isDataReady;
    
    // Burst read for I2C
    hal_pixart_readRegisters(0x64,hrd+1, 4);
    hal_pixart_readRegisters(0x1A,hrd+5, 3);
    hrd[8]=frame_count++;
    {
        uint32 timeNow=TimeGet32();
        uint32 timeDiff;
        uint8 timeDiffByte;
        if(timeNow>touchStartTime) timeDiff=timeNow-touchStartTime;
        else{
            timeDiff=timeNow+(0xffffffffL-touchStartTime);
        }
        touchStartTime=timeNow;
        
        timeDiff=timeDiff/1000;  // us to ms;
        timeDiffByte=timeDiff;
        hrd[9]=timeDiffByte;  // 50 ms
        
    }    
    hrd[10]=0;

    hal_pixart_writeRegister(0x7F, 0x00);    // bank0

    hrd[11]=(hal_pixart_readRegister(0x59)&0x80); // check touch flag
    hrd[12]=hrd[6];  
    
    i2cUseMainBus();
    return TRUE;
} 

#if 0
//-- volatile uint16 aFound;

static void touchPeriodicTimerHandler(timer_id tid)
{
    uint16 touchStatus=0;

    if(touchPeriodicTimer_tid!=tid) return;
    
    touchPeriodicTimer_tid=TIMER_INVALID;
    
    i2cUsePeripheralBus();
    
    hal_pixart_writeRegister(0x7F, 0);    // bank0
    touchStatus=hal_pixart_readRegister(0x59)&0x80;    // Reset
    
    
    if(touchStatus==0x80){  // touched
        // displayChar(DISPLAY_FONT_INDEX_STEP);
        // uiDisplayNumber(123, 3);
        uiDisplayStart();
    }
    else{
    }

    //-- aFound=tmp;
    
    i2cUseMainBus();

    touchPeriodicTimer_tid=TimerCreate((1*SECOND), TRUE, touchPeriodicTimerHandler);
        
#if 0   
    sum=0;
    sum+=hal_pixart_readRegister(0x64)&0xff;    // Reset
    sum+=hal_pixart_readRegister(0x65)&0xff;    // Reset
    sum+=hal_pixart_readRegister(0x66)&0xff;    // Reset
    sum+=hal_pixart_readRegister(0x67)&0xff;    // Reset
    sum+=hal_pixart_readRegister(0x1A)&0xff;    // Reset
    sum+=hal_pixart_readRegister(0x1B)&0xff;    // Reset
    sum+=hal_pixart_readRegister(0x1C)&0xff;    // Reset

    uiDisplayNumber(sum, 5);
#endif

}

#endif

extern bool gSuccess;

static void touchInitSetting(void)
{
   uint8 temp;
   bool result;

   i2cUsePeripheralBus();
        
   hal_pixart_writeRegister(0x06, 0x82);    // Reset

   TimeWaitWithTimeout16(FALSE, (100*MILLISECOND), result); 
   //-- frameDelay(); // about 10ms
    
   hal_pixart_writeRegister(0x09, 0x5A);
   hal_pixart_writeRegister(0x05, 0x99);    //-- wakeup from sleep mode 
#ifdef CONFIG_TOUCH_SLEEP2
   hal_pixart_writeRegister(0x05, TOUCH_MODE_SLEEP);    // Force entering Sleep2
#endif
   
   temp= hal_pixart_readRegister(0x17);
   hal_pixart_writeRegister(0x17, temp | 0x80);
   hal_pixart_writeRegister(0x27, 0xFF);
   hal_pixart_writeRegister(0x28, 0xFA);
   hal_pixart_writeRegister(0x29, 0x0A);
   hal_pixart_writeRegister(0x2A, 0xC8);
   hal_pixart_writeRegister(0x2B, 0xA0);
   hal_pixart_writeRegister(0x2C, 0x8C);
   hal_pixart_writeRegister(0x2D, 0x64);
   hal_pixart_writeRegister(0x42, 0x20);
   hal_pixart_writeRegister(0x4D, 0x18);
   hal_pixart_writeRegister(0x7A, 0xB5);
   hal_pixart_writeRegister(0x7F, 0x01);
   hal_pixart_writeRegister(0x07, 0x48);
   hal_pixart_writeRegister(0x2E, 0x48);
   hal_pixart_writeRegister(0x38, 0xE4);
   hal_pixart_writeRegister(0x42, 0xA4);
   hal_pixart_writeRegister(0x45, 0x3F);
   hal_pixart_writeRegister(0x46, 0x00);
   hal_pixart_writeRegister(0x52, 0x32);  //20Hz
   hal_pixart_writeRegister(0x53, 0x28);
   hal_pixart_writeRegister(0x56, 0x60);
   hal_pixart_writeRegister(0x57, 0x28);
   hal_pixart_writeRegister(0x6D, 0x02);
   hal_pixart_writeRegister(0x0F, 0xC8);
   hal_pixart_writeRegister(0x7F, 0x00);
   hal_pixart_writeRegister(0x5D, 0x81);
      
    
   i2cUseMainBus();
   
}



uint8 gCurrentHR;
uint32 gCurrentHRTime;

uint8 getCurrentHeartRate()
{
    return gCurrentHR;
}
void touchHeartRateUpdate(uint8 currentHR)
{
    gCurrentHR=currentHR;
    
    gTouchHeartRateReadCounter=0; //-- reset if received a valid HR value;
    gCurrentHRTime=TimeGet32();
}

uint8 getLatestHeartRate()
{
   
#if 1 
    uint32 timeNow, timeDiff;
    
    if(gCurrentHRTime==0) return 0;
    
    timeNow=TimeGet32();
    
    if(timeNow>=gCurrentHRTime) timeDiff=timeNow-gCurrentHRTime;
    else{ 
        timeDiff=timeNow+(0xffffffffL - gCurrentHRTime);
    }
     
    //-- TODO 07.08, wrong if included
    if(timeDiff<(5*SECOND)){ //-- 2015.01.31 from 20*SECOND
        return gCurrentHR;
    }
    else gCurrentHRTime=0;  // reset it
#else
    return gCurrentHR;    
#endif
    
    return 0;
}

bool isTouchPowerDown;

void touchPowerdown(bool onoff)
{
    i2cUsePeripheralBus();
    
    hal_pixart_writeRegister(0x7F, 0x00);    // bank0   
    if(onoff==TRUE){   
        if(gIsTouchLocked==FALSE){ //-- 08.20, if locked then do not wakeup
            hal_pixart_writeRegister(0x06, 0x0A);    // Force to power down
            // PioSet(TOUCH_PD_PIO, TRUE);    // enter power down mode
        }
    }
    else{
        // PioSet(TOUCH_PD_PIO, FALSE);    // leave power down mode 
        hal_pixart_writeRegister(0x06, 0x00);    // Leave power down
#ifdef CONFIG_TOUCH_SLEEP2
        hal_pixart_writeRegister(0x05, TOUCH_MODE_SLEEP);    // Force entering Sleep2
#endif
#ifndef CONFIG_VIBRATION  // 12.22      
        hadTouchAlertSet=FALSE; //-- 09.19
#endif
    }   

    isTouchPowerDown=onoff;
    
    i2cUseMainBus();

}

#ifndef CONFIG_TARGET_BINSZ3    // 12.22 no need from Z3

extern bool touchAlertIsRunning(void)
{
    return hadTouchAlertSet;
}

static void touchAlertTimerHandler(timer_id tid)
{ 
    uint8 nextTimerInSecond=5;
    
    if(touchAlertTimer_tid!=tid) return;

    i2cUsePeripheralBus();
    
    hal_pixart_writeRegister(0x7F, 0x00);    // bank0   

    if(touchAlertIsOn==FALSE){
        touchAlertIsOn=TRUE;
        if(touchSensorStatus()==FALSE){
            hal_pixart_writeRegister(0x06, 0x00);    // Leave from power down, means power on
            hal_pixart_writeRegister(0x05, TOUCH_MODE_SLEEP2);  
            nextTimerInSecond=1;
            hadTouchAlertSet=TRUE;  // set to busy, and need to turn power down next time.
        }   
        else{
            hadTouchAlertSet=FALSE; // not-busy, means will not need to power it down later.
        }
    }
    else{   // if previously it was ON,then it should be off this time
        touchAlertIsOn=FALSE;
        if(hadTouchAlertSet==TRUE){
            hal_pixart_writeRegister(0x06, 0x0A);    // Force to power down
        }
        hadTouchAlertSet=FALSE; //-- 10.5, not-busy always for while it is off.
    }
    i2cUseMainBus();
    
    touchAlertTimer_tid=TimerCreate((nextTimerInSecond*SECOND), 
                                        TRUE, touchAlertTimerHandler);        
}

void touchAlert(bool onoff)
{  
    if(touchAlertTimer_tid!=TIMER_INVALID){
        TimerDelete(touchAlertTimer_tid);        
        touchAlertTimer_tid=TIMER_INVALID;
    } 
    
    if(onoff==TRUE){    
        touchAlertIsOn=FALSE;
        touchAlertTimerHandler(touchAlertTimer_tid);        
    }        
    else{
        if(touchAlertIsOn==TRUE){
            touchAlertTimerHandler(touchAlertTimer_tid);
                    
            TimerDelete(touchAlertTimer_tid);   //-- 10.05 clean it up again
            touchAlertTimer_tid=TIMER_INVALID;
        }
    }
        
}

#endif

void touchHeartRateStartAll(void)
{    
    touchPowerdown(FALSE);   // power on touch sensor if not
    gHasNewHeartRateStart=TRUE;
    touchNewSession();
}
        
void touchInit(void)
{
    
    PioSetMode(TOUCH_RESET_PIO, pio_mode_user);
    PioSetDir(TOUCH_RESET_PIO, TRUE); // TRUE means OUTPUT
    PioSet(TOUCH_RESET_PIO, TRUE);  // TRUE, leave reset mode
    
    PioSetMode(TOUCH_PD_PIO, pio_mode_user);
    PioSetDir(TOUCH_PD_PIO, TRUE); // TRUE means OUTPUT
    PioSet(TOUCH_PD_PIO, FALSE);    // FALSE: leave power down mode
           
    {
        bool result;
        TimeWaitWithTimeout16(FALSE, (100*MILLISECOND), result);    
    }
    
    touchInitSetting();    
    
    {
        bool result;
        TimeWaitWithTimeout16(FALSE, (100*MILLISECOND), result);    
    }
    
    PioSetMode(TOUCH_INT_PIO, pio_mode_user);
    PioSetDir(TOUCH_INT_PIO, FALSE); // FALSE means INPUT
    PioSetPullModes(TOUCH_INT_PIO_MASK, pio_mode_strong_pull_down);
    
    PioSetEventMask(TOUCH_INT_PIO_MASK, pio_event_mode_both);
         
    isTouchNewSession=FALSE;
    touchHeartRatePollTimer_tid=TIMER_INVALID;
    touchHeartRateModeCheckTimer_tid=TIMER_INVALID;       
        
    gCurrentHR=gCurrentHRTime=0;
    gTouchSleepTime=0;  //-- 07.30
    
    touchSleepTimer_tid=TIMER_INVALID;
    
    touchSessionEnd(TOUCH_WAKE_WITH_NOACTION);
    
    gIsTouchLocked=FALSE;   //-- 08.20
    gHasNewHeartRateStart=FALSE; // 11.27    
#ifndef CONFIG_VIBRATION    // 12.22
    touchAlertTimer_tid=TIMER_INVALID; //-- 09.19
#endif
    
    pulseBatchCycleTimer_tid=TIMER_INVALID; //-- 150919
}  
    
bool touchHeartRatePollRunning()
{
    if(touchHeartRatePollTimer_tid!=TIMER_INVALID) return TRUE;
    return FALSE;
    
}
static void touchHeartRatePollTimerHandler(timer_id tid)
{
    bool readSuccess;
    uint8 hrdData[25];
    
    if(tid!=touchHeartRatePollTimer_tid) return;
    touchHeartRatePollTimer_tid=TIMER_INVALID;
        
    //-- 150501
    if(g_ht_data.state!=app_state_connected){
        touchSessionEnd(TOUCH_WAKE_WITH_NOACTION);
        return; // stop reading, as disconnected 
    }        

#if 1
    
    if(gTouchHeartRateReadCounter>TOUCH_HEARTRATE_READ_TRY_MAX
        &&
      (gConfiguration & BINS_CONFIG_HRD_ALWAYS_ON_MASK)==0   //-- if not continuous sampling                                                    
        &&
       (gConfiguration & BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK)!=0 //-- and not enter sleep if not detected long time
      )
    {
        touchSessionEndToDisconnect(TOUCH_WAKE_WITH_NOACTION);  // 150712
        return; // not a true skin touch, and also not a sport continuous reading, ignore   
    }
    gTouchHeartRateReadCounter++;
    
#if 0 //-- 150603 found it will eat computing power, make pulse data delayed, remark it,    
    
    if(getLatestHeartRate()==0) //--  && gTouchHeartRateReadCounter>=TOUCH_HEARTRATE_UI_UPDATE_INTERVAL)         
        // 2015.01.31
        // if not yet received a valid pulse output, signal at least
        // Per around 1 second
    {
        if((gTouchHeartRateReadCounter%TOUCH_HEARTRATE_UI_UPDATE_INTERVAL)==0){  
            uiDisplayHeartRateProcessing(FALSE);            
        }
    }
    
#endif
    
#endif
    
    touchHeartRatePollTimer_tid=TimerCreate(TOUCH_HEARTRATE_READ_INTERVAL,TRUE, touchHeartRatePollTimerHandler);        
    
    readSuccess=touchReadData(hrdData);    
    
    //150325 Not necessary, 40 or 80ms to get the result.
    //if(readSuccess==FALSE){
    //    TimerDelete(touchHeartRatePollTimer_tid);
    //    touchHeartRatePollTimer_tid=TimerCreate((10*MILLISECOND),TRUE, touchHeartRatePollTimerHandler);
    //}

    if(readSuccess==TRUE){    
        if(g_ht_data.state==app_state_connected)
        {                        
#if 1        
            {
                uint16 x, y, z;
                uint16 sx, sy, sz;
                bool success;
                
                success=AccelReadData(&x, &y, &z);     // 12 bits data
              
                //-- 07.30
                if((gConfiguration & BINS_CONFIG_STEP_WITH_HRD_ENABLE_MASK)!=0){
                    if((frame_count&0x03)==0){                   
                        pedometerNewDataInput( x, y, z);  //--             
                    } 
                }
                
                // if(success!=TRUE) x=y=z=0x1234;

                //-- x=y=z=0x00;                
                
                sx= x<<4;  //-- 150325 12 bits to 16 bits
                sy= y<<4;
                sz= z<<4;
                
                // sx=x;
                // sy=y;
                // sz=z;
                
                // sx=signedForm(x);
                // sy=signedForm(y);
                // sz=signedForm(z);
                
                
                ShortToByteArray(hrdData+13, (sx));  // 10 bits to 16 bits
                ShortToByteArray(hrdData+15, (sy));
                ShortToByteArray(hrdData+17, (sz));  
                               
            }                                    
#else
            if(gsensorIsAwake()==TRUE)
            {
                
                GsensorData *gsensorNow;
                gsensorNow=gsensorDataGet(0);


                if(gsensorNow!=NULL)
                {
                    ShortToByteArray(hrdData+13, (gsensorNow->x<<4));  // 12 bit to 16 bit
                    ShortToByteArray(hrdData+15, (gsensorNow->y<<4));
                    ShortToByteArray(hrdData+17, (gsensorNow->z<<4));                   
                }
            }
            else
            {
                //-- initialization
                int i; 
                for(i=0; i<6; i++) hrdData[13+i]=0;
            }            

#endif
            heartRateRawDataNotify(g_ht_data.st_ucid, hrdData);       
        }
    }    
 
}  

void touchHeartRateStartMeasure(void)
{        
        if(touchHeartRatePollTimer_tid!=TIMER_INVALID){
            TimerDelete(touchHeartRatePollTimer_tid);
            touchHeartRatePollTimer_tid=TIMER_INVALID;
        }
        gTouchHeartRateReadCounter=0;
/*        
        gIsTouchHeartRateQueued=FALSE;  //-- 07.24
*/
        
        i2cUsePeripheralBus();
        hal_pixart_writeRegister(0x7F, 0x00);    // bank0  
        hal_pixart_writeRegister(0x05, 0x99);    // Force wakeup from sleep2 mode
        
        mma8653DataNotifyEnable(FALSE);
        i2cUseMainBus();
        
        touchStartTime=TimeGet32();
    	frame_count=0;       
        touchHeartRatePollTimer_tid=TimerCreate(TOUCH_HEARTRATE_READ_INTERVAL,TRUE, touchHeartRatePollTimerHandler);      
        
        uiDisplayHeartRateActive();  //-- after poll timer enabled 
        
        if(pulseBatchCycleTimer_tid==TIMER_INVALID){ //-- 150919 only if batch cycle is not running, then reset it
            gToDisconnectAfterCycle=FALSE;  // 150712 reset to false as default
        }
}
/*
void touchRestartHeartRateReading(void)
{
    if(touchHeartRatePollTimer_tid!=TIMER_INVALID) return;  // do nothing
    if(touchHeartRateModeCheckTimer_tid!=TIMER_INVALID) return;
    
    if(g_ht_data.state==app_state_connected && 
       (gConfiguration & BINS_CONFIG_HEARTRATE_ENABLE_MASK)!=0)
    {
        touchHeartRateStartMeasure();
    }
}
*/
static void touchHeartRateModeTimerHandler(timer_id tid)
{
    bool isDisplayOn=TRUE;
    if(touchHeartRateModeCheckTimer_tid!=tid) return;
    
    isTouchNewSession=FALSE; 
    
    if(g_ht_data.state==app_state_connected && 
       (    (uiDisplayCurrentMode(&isDisplayOn)==UI_DISPLAY_MODE_HRD) // 11.26, if in Heart Rate detection UI mode,  
            ||
            gHasNewHeartRateStart==TRUE            // 11.27 if it is from the phone request, do it one time
       )                                    &&     
       (gConfiguration & BINS_CONFIG_HEARTRATE_ENABLE_MASK)!=0){
        
        gHasNewHeartRateStart=FALSE;    // reset it
        touchHeartRateStartMeasure();
        
    }
    else{
        touchSessionEnd(TOUCH_WAKE_PERIOD);
    }    
}


static void touchSleepTimerHandler(timer_id tid)
{
    if(touchSleepTimer_tid!=tid) return;
    
    touchSleepTimer_tid=TIMER_INVALID;
    touchPowerdown(TRUE);

/* 10.08    
    i2cUsePeripheralBus();
    mma8653MotionDetectStart(TRUE);
    i2cUseMainBus();
*/    
    gTouchSleepTime=TimeGet32();
    
}

bool touchFastSwitch()
{
    return isTouchNewSession;
}

static void touchSessionEndToDisconnect(uint8 timeWaitBeforeDown)
{
    touchSessionEnd(timeWaitBeforeDown);
    if(gToDisconnectAfterCycle==TRUE){
        atDisconnectByDevice();
        gToDisconnectAfterCycle=FALSE;
        uiDisplayShow(TRUE);    // refresh the heart rate screen
    }
}
        
void touchSessionEnd(uint8 timeWaitBeforeDown)
{
    isTouchNewSession=FALSE;
    if(touchHeartRatePollTimer_tid!=TIMER_INVALID){   
        TimerDelete(touchHeartRatePollTimer_tid);        
        touchHeartRatePollTimer_tid=TIMER_INVALID;
    }
    if(touchHeartRateModeCheckTimer_tid!=TIMER_INVALID){
        TimerDelete(touchHeartRateModeCheckTimer_tid);
        touchHeartRateModeCheckTimer_tid=TIMER_INVALID;
    }
    if(touchHeartRateModeCheckTimer_tid!=TIMER_INVALID){    //-- 11.26 
        TimerDelete(touchHeartRateModeCheckTimer_tid);
        touchHeartRateModeCheckTimer_tid=TIMER_INVALID;
    }
    
    uiDisplayHeartRateStop();
   
   
    i2cUsePeripheralBus();
    mma8653DataNotifyEnable(TRUE);        
#ifdef CONFIG_TOUCH_SLEEP2     
    hal_pixart_writeRegister(0x7F, 0x00);    // bank0   
    hal_pixart_writeRegister(0x05, TOUCH_MODE_SLEEP);    // Force entering Sleep2
#ifndef CONFIG_VIBRATION    // 12.22
    hadTouchAlertSet=FALSE; //-- 09.19
#endif
#endif
    i2cUseMainBus();
    
    if(touchSleepTimer_tid==TIMER_INVALID){       
        touchSleepTimer_tid=TimerCreate((timeWaitBeforeDown*SECOND), TRUE, 
                                            touchSleepTimerHandler);
    }
}
static void touchTimerReset(void)
{
   if(touchSleepTimer_tid!=TIMER_INVALID){
        TimerDelete(touchSleepTimer_tid);
        touchSleepTimer_tid=TIMER_INVALID;
    }

    if(touchHeartRatePollTimer_tid!=TIMER_INVALID){   
        TimerDelete(touchHeartRatePollTimer_tid);        
        touchHeartRatePollTimer_tid=TIMER_INVALID;
    }
    if(touchHeartRateModeCheckTimer_tid!=TIMER_INVALID){
        TimerDelete(touchHeartRateModeCheckTimer_tid);
        touchHeartRateModeCheckTimer_tid=TIMER_INVALID;
    }        
}


static void touchDelayedPowerdownHandler(timer_id tid)
{
    if(touchHeartRateModeCheckTimer_tid!=tid) return;    
    touchHeartRateModeCheckTimer_tid=TIMER_INVALID;
    touchPowerdown(TRUE);    
}

void touchNewSession()
{    
    touchTimerReset();
    
    isTouchNewSession=TRUE;
    

    if((gConfiguration & BINS_CONFIG_HEARTRATE_ENABLE_MASK)==0 )
    {    
        touchHeartRateModeCheckTimer_tid=TimerCreate((2*SECOND), TRUE, touchDelayedPowerdownHandler);
        return;     // heart rate is not enabled, no need to check for it further.
    }
        
    // only trigger 
    // 08.01 cannot be here otherwise, will not auto sleep while not connected. if(g_ht_data.state==app_state_connected)
    {                        
        touchHeartRateModeCheckTimer_tid=TimerCreate((2*SECOND), TRUE, touchHeartRateModeTimerHandler);    
    }
}

bool touchSensorStatus()
{
   uint8 temp;
   
   i2cUsePeripheralBus();
   
   hal_pixart_writeRegister(0x7F, 0x00);    // bank0   

   temp=hal_pixart_readRegister(0x06);
   
   if(temp & 0x08) return FALSE;    // sleep
   
   return TRUE;     // normal
}


bool touchPressed()
{
    uint32 pios;
    
    if(touchFastSwitch()==TRUE) return FALSE;
            
    pios = PioGets();       
    //-- Active High
        
    if((pios & TOUCH_INT_PIO_MASK)==1)
        return TRUE;
    
    return FALSE;
}

static void pulseBatchCycleTimeoutHandler(timer_id tid)
{
    if(pulseBatchCycleTimer_tid!=tid) return;
    pulseBatchCycleTimer_tid=TIMER_INVALID;
    
    touchSessionEndToDisconnect(TOUCH_WAKE_WITH_NOACTION); // force to close the pulse session    
}

//-- 150710
void touchHeartRateBatchCycle(bool isDisconnectAfterCycle)
{
    if(g_ht_data.state!=app_state_connected) return;
 
    gCurrentHR=0;   // clear this value first                

    if(touchHeartRatePollRunning()==TRUE){ // if already running polling, then stop
        touchSessionEnd(TOUCH_WAKE_WITH_NOACTION);
    }
    else { // else, start it.
        touchPowerdown(FALSE);  // leave power down mode
    }

    touchHeartRateStartMeasure();   
            // this function will call uiDisplayHeartRateActive, eventually uiDisplayHeartRate
    
    gToDisconnectAfterCycle=isDisconnectAfterCycle; // this value was reset by touchHeartRateStartMeasure

    pulseBatchCycleTimer_tid=TimerCreate((20*SECOND), TRUE, pulseBatchCycleTimeoutHandler);
}