
#include <time.h>
#include <reset.h> 

#include "configuration.h"
#include "ht_hw.h"
#include "pedometer.h"
#include "activity_tracking_service.h"
#include "health_thermometer.h"

#include "button_ui.h"
#include "display.h"

timer_id buttonModeCheckTimer_tid;
timer_id buttonSoftResetCheckTimer_tid;

bool isButtonNewSession;

void buttonInit(void)
{           
    isButtonNewSession=FALSE;
    buttonModeCheckTimer_tid=TIMER_INVALID; 
    buttonSoftResetCheckTimer_tid=TIMER_INVALID;
}

static void buttonSoftResetCheckTimerHandler(timer_id tid)
{
    if(buttonSoftResetCheckTimer_tid!=tid) return;
    buttonSoftResetCheckTimer_tid=TIMER_INVALID;

#ifdef CONFIG_BUTTON_SOS    //-- 150510 to handle long press to send SOS beacon
    if(CheckPowerCharging()==TRUE){

#ifdef ENABLE_OTA    
        // forced to forget bonding information
        HandleExtraLongButtonPress(g_app_hw_data.button_press_tid);
#endif        
        systemResetAndRecovery();        
    } else {
        atDeviceEvent(AT_DEVICE_EVENT_LONGPRESS, TRUE);
    }
#else   // NOT BUTTON_SOS
    
#ifdef ENABLE_OTA    
        // forced to forget bonding information
        HandleExtraLongButtonPress(g_app_hw_data.button_press_tid);
#endif
        systemResetAndRecovery(); 
#endif
}

static void buttonModeTimerHandler(timer_id tid)
{
    if(buttonModeCheckTimer_tid!=tid) return;
    buttonModeCheckTimer_tid=TIMER_INVALID;    
    
    isButtonNewSession=FALSE; 

#ifdef CONFIG_TARGET_BINSZ3
    uiLongPressAction();
#endif
    
    if(buttonSoftResetCheckTimer_tid!=TIMER_INVALID){
        TimerDelete(buttonSoftResetCheckTimer_tid);    
        buttonSoftResetCheckTimer_tid=TIMER_INVALID;
    }
    buttonSoftResetCheckTimer_tid=TimerCreate((15*SECOND), TRUE, buttonSoftResetCheckTimerHandler);        
}

bool buttonFastSwitch()
{
    return isButtonNewSession;
}
void buttonSessionEnd()
{
    isButtonNewSession=FALSE;

    if(buttonModeCheckTimer_tid!=TIMER_INVALID){
        TimerDelete(buttonModeCheckTimer_tid);
        buttonModeCheckTimer_tid=TIMER_INVALID;
    }
    if(buttonSoftResetCheckTimer_tid!=TIMER_INVALID){    //-- 11.26 
        TimerDelete(buttonSoftResetCheckTimer_tid);
        buttonSoftResetCheckTimer_tid=TIMER_INVALID;
    }    
}

void buttonNewSession()
{    
    if(buttonModeCheckTimer_tid!=TIMER_INVALID){
        TimerDelete(buttonModeCheckTimer_tid);
        buttonModeCheckTimer_tid=TIMER_INVALID;
    }     
    isButtonNewSession=TRUE;
                      
    buttonModeCheckTimer_tid=TimerCreate((2*SECOND), TRUE, buttonModeTimerHandler);   
    
#ifdef CONFIG_PUSH_ADVERTISING
    PushAdvertisingEnable();    // 150517 - when button pressed, to start advertising lasting 40 seconds
#endif
}

bool buttonPressed(void)
{
    uint32 pios;
    
    if(buttonFastSwitch()==TRUE) return FALSE;
            
    pios = PioGets();       
    //-- Active High
        
    if((pios & BUTTON_UI_PIO_MASK)==0)  // active low
        return TRUE;
    
    return FALSE;
}