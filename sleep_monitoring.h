  

/*  

  1. A timer, to check score chart every 6 minutes.
    1. Record the max. X/Y/Z to the score chart.
    1. If qualified for a sleep condition, add it into the sleep record.
        1. If there were activity records during this period, means already wake, ignore.
    1. Create a new score chart, once after the checking and update action.
    
  1. What is the structure of the sleep record?
    The orignial step record is designed to have four bytes, with shorts for reletive time and step counts.
    The relative time field is structured as 
        14th bit: Notified.
        15th bit: Uploaded.
        13-0 bit: means 11 days if every record costs 1 minute.
    The step count field has 16 bits, we use the 15th bit for the type identifier.
        [15] bit: 1 means, a sleep record,
                  0 means, an activity record.
        and this field is shared with sleep record field as an union.
    The sleep record has:
        [15] bit is always 1.
        [0-7] normalized sleep quality counter.
        [8-14] the maximum activity level during this period.
        
    The sampling rate of g sensor is 6.25 times per second, means 
        12 bits * 6.25 per seconds,
        sum of  12b *6.25 * 60 * 10 per 10 minutes can be represented by 12+12 = 24 bit width.
        
    A normalized sum of ((X'-X)^2)+((Y'-Y)^2)+((Z'-Z)^2) is the value to represent a measurement of activity
    (entropy) during a period of time.
    
    We simply feedback the entropy number to APP to decide what that means from the sleep quality.
    Because XYZ are 12bits (actually can be shifted to 10 bits) each,
    The entropy will not exceed 13 bits. 13+12(the total number of samples per score (up to 10 minutes), 
    it is 25 bits.
    
    We only allow 15 bits for per sleep record.
    Means, it must be requantified from 25 bits to 15 bits. A simple way is to shift it 10 bits,
    or have a ceiling to the upper most big numbers, to create more rooms to the accuracy of the sleep related
    information.
    
*/


#ifndef _SLEEP_MONITORING_H_
#define _SLEEP_MONITORING_H_
 
extern void sleepCheckUpdateEntropy(uint32 momentStrength);
extern void sleepCheckInit(void);
extern void sleepCheckNewCycle(void);
extern void sleepCheckStop(void);
extern void sleepRecordReset(void);

#ifdef CONFIG_TARGET_BINSZ3_HBP

#define MAX_SLEEP_DATA_UPLOAD_SIZE  9   // 150501 from 8, to match with BEACON case
#define MAX_SLEEP_DATA_UPLOAD_SIZE_BYTES (MAX_SLEEP_DATA_UPLOAD_SIZE*2)  // for word length in bytes 

extern uint16 sleepRecordAvailSize(void);
extern uint16 sleepRecordGetStartTime(void);
extern uint16 sleepRecordGet(uint16 *sleepRecordBuffer, uint16 *numRead);
extern void sleepValueNotify(uint16 ucid, uint16 sleepRecord);

extern uint16 lightDataAverage(void);
extern uint16 lightDataCurrent(void);
extern void lightDataQueueInit(void);
extern void lightCheckNewCycle(void);

extern uint16 sleepTimeShiftInMinute(void);

#define SLEEP_CHECK_CYCLE_DEFAULT    1  // 150621

#else   // not HBP

#define SLEEP_CHECK_CYCLE_DEFAULT    5  // 150621

#endif //-- CONFIG_TARGET_BINSZ3_HBP
#define SLEEP_CHECK_CYCLE_MAX        10 // sleep check cycle time cannot be larger than this number


#endif //-- _SLEEP_MONITORING_H_