/*******************************************************************************
 *    Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 * FILE
 *    accel_mma8653fc.h
 *
 *  DESCRIPTION
 *    This module defines the API of the device-driver for the MMA8653FC
 *    accelerometer.
 *
 ******************************************************************************/
#ifndef _MMA_8653FC_H
#define _MMA_8653FC_H

/*============================================================================*
 *  SDK Header Files
 *============================================================================*/
#include <types.h>

/*============================================================================*
 *  Local Header Files
 *============================================================================*/
#include "configuration.h"
#include "display_ui.h"
#include "pedometer.h"
/*============================================================================*
 *  Public Function Prototypes
 *============================================================================*/

/* General initialisation of the MMA8452Q for this application */
extern bool mma8653fcInit(void);

/* Enable/disable full-rate data collection */
extern bool mma8653fcEnableDataCollection(bool enable);

/* Enable/disable full-rate data collection */
extern bool mma8653fcEnableLowPowerDataCollection(bool enable);
extern bool mma8653fcInSleepMode(void);

/* Clear outstanding interrupts */
extern bool mma8653fcClearInterrupt(void);

/* MMA8452Q interrupt handler. The return value indicates whether
 * new data is available (or not).
 */
extern bool mma8653fcInterruptHandler(void);

/* Put the MMA8452Q into "shallow sleep" state, to save power, but
 * still generate an interrupt when movement is detected. 
 */
extern void mma8653fcEnableShallowSleepWithInterrupts(void);

/* Exit the "shallow sleep" state.
 * This function restores the accelerometer to the same state as initialisation
 * followed by enabling.
 */
extern void mma8653fcExitShallowSleep(void);

/* Determine whether the accelerometer is in "shallow sleep" mode. */
extern bool mma8653fcShallowSleepEnabled(void);

/* Read 6-bytes of X,Y,Z data from the accelerometer. */
extern uint8 mma8653fcReadData(uint16 *x, uint16 *y, uint16 *z);

extern void mma8653EnterSleep(void);
extern void mma8653Wakeup(void);

//-- BINS hardware mappings 
//-- INT1     -> GSOR_INT1 -> WAKE
//-- INT2     -> GSOR_INT2 -> PIO[9]
//-- I2C_SDA  -> GSOR_SDA  -> PIO[11]
//-- I2C_SDL  -> GSOR_SDL  -> PIO[10]

/*
    Operation Modes : Off ->Standby ->Wake ->Sleep -> Standby
    
    Standby : Only digital blocks are enabled. others disabled.
    Wake/Sleep are active modes where every blocks are enabled.
    
    Full scale is set to +- 2g/4g/8g
    Each count corresponding to 1/256 or 1/128 or 1/64 g at 10-bit resolution
    
    Low power modes:
    Oversampling schemes can be activated. By MODS= 10 in Register 0x2B.
    Highest resolution is achieved at 1.56 Hz
    Trade-off between low power and high resolution
    
    Auto-WAKE/SLEEP mode
    Orientation detection / Freefall detection, to WAKE from SLEEP
    Freefall,the acceleration magnitude is below a user specified threshold for a user-definable amount of time.
    Typically below +_100mg to +_500mg
    Configurable interrupts: Data Ready,Motion/Freefall, Orientation and Auto-SLEEP events.
    Interrupts configured by INT ENABLE, INT CFG
    
    I2C device address [6:0] is b0011101 = 0x1D  8-bit final is 0x3B (read), 0x3A (write)
    Out_X-MSB-LSB.... Out_Z_LSB :  Register address 0x01-0x06
    Other registers definition in PAGE 20
    (S)MODS Oversampling modes is set to 11 as low power. (Sleep mode power scheme)
    
    Different modes for sleep and wake modes. To save power.
    
    ODR: Output data rates set to 1.56Hz
    
    Interrupt polarity : IPOL 0=low (default) 1=high
        PP_00 : Push-pull (default) 0
    
        CTRL_REG5 to set INT1 or INT2.
*/        


void mma8653MotionDetectStart(bool isStart);
void mma8653DataNotifyEnable(bool isEnable);

#define MMA8653_NEW_SENSITIVITY (((gShakeThreshold+1)*16)-1)    // 09.30
//-- #define MMA8653_NEW_SENSITIVITY 44  // 127 // 09.29 44  // 09.12 56 is too heavy, 09.02 56, trigger the Bins X2 touch sensor

#define MMA8653_GSENSOR_SLEEP_DEFAULT 18  // 09.26 18, 20, 16 is too light

#endif /* _MMA8653FC_H */