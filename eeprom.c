#include <i2c.h>
#include <config_store.h>


/*=============================================================================
 *  Local Header Files
 *============================================================================*/
#include "configuration.h"
#include "i2c_comms.h"
#include "eeprom.h"

#define DATA_EEPROM_I2C_7BIT_ADDR  0x51  // I2C address is 1010 001,
                                         // The first 3 bits are hardwired by hw design.
                                         // 1010 XXX
extern uint32 eepromSize(void)
{
    return CSReadUserKey(7); // the fourth CS key is to indicate the sleep record spare eeprom size
}


extern sys_status eepromReadBytes(uint16 romAddress, uint16 numBytes, uint16 *buffer)
{    
    return I2cEepromRead(DATA_EEPROM_I2C_7BIT_ADDR, romAddress, TRUE, numBytes, buffer);
}

extern sys_status eepromWriteBytes(uint16 romAddress, uint16 *data, uint16 numBytes)
{  
    return I2cEepromWrite(DATA_EEPROM_I2C_7BIT_ADDR, romAddress, TRUE, numBytes, data);
    
}

