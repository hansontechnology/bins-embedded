/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2013
 *
 *  FILE
 *      registers_accel_mma8452q.h
 *
 *  DESCRIPTION
 *      Register definitions for the MMA8653FC accelerometer.
 *
 ******************************************************************************/
#ifndef _MMA8653FC_REGISTERS_
#define _MMA8653FC_REGISTERS_

/* http://cache.freescale.com/files/sensors/doc/data_sheet/MMA8653FC.pdf */

/*============================================================================*
 *  Public Definitions
 *============================================================================*/

#define MMA8653FC_WRITE_ADDR         0x3a   //--
#define MMA8653FC_READ_ADDR          0x3b   //--

#define MMA8653FC_STATUS             0x00
#define MMA8653FC_OUT_X_MSB          0x01
#define MMA8653FC_OUT_X_LSB          0x02
#define MMA8653FC_OUT_Y_MSB          0x03
#define MMA8653FC_OUT_Y_LSB          0x04
#define MMA8653FC_OUT_Z_MSB          0x05
#define MMA8653FC_OUT_Z_LSB          0x06

#define MMA8653FC_SYSMOD             0x0B
#define MMA8653FC_INT_SOURCE         0x0C
    #define MMA8653FC_INT_SRC_ASLP      0x80
    #define MMA8653FC_INT_SRC_FIFO      0x40
    #define MMA8653FC_INT_SRC_TRANS     0x20
    #define MMA8653FC_INT_SRC_LNDPRT    0x10
    #define MMA8653FC_INT_SRC_PULSE     0x08
    #define MMA8653FC_INT_SRC_FF_MT     0x04
    #define MMA8653FC_INT_SRC_DRDY      0x01

#define MMA8653FC_WHO_AM_I           0x0D
#define MMA8653FC_XYZ_DATA_CFG       0x0E
#define MMA8653FC_HP_FILTER_CUTOFF   0x0F
    #define MMA8653FC_HP_FILTER_CUTOFF_PULSE_HPF_BY  0x20
    #define MMA8653FC_HP_FILTER_CUTOFF_PULSE_LPF_EN  0x08
    #define MMA8653FC_HP_FILTER_CUTOFF_SEL1  0x02
    #define MMA8653FC_HP_FILTER_CUTOFF_SEL0  0x01

#define MMA8653FC_PL_STATUS          0x10
#define MMA8653FC_PL_CFG             0x11
#define MMA8653FC_PL_COUNT           0x12
#define MMA8653FC_PL_BF_ZCOMP        0x13
#define MMA8653FC_PL_THIS_REG        0x14
#define MMA8653FC_FF_MT_CFG          0x15
    #define MMA8653FC_ELE    0x80
    #define MMA8653FC_OAE    0x40
    #define MMA8653FC_ZEFE   0x20
    #define MMA8653FC_YEFE   0x10
    #define MMA8653FC_XEFE   0x08
#define MMA8653FC_FF_MT_SRC          0x16
    #define MMA8653FC_FF_MT_SRC_EA   0x80
    #define MMA8653FC_FF_MT_SRC_XHE  0x02
    #define MMA8653FC_FF_MT_SRC_YHE  0x08
    #define MMA8653FC_FF_MT_SRC_ZHE  0x20

#define MMA8653FC_FF_MT_THS          0x17
#define MMA8653FC_FF_MT_COUNT        0x18

#define MMA8653FC_TRANSIENT_CFG      0x1D
#define MMA8653FC_TRANSIENT_SRC      0x1E
#define MMA8653FC_TRANSIENT_THS      0x1F
#define MMA8653FC_TRANSIENT_COUNT    0x20
#define MMA8653FC_PULSE_CFG          0x21
    #define MMA8653FC_PULSE_DPA 0x80
    #define MMA8653FC_PULSE_ELE 0x40
    #define MMA8653FC_ZDPEFE 0x20
    #define MMA8653FC_ZSPEFE 0x10
    #define MMA8653FC_YDPEFE 0x08
    #define MMA8653FC_YSPEFE 0x04
    #define MMA8653FC_XDPEFE 0x02
    #define MMA8653FC_XSPEFE 0x01

#define MMA8653FC_PULSE_SRC          0x22
    #define MMA8653FC_PULSE_SRC_EA   0x80
    #define MMA8653FC_PULSE_SRC_AXZ  0x40
    #define MMA8653FC_PULSE_SRC_AXY  0x20
    #define MMA8653FC_PULSE_SRC_AXX  0x10
    #define MMA8653FC_PULSE_SRC_DPE  0x08    /* double pulse event*/


#define MMA8653FC_PULSE_THSX         0x23
#define MMA8653FC_PULSE_THSY         0x24
#define MMA8653FC_PULSE_THSZ         0x25
#define MMA8653FC_PULSE_TMLT         0x26
#define MMA8653FC_PULSE_LTCY         0x27
#define MMA8653FC_PULSE_WIND         0x28
#define MMA8653FC_ASLP_COUNT         0x29
#define MMA8653FC_CTRL_REG1          0x2A
    #define MMA8653FC_ASLP1  0x80
    #define MMA8653FC_ASLP0  0x40
    #define MMA8653FC_DR2    0x20
    #define MMA8653FC_DR1    0x10
    #define MMA8653FC_DR0    0x08
//-- #define MMA8653FC_LNOISE 0x04
    #define MMA8653FC_FREAD  0x02
    #define MMA8653FC_ACTIVE 0x01
#define MMA8653FC_CTRL_REG2          0x2B
    #define MMA8653FC_ST     0x80
    #define MMA8653FC_RST    0x40
    #define MMA8653FC_SMODS1 0x10
    #define MMA8653FC_SMODS0 0x08
    #define MMA8653FC_SLPE   0x04
    #define MMA8653FC_MODS1  0x02
    #define MMA8653FC_MODS0  0x01
#define MMA8653FC_CTRL_REG3          0x2C
    #define MMA8653FC_TRANS  0x40
    #define MMA8653FC_LNDPRT 0x20
    #define MMA8653FC_PULSE  0x10
    #define MMA8653FC_FF_MT  0x08
    #define MMA8653FC_IPOL   0x02
    #define MMA8653FC_PP_OD  0x01
#define MMA8653FC_CTRL_REG4          0x2D
    #define MMA8653FC_IASLP  0x80
    #define MMA8653FC_ITRANS 0x20
    #define MMA8653FC_ILNDPRT 0x10
    #define MMA8653FC_IPULSE 0x08
    #define MMA8653FC_IFF_MT 0x04
    #define MMA8653FC_IDRDY  0x01
#define MMA8653FC_CTRL_REG5          0x2E
    #define MMA8653FC_CASLP  0x80
    //- #define MMA8653FC_CTRANS 0x20
    #define MMA8653FC_CLNDPRT 0x10
    #define MMA8653FC_CPULSE 0x08
    #define MMA8653FC_CFF_MT 0x04
    #define MMA8653FC_CDRDY  0x01
#define MMA8653FC_OFF_X              0x2F
#define MMA8653FC_OFF_Y              0x30
#define MMA8653FC_OFF_Z              0x31

#endif /* _MMA8653FC_REGISTERS_ */