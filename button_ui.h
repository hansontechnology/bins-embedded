
#ifndef __BUTTON_UI_H__
#define __BUTTON_UI_H__

#include "configuration.h"
#include "ht_hw.h"
#include "pedometer.h"
#include "activity_tracking_service.h"
#include "health_thermometer.h"

extern bool isButtonNewSession;

void buttonInit(void);
void buttonNewSession(void);
void buttonSessionEnd(void);
bool buttonFastSwitch(void);
bool buttonPressed(void);

#endif