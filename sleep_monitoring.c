  
/*  

  1. A timer, to check score chart every 6 minutes.
    1. Record the max. X/Y/Z to the score chart.
    1. If qualified for a sleep condition, add it into the sleep record.
        1. If there were activity records during this period, means already wake, ignore.
    1. Create a new score chart, once after the checking and update action.
    
  1. What is the structure of the sleep record?
    The orignial step record is designed to have four bytes, with shorts for reletive time and step counts.
    The relative time field is structured as 
        14th bit: Notified.
        15th bit: Uploaded.
        13-0 bit: means 11 days if every record costs 1 minute.
    The step count field has 16 bits, we use the 15th bit for the type identifier.
        [15] bit: 1 means, a sleep record,
                  0 means, an activity record.
        and this field is shared with sleep record field as an union.
    The sleep record has:
        [15] bit is always 1.
        [0-7] normalized sleep quality counter.
        [8-14] the maximum activity level during this period.
        
    The sampling rate of g sensor is 6.25 times per second, means 
        12 bits * 6.25 per seconds,
        sum of  12b *6.25 * 60 * 10 per 10 minutes can be represented by 12+12 = 24 bit width.
        
    A normalized sum of ((X'-X)^2)+((Y'-Y)^2)+((Z'-Z)^2) is the value to represent a measurement of activity
    (entropy) during a period of time.
    
    We simply feedback the entropy number to APP to decide what that means from the sleep quality.
    Because XYZ are 12bits (actually can be shifted to 10 bits) each,
    The entropy will not exceed 13 bits. 13+12(the total number of samples per score (up to 10 minutes), 
    it is 25 bits.
    
    We only allow 15 bits for per sleep record.
    Means, it must be requantified from 25 bits to 15 bits. A simple way is to shift it 10 bits,
    or have a ceiling to the upper most big numbers, to create more rooms to the accuracy of the sleep related
    information.
    
    
    For the system time in minutes, how to handle the overflow case.
    
    The system time in minutes is 16 bit length, means 65536 minutes, equals to 45 days.
    For each sync time, it has a chance to reset it to 0, as it is just a time base from local clock,
    However,this action will damage to the day goal tracking, as the day goal is calculated through this
    current system time - 24 hours (max).
    
    
    
    
*/

#include <timer.h>
#include <mem.h>

#include "configuration.h"
#include "pedometer.h"
#include "activity_tracking_service.h"
#include "sleep_monitoring.h"
#include "health_thermometer.h"
#include "i2c_comms.h"
#include "eeprom.h"

#ifdef CONFIG_TARGET_BINSZ3_HBP
#define LIGHT_CHECK_STORE_CYCLE  5   // every 5 sleep check cycles, will save one time the light data
#endif


#define SLEEP_CHECK_CYCLE_MINUTE        (g_at_serv_data.sleep_cycle_time & 0xFF)
#define SLEEP_CHECK_CYCLE_TIME          (SLEEP_CHECK_CYCLE_MINUTE*MINUTE)
// 150413 for test #define SLEEP_CHECK_CYCLE_TIME  (3*SECOND)



// ------------------------------  sleep --------------------------

typedef struct StSleepScore {
    uint16 timeMinute;
    uint32 activeEntropy;
} SleepScore;

SleepScore sleepScore;
timer_id sleepCheckTimerHandler_tid;

void sleepCheckUpdateEntropy(uint32 momentStrength)
{
    sleepScore.activeEntropy +=momentStrength;
}

#ifdef CONFIG_TARGET_BINSZ3_HBP

static void sleepRecordAddStorage(uint16 sleepRecord);
static void sleepRecordInit(void);

static void sleepRecordAdd(void)
{
    uint32 ep=sleepScore.activeEntropy;
    uint16 epNew;
    ep=ep>>10;
    if(ep>0x7FFF) ep=0x7FFF; //-- the 15th bit must be 0
    epNew=ep;        

    if(g_ht_data.state==app_state_connected &&
            gConfiguration & BINS_CONFIG_MASK_RECORD_NOTIFY)
    {                         
        sleepValueNotify(g_ht_data.st_ucid, epNew); 
    } else {
        sleepRecordAddStorage(epNew);
    }           
    
}

#else //-- NOT CONFIG_TARGET_BINSZ3_HBP

static void sleepRecordAdd(void)
{
#if 0    
    StepRecord *latestRecord=stepRecordTop(); 
    
    if(STEP_COUNT(latestRecord)!=0 || ACTIVITY_TYPE(latestRecord)!=0){
        stepRecordAdd(sleepScore.timeMinute,0);
        latestRecord=stepRecordTop();
    }    
    
    {
        uint32 ep=sleepScore.activeEntropy;
        uint16 epNew;
        ep=ep>>10;
        if(ep>0x7FFF) ep=0x7FFF;
        epNew=ep;        
        latestRecord->stepCount= epNew | 0x8000;    //-- 15th bit for the type sleep     
    }
    
    //-- 09.17 stepRecordAdd(systemTimeInMinute(),0);    // to prevent the sleep record, reuse from step adder
#else
    StepRecord *latestRecord;
    
    stepRecordAdd(sleepScore.timeMinute,0);
    latestRecord=stepRecordTop(); 
    {
        uint32 ep=sleepScore.activeEntropy;
        uint16 epNew;
        ep=ep>>10;
        if(ep>0x7FFF) ep=0x7FFF;
        epNew=ep;        
        latestRecord->stepCount= epNew | 0x8000;    //-- 15th bit for the type sleep     
    }

#endif
    
    if(g_ht_data.state==app_state_connected &&
            gConfiguration & BINS_CONFIG_MASK_RECORD_NOTIFY)
    {         
        StepRecord vR= (*latestRecord);                   
        activityValueNotify(g_ht_data.st_ucid, (&vR), STEP_COUNT(latestRecord)); 
        setStepRecordNotify(latestRecord);
    }

}

#endif //-- NOT CONFIG_TARGET_BINSZ3_HBP

static void sleepCheckTimerHandler(timer_id tid)
{
    if(tid!=sleepCheckTimerHandler_tid) return;
    if((gConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)!=0)
    {
        sleepRecordAdd();
        sleepCheckNewCycle();  
#ifdef CONFIG_TARGET_BINSZ3_HBP
        lightCheckNewCycle(); // it will be called only one time, no repeat
#endif
        
    }
}

void sleepCheckStop()
{
    if(sleepCheckTimerHandler_tid!=TIMER_INVALID) //-- 09.26
    {
        TimerDelete(sleepCheckTimerHandler_tid);
        sleepCheckTimerHandler_tid=TIMER_INVALID;
    }    
}

void sleepCheckNewCycle()
{
    sleepCheckStop();
    
    // reset sleepScore
    MemSet(&sleepScore, 0, sizeof(sleepScore));        

    sleepScore.timeMinute= (systemTimeInMinute() & 0x3FFF);
    
    sleepCheckTimerHandler_tid=
       TimerCreate(SLEEP_CHECK_CYCLE_TIME, TRUE, sleepCheckTimerHandler); 

            
}

extern void sleepCheckInit()
{
#ifdef CONFIG_TARGET_BINSZ3_HBP    
    sleepRecordInit();    
    lightDataQueueInit();
#endif
    
    sleepCheckTimerHandler_tid=TIMER_INVALID;
    
    if((gConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)!=0)
    {
        sleepCheckNewCycle();
    }    
}

#ifdef CONFIG_TARGET_BINSZ3_HBP

#include <gatt.h>
#include <gatt_prim.h>
#include <buf_utils.h>
#include "app_gatt.h"
#include "app_gatt_db.h"

static uint16 sleepDataHeader;
static uint16 sleepDataTail;
static uint16 sleepDataSize;
static uint16 sleepStorageSize; // in unit of sleep record
static uint16 sleepStartTime;


#define EEPROM_PHYSICAL_ADDR(dataHeader) (dataHeader*2)

extern void sleepRecordReset(void)
{
    sleepDataSize=0;
    sleepStartTime=systemTimeInMinute();
}

static void sleepRecordInit(void)
{
    sleepStorageSize=(eepromSize()/2);
    sleepDataHeader=sleepDataTail=0;
    sleepRecordReset();
}

static void sleepRecordAddStorage(uint16 sleepRecord)
{   
    sleepDataSize++;
                   
    i2cUsePeripheralBus();    
    eepromWriteBytes(EEPROM_PHYSICAL_ADDR(sleepDataHeader), &sleepRecord, 2); // 2 means 2 bytes   
    i2cUseMainBus();   
    
    sleepDataHeader++;
    if(sleepDataHeader>=sleepStorageSize) sleepDataHeader=0;
    if(sleepDataHeader==sleepDataTail){ // memory full, truncate the last one from tail
        sleepDataTail++;
        if(sleepDataTail>=sleepStorageSize) sleepDataTail=0;
    }

}

uint16 sleepTimeShiftInMinute(void)
{
    uint16 tmpTime;
    uint16 now=systemTimeInMinute();
    
    if(now<sleepStartTime){ // means time wrapped
          tmpTime=0xFFFF-sleepStartTime;
          return now-tmpTime;
    }        
    return now-sleepStartTime;    
}

extern uint16 sleepRecordGetStartTime()
{   
    return sleepStartTime;   //-- unit of minute
}
extern uint16 sleepRecordAvailSize(void)
{
    if(sleepDataSize==0) sleepRecordReset();  //-- 150619 chance to reset
    return sleepDataSize;
}
extern uint16 sleepRecordGet(uint16 *sleepRecordBuffer, uint16 *numRead)
{   
    uint16 numBatchRead;
    bool isReadSuccess;
    uint16 queueLeft;
    
    *numRead=0;
    
    if(sleepDataSize==0) return 0;
    
    numBatchRead=sleepDataSize;
  
    if(numBatchRead>MAX_SLEEP_DATA_UPLOAD_SIZE){
        numBatchRead=MAX_SLEEP_DATA_UPLOAD_SIZE;
    }

    if(sleepDataTail>=sleepStorageSize){    // avoid dead lock condition, TODO, issue from other place
        sleepDataTail=0;        
    }
    
    queueLeft=(sleepStorageSize-sleepDataTail);
    if(queueLeft<numBatchRead){
        numBatchRead=(sleepStorageSize-sleepDataTail);
    }

    i2cUsePeripheralBus();
   
    isReadSuccess=eepromReadBytes(EEPROM_PHYSICAL_ADDR(sleepDataTail), 2*numBatchRead, sleepRecordBuffer);      
   
    i2cUseMainBus();

    {
        
        sleepDataTail=sleepDataTail+numBatchRead;

        if(sleepDataTail>=sleepStorageSize) sleepDataTail=0; //-- it will overflow to 0 actually if under 64KB.

        // adjust the time shift, time move onlu when it is for movement type, not for light sensing type        
        {
            uint16 i;
            for(i=0; i<numBatchRead; i++){
                if((sleepRecordBuffer[i]&0x8000)==0){ 
                    sleepStartTime=sleepStartTime+SLEEP_CHECK_CYCLE_MINUTE; 
                                            // unit of minute
                }
            }
        }
            
        *numRead=numBatchRead;
        sleepDataSize=sleepDataSize-numBatchRead;
        
        if(sleepDataSize==0) sleepRecordReset();    //-- 150619s
    }
        
    return sleepDataSize;
}

extern void sleepValueNotify(uint16 ucid, uint16 sleepRecord)
{
    unsigned char  gVar[2];
    unsigned char  *p_value;

    p_value=gVar;
        
    BufWriteUint16((uint8 **)&p_value, sleepRecord);
    
    GattCharValueNotification(ucid, 
                              HANDLE_SLEEP_RECORDS, 
                              2, 
                              gVar);    
}


#include "lightsensor.h"

#define LIGHT_DATA_QUEUE_SIZE  LIGHT_CHECK_STORE_CYCLE

timer_id lightCheckTimerHandler_tid;
static uint8 lightReadCycleIndex;
static uint8 lightDataAvail;
static uint16 lightDataQueue[LIGHT_DATA_QUEUE_SIZE];
static uint8 lightDataHead;

extern void lightDataQueueInit(void)
{
    int i;
    for(i=0; i<LIGHT_DATA_QUEUE_SIZE; i++) {
        lightDataQueue[i]=0;
    }
    lightDataHead=0;
    lightDataAvail=0;
    
    lightReadCycleIndex=0;
}

static void lightDataNew(uint16 lightData)
{
    lightDataQueue[lightDataHead]=lightData;
    lightDataHead++;
    lightDataAvail++;
    if(lightDataHead>=LIGHT_DATA_QUEUE_SIZE) lightDataHead=0;
}

extern uint16 lightDataCurrent(void) //-- may be used for UI
{
    uint16 lightData;
    uint16 currentDataIndex;
    
    if(lightDataAvail==0) return 0;
    
    if(lightDataHead==0) currentDataIndex=LIGHT_DATA_QUEUE_SIZE-1;
    else currentDataIndex=lightDataHead-1;
    
    lightData=lightDataQueue[currentDataIndex];
    
    return lightData;
}    

extern uint16 lightDataAverage(void) //-- may be used for UI
{
    uint16 lightAverage;
    uint32 lightSum=0;
    int i;
    for(i=0; i<LIGHT_DATA_QUEUE_SIZE; i++) lightSum+=lightDataQueue[i];
    //-- 150602 no need to make average, as degraded already, lightSum=lightSum/lightDataAvail;
    
    if(lightSum>0x7FFF) lightSum=0x7FFF;
    
    lightAverage=lightSum;
    
    return lightAverage;
}    


    
static void lightCheckReadTimerHandler(timer_id tid)
{
    uint16 currentLightStrength;
    
    if(tid!=lightCheckTimerHandler_tid) return;
    
    currentLightStrength=lightSensorRead();    
    lightSensorPowerDown();
    
    lightDataNew(currentLightStrength);
    
    if(lightReadCycleIndex==0)
    {
        uint16 lightRecord= 0x8000 | lightDataAverage(); //-- 150602 no need lightDataAverage()>>1;        
        
        if(g_ht_data.state==app_state_connected &&
            gConfiguration & BINS_CONFIG_MASK_RECORD_NOTIFY)
        {                         
            sleepValueNotify(g_ht_data.st_ucid,lightRecord); 
        } else {
            sleepRecordAddStorage(lightRecord);
        }
    }    
    lightReadCycleIndex+=SLEEP_CHECK_CYCLE_MINUTE;
    if(lightReadCycleIndex>=LIGHT_CHECK_STORE_CYCLE) lightReadCycleIndex=0;

    lightCheckTimerHandler_tid=TIMER_INVALID;
}

static void lightCheckStartTimerHandler(timer_id tid)
{
    if(tid!=lightCheckTimerHandler_tid) return;
    lightSensorPowerOn();
    
    // need wait > 200ms after power on, to read light data
    lightCheckTimerHandler_tid=
       TimerCreate((300*MILLISECOND), TRUE, lightCheckReadTimerHandler); 
}

static void lightCheckStop(void)
{
    if(lightCheckTimerHandler_tid!=TIMER_INVALID)
    {
        TimerDelete(lightCheckTimerHandler_tid);
        lightCheckTimerHandler_tid=TIMER_INVALID;
    }    
}

extern void lightCheckNewCycle(void)
{
    lightCheckStop();
    lightCheckStartTimerHandler(lightCheckTimerHandler_tid);  
}
    
#endif // CONFIG_TARGET_BINSZ3-HBP    
    
   
